AntRay
======
AntRay is free & open-source 3D/XR(VR+AR) browser, framework, game/app maker/editor... system.

Implemented in [Godot](https://godotengine.org).

![AntRay](splash.png)

# Menu
* [Try](#try)
* [Download](#download)
* [Controls](#controls)
* [What can be browsed](#what-can-be-browsed)
* [Documentation](#documentation)
* [Donations](#donations)

# Try
Antray 3D/XR browser in your web browser:
[ < ]

# Download
Download standalone app:
[Android GLES2/3] [iPhone GLES2/3] [Windows GLES2/3] [Mac GLES2/3] [Linux GLES2/3] [HTML5 GLES2/3]

# Features & Controls

| Feature                 | Keyboard      | Mouse       | Touch    | VR       | Linux | Android | HTML5 | Windows | OSX | iOS | UWP |
|-------------------------|:-------------:|:-----------:|:--------:|:--------:|:-----:|:-------:|:-----:|:-------:|:---:|:---:|:---:|
| Rotate Left & Right     | Left & Right  | Move X      | Joystick | Gyro/Mag | ✔     | ✔       |       |         |     |     |     |
| Look Up & Down          | Up & Down     | Move Y      | Swipe Y  |          |       |         |       |         |     |     |     |
| Zoom In & Out           | PgUp & PgDn   | Scroll      |          |          |       |         |       |         |     |     |     |
| Move Forward & Backward | W & S         | ×           | Joystick | !        | ✔     | ✔       |       |         |     |     |     |
| Strafe Left & Right     | A & D         | ×           |          |          |       |         |       |         |     |     |     |
| Change Speed            | +-123456      | ×           |          |          |       |         |       |         |     |     |     |
| Jump & Crouch           | Q/Space & E/C | ×           |          |          |       |         |       |         |     |     |     |
| Fly Mode                | Tab           |             |          |          |       |         |       |         |     |     |     |
| Fly Up & Down           | ^^^           | ×           |          |          |       |         |       |         |     |     |     |
| Change Weapon           | Ctrl+1234…    | Ctrl+Scroll |          |          |       |         |       |         |     |     |     |
| Seed on the ground      |               | Left-click  |          |          |       |         |       |         |     |     |     |
| Reload                  |               | Right-click |          |          |       |         |       |         |     |     |     |
| Shoot enemy             |               | Left-click  |          |          |       |         |       |         |     |     |     |
|                         |               |             |          |          |       |         |       |         |     |     |     |

## Keyboard & Mouse
Use `F1` for help, `Esc` to toggle between mouse capture & release (with menu)

* `WASD` to move, `Q` jump/up, `E` or `C` crouch/down
* `Tab` to toggle fly & walk modes

## Touch screen
Use joysticks, buttons and top menu.

### AR on 2D display without 3D glasses
Mobile sensors (gravity, magnetic field, gyroscope, accelerometer...) are used for AR on touch screen without AR glasses.

## XR (VR/AR) glasses and controllers

# What can be browsed
* generated world
* [... more will follow](#donations)

# Documentation
* [Exports](doc/Exports.md)
* [Controllers](doc/Controllers.md)
* [Directories & Files](doc/Filesystem.md)
* [Repositories & Download Manager](doc/DownloadManager.md)
* [Web](doc/Web.md)

## Roadmap
* [Issues](https://gitlab.com/duoverso/antray/-/issues)
* [TODO.md](TODO.md)
* [Trello](https://trello.com/b/AEBgillp/antray) board

# Donations
at [Patreon> AntRay](https://www.patreon.com/antray)

Allows to do or do better following and more:
* Custom AR worlds (generated and/or manualy created/edited), custom VR games...
* More content: models, kinematic, music, sound... in Library
* Multiplayer, custom servers
* multimedia, communication, learning, apps, tools...
* Real world from [OpenStreetMap](https://www.openstreetmap.org/)
* Historical world from [OpenHistoricalMap](https://openhistoricalmap.org/)
* Documentation (in [`doc/`](/doc/) & in the app)
* OS X & iPhone exports

# Community
* [GitLab](https://gitlab.com/duoverso/antray)
* [YouTube](https://www.youtube.com/channel/UCAKR3L--gkQQQZnTWtNBg-A)
* [Twitter](https://twitter.com/AntRayXR)
* [Facebook](https://www.facebook.com/AntRayXR)
* [Discord](https://discord.gg/kXfZ7BtpCE)
* [Trello](https://trello.com/b/AEBgillp/antray)