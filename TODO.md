TODO
====
! Merge previous TODOs
    - TODOs in code
    - move everything to issues on gitlab
        - this TODO.md
        - Trello board https://trello.com/c/czaBdLZo/89-antray


# Refactoring

## Settings
? change vars `settings_properties` to godot's `_get_property_list()` ?
    ```
    func _get_property_list():
       return [
           { name = "MIDI", type = TYPE_NIL, hint_string = "midi_", usage = PROPERTY_USAGE_GROUP }
       ]
    ```
- Doesn't allow to save default_config_file (in development mode before export app), only user_config_file

- speed presets at one place - move from TopMenu to Player and use them for switching (in gonkee too)
? Player.can() to has_ability() ?
? Change a way to access dirs & files? `OS.get_executable_path().get_base_dir().plus_file("hello.txt")`
- rename tower scene to mob/nest
- rename left_touch_joystick_button -> touch_joystick_left ? & for right
- Generator:
    - create new chunks not from Chunk class but from changable (pre)loaded resource
        ? rename TerrainGenerator to Chunker because TerrainGenerator itself is part of Chunk.generate_terrain()
    - options configurable as dictionary (and loaded from json)

- rename `core/` to `kernel/`
- Signals - how fast they really are for heavy signaling?
    ? propagate changes from Player to HUD?
- `Log.*usec*()` move to custom Time class?

# Bugs
- speeds: 1, 2, 3, 4, 5...
- Android
    - Double-clicking in TopMenu needed. Looks like it needs press&release to select&confirm but something like PlayerController handles one of this double event ?
    - Sliders in TopMenu doesn't work when touch already started elsewhere, eg. on joystick
    - joystick's touch area in portrait is moved to half of y position
    - joystick is too small and too much at corner of the screen
- HTML5 export: better mouse capture & top menu enabled
% Too many Buildings at few Chunks
- Generator: some objects (trees, nests...) should consider normal vector of surface where are placed
- Return to Lobby
- control Log categories
- water shader on Android
- grid 1x1 should work without falling
- lags when generator creating chunks
    - multithreaded
    - generate in background (in idle time only)
    - never fall through? relocate player securely above max_height(+objects) when enter in area bellow chunk's min_height? toggle [ ] Generator to generate/remove areas
    - periodically raycast from underground up to surface if player is above ground on it's XZ coords
- submenu items in topmenu doesn't work on first click at Android (if submenu is selected then every menu item even in same level works at second click)

# ASAP
- save Settings for everything
    > We love GDPR as everybody does. But you have to agree with bringing doomsday by you using this software to entire Milky Way galaxy including you, your family, your pets, your butler and gardener, your aircrafts, cars and bikes, your nation, government and big tech companies which you love so much... and to all ants and other killer beasts on our lovely planet. This is free & open-source software (FOSS) and you're using it on your own risk. We did our best to make it bullet-proof and minimize damages to negative values but you were warned.
! DownloadManager & Loader
    ? Check hash of content, mtime...
! Multiplayer
- LoD (still isn't implemented in Godot 3.5, try 4.0?)
? Portals
% switch material for terrain, sea...
- autorun (like bad implementation flying with mouse) Ctrl+A
? sea levels is changing by external scene's `_process()` but it could have custom animation node & timer to be added/removed from scene tree

# Input
- better gravity? https://gamedev.stackexchange.com/a/41917
    - use internal ProjectSettings gravity (9.8) instead of -60
    - to change gravity: `PhysicsServer.area_set_param(get_viewport().find_world().get_space(), PhysicsServer.AREA_PARAM_GRAVITY, 9.8)`
- jump_timer and jump_max_time instead of jumping: bool to allow player hold jump shorter or longer time and apply jump_force shorter and longer time
- wall jumps?
- One joystick for: Move(WASD), Move(WA) & Turn(Left+Right); Two joysticks: Move(WASD) and Look(Arrows)
- Flashing Flashligh ;-) Shift+F
- player.mouse_captured -> has_cursor ?
- switch player controllers (enter car/ship)
- more specific inputs should be tested before less specific, not for current InputMap only but more generaly (eg. BUTTON_WHEEL_UP & event.control)
- remove `ui_` in InputMap?
- hscrollbars in topmenu doesn't work
- toggle mobile sensors

- custom print_debug() as Log.debug() ?
- physics layers vs. node groups
    - what's faster?
    - where are used in entire project?
        - `find . -name '*.tscn' -exec grep layer {} \; -print`
```
- terrain
    - tree
    - grass
- buildings
    ? walls
    ? enemy-buildings / destructible-buildings
- platform
- player
    ? hit-area
- enemies
    ? hit-area
    ? detect-area
- npc
- items (limited amount by space/weight...)
    - coins (unlimited amount)
    - health-packs (limited by not-full-health)
    - weapons... (give weapon and/or ammo, limited by not-full-ammo)
- object/terminal/lever...
```

## UI & HUD
> [HUD (heads-up display)](https://en.wikipedia.org/wiki/HUD_(video_gaming)) *"transparent display that presents data without requiring users to look away from their usual viewpoints... do not need to refocus"* ... in context of XR means "object & viewport gizmos" only ?!?

- Dialogues
    - install Dialogic plugin & add input key for dialog actions
- Popup Dialog with LineEdit input
- Saves > [x] Autosave on exit || [x] Load last on start || ... list from user://saves/
    - Remove older than > 1 hour | 1 day | 1 week | 1 month | 3 months | 6 months | 1 year
- [ ] Flashlight (as checkbox)
- Game > Settings
    - On Start
        - [x] Capture nouse
    - HUD > ...
    - [x] Fullscreen (F11)
    - [x] V-Sync
    - FPS: [ ] Show || (x) Unlimited | ( ) 5 | ( ) 10 | ( ) 15 | ( ) 20 | ( ) 30..90 step10 | ( ) 100..200 step20
        - `Engine.target_fps = `
    - Music > (o) Play (,) | ( ) Pause (,) | ( ) Stop (.) | Next Track (>) | Previous Track (<) || [x] Shuffle | (o) Repeat All | ( ) Repeat current | ( ) No repeat
        - Volume >
        - || ... Tracks >
    - Sound > [x] Enabled
        - Volume >
    - Mouse Sensitivity
    - Invert Mouse & Touch Drag (separately for both axis)
    - Menu Button Transparency > 0-100% with step 10%
    - OS Permissions > ...
    - Crosshair > ... change style; [ ] Hide when mouse not captured
    - joystick size (default size is based on window resolution and stretch mode)
    - DirectLight: direct energy, indirect energy, shadows
    - [ ] Top Menu as One button
- Level >
    - Environment >
        - Skymap (or WorldEnvironment?)
        ? Current&All Environment(s) (in current&all level(s)) ?? changing skymap only or entire WorldEnvironment
        - Ambient light...
        - Fog > [x] Enabled || Color > ... | Min depth > ( ) 0..90%
    - Underwater > DarkBlue, LightBlue... Sun settings
- Player >
    - Character > ... player controllers
- Game over & restart (when player die)
- Main screen:
    - Visit:
        - Bookmarks
        - URL address
        - Current position
        - World places
        - Virtual Worlds
            - Demo
                - Segway, Scooter, Bike, Motorbike, Car, Formula...
                - Tram, Subway, Train...
                - Yacht, Ship...
                - Airplane, Jet
                - Spaceship
                    - Asteroids (Solo&Coop)
                    - Space Invaders
                    - 3D Space Shooter
- Tabs for more Viewports at more XR locations
    - first as AR-place
    - disabled notifications as tab highliting, sounds... sleep tab
    - new tab as lobby (home), constructor (new file/scene) or clone current character in current scene

## Player Controllers
- CAN_SWIM: can't go underwater
    - CAN_DIVE: oxygen bar & "underwater flying" up to surface
    - CAN_WALK_ON_WATER
- **Skills**: array of item categories indexes with achieved e**xp**erience int
- **Spells**: magical abilities
- Ctrl+num change speed, num change weapon
- hands
- model of tools (and animations for raising, falling, dropping/throwing...)
### physics/damage types (with different sound mechanics too)
- properties:
    - throwable (damage from speed, weight sharpness)
!!! Ranged (for raycast, bullets...)
- Area of Effect
- bullets (bouncing like grenades, sticky like Bio-rifle, reflecting off surfaces like Ripper)
    - multiple like shotgun
- Bouncing
- controlled with crosshair (like Redemeer)
## Tools (everything as data)
? are in hands only or pockets, wearable, inventory/backpack... too ?
-> check rpgs:
    - RS [Tool belt](https://runescape.wiki/w/Tool_belt)
    - WoW https://www.wowhead.com/items
    ? Lineage... ?
- Knife, Machete
- Translocator
- Key ring
- Hammer, Pliers/Tongs, Chisel, Glassblowing pipe
    - Moulds (... Amulet, Bracelet, Necklace, Ring, Tiara, Ammo, Bolt, (Un)Holy, Sickle, Chain link, Rod)
- Axe/Hatchet, Pickaxe, Saw, Shears
- **Fishing:** rod, cage, net; Harpoon
- **Firemaking:** Tinderbox/Lighter
- **Cooking:**
- **Farming:** Rake, Seed dibber, Spade, Gardening trowel, Secateurs, Watering can
- Needle
- **Thieving:** Lockpick, Stethoscope (for Healthcare too)
- Watch, Sextant/Compas, Map
- (Smart)Phone/Tablet, Computer/Notebook
- (dioptric/sun/transparent/VR/AR/XR) Glasses, facemask
- Pestle & mortar
- Smoker, Incense burner, Insect repellent
- Grappling hook
- Shrinker (duke3d) & Enlarger
- Healer, Speeder, Zoomer... Blinder (creates fog for enemy?)
- Invisibler
### Weapons (everything as data)
- Punch, **Fist weapons:** 
- Club, Rock hammer
- Dagger, Sword/Sabre/Katana
- Bow & Crossbow, Darts, *... throwing weapons*
- Sharp stick, Spear
- Flamethrower (make a fire too like tinderbox)
- Grenades & Grenade Launcher
- Mines, sticky bomb with laser (duke3d)

# Lobby
- Constructor
    - delete everything to blank space
? one lobby limited in space for everybody, larger & more space(s) paid
- private/locked locations, invitations for others, moving to real location

# Sound & Music
- Do not repeat same track nor previous track... so make a list, shuffle it and play it
- insects (butterflies, bees, stingers, mosquitos, other bug) flying around Player with spatial sounds

# Generator
- `options` as more optimized `int` instead of `Array`? No. Array of bools with keys by enum

## Chunks
- more threads
- mutex & semaphore
- pool of freed reusable chunks (lower time to recreate), reduce pool only when lowering grid size (chunk_dist)
? Collisions only for nearest (but weapons and other far impacts will not work)
- `Chunk.generator` should by of type `TerrainGenerator` but cycling references deny it
- https://www.gamedeveloper.com/programming/continuous-lod-terrain-meshing-using-adaptive-quadtrees
- create terrain in `_init()` stage and only call `add_child()` in `_ready()` ?
? https://www.geeksforgeeks.org/form-a-spiral-matrix-from-the-given-array/

# Long term
- check https://github.com/KOBUGE-Games/godot-logger/blob/master/logger.gd
- everything as editor tool
- everything visually scripted
    - VisualScript should be improved
- top_menu.gd: trigger actions instead of duplicate code to handle clicks in top menu
- destructible trees, buildings...
- destructible/editable terrain
- touch: speed & zoom dragable, value shown, +/- buttons for larger steps, label/btn as reset
- swipe to zoom-in/out
- part of screen for joystick anywhere (or bottom-left/right quarters?)
- configurable UI
    - joystick(s) (actions, amount, positions (anywhere in area), theme)
    - buttons (actions, amount, size, theme...)
- web top menu falling on the ground and breaks apart
- circle/radial menu https://kidscancode.org/godot_recipes/ui/radial_menu/

!!! var `input_direction` changed in `_input()` (from keyboard, mouse, joystick...) and used to combine in `_physics_process()` instead of checking pressed actions each cycle
!!! so `scene_tree.set_input_as_handled()` for all actions (including movement actions!)
!!! move `_input()` from player to global.gd (or `input.gd`) and call player's methods! or in `core/input/controller.gd` parent class for all `core/input/*.gd` controllers

- combine gonkee's & garbaj's controllers together by settings (slope, ...)
- combine all controllers together (fps, rpg, car, airplane, rts...)

# Optimizations
- rewrite GDScript noises from TerrainGenerator into custom Noise

## Debugging
- Log
- Global.debug - change to int or Array?
- Project Settings... Debug > Settings

# Assets
- font Fira Sans http://mozilla.github.io/Fira/

# Promo
- Record screen to video
- URL params to start in different modes, screens, settings, locations...
- Recording track as Path for camera_follow.gd
- Documentation
    - generated for API

# Godot Proposals
- string format for binary, bin_to_int(), String::int_to_hex(),int_to_bin()
- log10 or better log(value, base) with custom bases to invert pow(base, value); log() should be renamed to ln()
    ```
    func logWithBase(value: float, base: float) -> float:
        return log(value) / log(base)
    ```
- Editor: when text is Replaced, search next