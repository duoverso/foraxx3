// Gonkee's noise textures, original video: https://youtu.be/ybbJz6C9YYA
// this file heavily references https://thebookofshaders.com/
shader_type canvas_item;
//shader_type spatial;

float rand(vec2 coord){
	// prevents randomness decreasing from coordinates too large
	coord = mod(coord, 10000.0);
	// returns "random" float between 0 and 1
	return fract(sin(dot(coord, vec2(12.9898,78.233))) * 43758.5453);
}

void fragment() {
	vec2 coord = UV * 10.0;

	float noise;
	noise = rand(coord);

	COLOR = vec4(vec3(noise), 1.0);
}