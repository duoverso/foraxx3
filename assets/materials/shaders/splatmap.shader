// https://godotforums.org/discussion/20565/splatmap-shader-issues-on-mobile-devices-gles2-and-how-to-cope-with-them
shader_type spatial;
render_mode cull_disabled;
uniform sampler2D splatmap;
uniform sampler2D tex_base;
uniform sampler2D tex_r;
uniform sampler2D tex_g;
uniform sampler2D tex_b;
uniform sampler2D tex_a;
uniform float base_res = 0.05; //20
uniform float r_res = 0.05; //2
uniform float g_res = 0.05;//10
uniform float b_res = 0.05;//10
uniform float a_res = 0.05;//5
varying vec2 loc_vertex;
void vertex() {
    loc_vertex = VERTEX.xz;
}
void fragment() {
    vec3 base_col;
    vec3 r_col;
    vec3 g_col;
    vec3 b_col;
    vec3 a_col;
    vec4 texs = texture(splatmap, UV);
    float rval = texs.r;
    float gval = texs.g;
    float bval = texs.b;
    float aval = 1.0 - texs.a;
    float baseval = max(0.0, 1.0 - (rval + gval + bval + aval));
    base_col = texture(tex_base, loc_vertex * base_res).rgb * baseval;
    r_col = texture(tex_r, loc_vertex * r_res).rgb * rval;
    g_col = texture(tex_g, loc_vertex * g_res).rgb * gval;
    b_col = texture(tex_b, loc_vertex * b_res).rgb * bval;
    a_col = texture(tex_a, loc_vertex * a_res).rgb * aval;
    ALBEDO = base_col + r_col + g_col + b_col + a_col;
}