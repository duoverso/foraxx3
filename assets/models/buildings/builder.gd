# @TODO
# - flip Z coords so front is heading to player/camera
#   (it's better than 180deg rotation around Y because flipping of X is still needed and it's even more confusing)
# - working as tool script
# - "x": 1; "x": "1%"; "x": {"from": 1, "to": "1%", "step": "2%"}
# + working as class without scene

extends StaticBody
class_name Builder

var debug: bool = false
var debug_log := ""

# Width of temple between center of corner columns
export var width: float = 30 setget set_width
# Height of columns
export var height: float = 10 setget set_height
# Depth of temple between center of corner columns
export var depth: float = 70 setget set_depth

export var aging: float = 0

# https://en.wikipedia.org/wiki/Ancient_Greek_architecture#Structure
var parts := {
	"width": 30.923, #width, # 30.935 width of stylobate - 2 * 0.06 margin of outter columns
	"height": 10.443, #height,
	"depth": 69.577, # depth, # 69.589 stylobate - 2 * 0.06 margin of outter columns

#	"materials": { # doesn't need create:false until there is no mesh or collision defined, but it's a bit faster to parse
#		"Gravel": {"material": "res://assets/materials/terrain/Gravel020/Gravel020.tres", "uv1_triplanar": true, "uv1_scale": {"x": 0.2, "y": 0.2, "z": 0.2}, "random_uv_offset": true},
#	},

#	"gizmo/": {"mesh": "cube", "width": 1, "height": 1, "depth": 1,
#		"Bottom": {"x": 0, "y": 0, "z": 0},
#		"Top": {"x": 0, "y": "100%", "z": 0},
#		"Front": {"x": 0, "y": 0, "z": "-50%"},
#		#"Back": {"x": 0, "y": 0, "z": "50%"},
#		"Left": {"x": "-50%", "y": 0, "z": 0},
#		#"Right": {"x": "50%", "y": 0, "z": 0},
#	},
#	"Floor": {"mesh": "cube", "collision": "cube", "height": 0.1}, # "height": "5%", "y": "-50%"},
#	"SideWall": {"mesh": "cube", "width": "1", "depth": "90%", "x": "-50%", "mirror_x": true},
#	"FrontWall": {"mesh": "cube", "depth": "1", "width": "90%", "z": "50%", "mirror_x": true},
#	"InvisibleBox": {"collision": "cube", "width": 3, "height": 2, "depth": 1, "y": 1},

	# Roof (y <= 0 and bellow)
	# https://en.wikipedia.org/wiki/Crepidoma
	"floor/": {"name": "Stylobate", "mesh": "cube",
			"collision": "cube", "collision_name": "StylobateCollision",
			"shift_by_height": -1,
			"material": "res://assets/materials/terrain/Gravel020/Gravel020.tres",
			#"uv1_triplanar": true, "uv1_scale": {"x": 0.2, "y": 0.2, "z": 0.2},
			"uv1_scale": {"x": 6.0, "y": 14.0, "z": 1.0},
#		# first stylobate has to got mesh & collision to be duplicated, all has to have height, extent_x & extent_z
#		# (extents are for one side only, so it's doubled)
		"Stylobate1": {"height": 0.55, "extents_x": 0.06, "extents_z": 0.06, "y": 0}, # "y": -0.275},
#		"Stylobate2": {"height": 0.512, "extents_x": 0.7, "extents_z": 0.7}, # "y": "-55%"},
#		"Stylobate3": {"height": 0.512, "extents_x": 0.7, "extents_z": 0.7}, # "y": "-60%"},
		#"Stylobate4": {"height": 0.3, "extents_x": 0.103, "extents_z": 0.103}, # "y": "-65%"},
#		"Stylobate4": {"height": 20, "extents_x": 0.103, "extents_z": 0.103, "uv1_scale": {"x": 0.05, "y": 0.05, "z": 0.05}}, # "material": "res://assets/materials/terrain/Ground047/Ground047.tres"}, # "y": "-65%"},

		"UpperStylobateBottom": {"height": 0.315, "width": 22.348, "depth": 59.087, "y": 0, "shift_by_height": 1},
		"UpperStylobateTop": {"height": 0.395, "width": 21.715, "depth": 58.243, "shift_by_height": 1},
	},
#	"stairs/": {"name": "Stair", "mesh": "cube", "collision": "cube",
#		"Front": {"width": 5, "height": 0.25, "y": -0.3, "shift_by_height": -1, "depth": 0.35, "z": -35.1985, "shift_by_depth": 1, "mirror_z": true},
#	},
	"walls/": {"mesh": "cube", "collision": "cube",
			"material": "res://assets/materials/terrain/Gravel020/Gravel020.tres",
			#"uv1_triplanar": true, "uv1_scale": {"x": 0.2, "y": 0.2, "z": 0.2},
			"uv1_scale": {"x": 2.0, "y": 2.0, "z": 2.0},
			"random_uv_offset": true,
		"Side": {"width": 1.3, "x": 10.8, "shift_by_width": -1, "y": 0.71, "shift_by_height": 1, "depth": 51.6165, "mirror_x": true},
		"Middle": {"width": 19, "x": 0, "y": 0.71, "shift_by_height": 1, "depth": 1.154, "z": 8.866, "shift_by_depth": 1},
		"Front": {"width": 6, "x": -9.5, "shift_by_width": 1, "mirror_x": true, "y": 0.71, "shift_by_height": 1, "depth": 2.044, "z": -24.1245, "shift_by_depth": 1, "mirror_z": true},
	},
#	"stairs/": {},
#	"doors/": {},
#	"windows/": {},
#	# Above roof (y >= 0)
	"columns/": {"name": "Column", "mesh": "cylinder", "radial_segments": 8, "rings": 0, "collision": "cylinder", "collision_name": "ColumnCollision", "y": "50%",
			"material": "res://assets/materials/terrain/Gravel020/Gravel020.tres",
			#"uv1_triplanar": true, "uv1_scale": {"x": 0.2, "y": 0.2, "z": 0.2},
			"uv1_scale": {"x": 0.2, "y": 0.2, "z": 0.2},
			"random_uv_offset": true,
		"Side": {"bottom_radius": 0.9615, "top_radius": 0.68, "start_x": -14.503, "end_x": -14.503, "start_z": 33.827, "end_z": -33.827, "amount": 17, "mirror_x": true, "mirror_z": false, "parts": {"/entablature/Architrave1": {}, "/entablature/Architrave2": {}}}, # {"height": 0.5}}}, # start_x & end_x: -50%+bottom_radius
		"Front": {"bottom_radius": 0.9615, "top_radius": 0.68, "start_x": -10.6, "end_x": 10.6, "start_z": 33.827, "end_z": 33.827, "amount": 6, "mirror_z": true, "parts": {"/entablature/Architrave1": {"extent": 2.543}, "/entablature/Architrave2": {"extent": 2.543}}},
		"Pronaos": {"bottom_radius": 0.856, "top_radius": 0.6845, "start_x": -9.9415, "end_x": 9.9415, "y": 0.71, "shift_by_height": 1, "start_z": "-40%", "end_z": "-40%", "amount": 6, "mirror_z": true, "parts": {"/entablature/Architrave1": {"extent": 2.543}, "/entablature/Architrave2": {"extent": 2.543}}},
		"CellaSide": {"bottom_radius": 0.607, "top_radius": 0.405, "start_x": -5.52, "end_x": -5.52, "y": 0.71, "shift_by_height": 1, "start_z": "-25%", "end_z": "1%", "amount": 9, "mirror_x": true, "parts": {"/entablature/Architrave1": {}, "/entablature/Architrave2": {}}},
		"CellaFront": {"bottom_radius": 0.607, "top_radius": 0.405, "start_x": -5.52, "end_x": 5.52, "y": 0.71, "shift_by_height": 1, "start_z": "5%", "end_z": "5%", "amount": 5, "parts": {"/entablature/Architrave1": {}, "/entablature/Architrave2": {}}},
		"Opisthodomos": {"bottom_radius": 0.607, "top_radius": 0.405, "start_x": -5.52, "end_x": 5.52, "y": 0.71, "shift_by_height": 1, "start_z": "5%", "end_z": "5%", "amount": 5, "parts": {"/entablature/Architrave1": {}, "/entablature/Architrave2": {}}},
	},
#	"entablature/": {"create": false, "mesh": $Entablature, "collision": $EntablatureCollision,
#		"Architrave1": {"height": 1.362},
#		"Architrave2": {"height": 1.362},
#		#"frieze1": {"mesh": $Entablature, "collision": $EntablatureCollision, "height": 1.362},
#	},
#	# https://en.wikipedia.org/wiki/Tympanum_(architecture)
	"Roof": {"mesh": "prism",
		"collision": "prism",
		"extents_x": 0.5,
		"y": 13.0, "height": 5.0, # "extents_y": 0.1,
		#"start_z": height + 2 * 1.362,
		"material": "res://assets/materials/terrain/Gravel020/Gravel020.tres",
		"uv1_triplanar": true, "uv1_scale": {"x": 0.2, "y": 0.2, "z": 0.2}
	},
}

func _init(_parts: Dictionary = {}, _name: String = "Temple"):
	name = _name
	if !_parts.empty():
		parts = _parts
	#print(parts)
	width = parts["width"] if parts.has("width") else 1
	height = parts["height"] if parts.has("height") else 1
	depth = parts["depth"] if parts.has("depth") else 1

func set_width(_width: float) -> void:
	width = _width
#	if is_inside_tree():
#		update_temple()

func set_height(_height: float) -> void:
	print("set_height")
	height = _height
#	if is_inside_tree():
#		update_temple()

func set_depth(_depth: float) -> void:
	depth = _depth
#	if is_inside_tree():
#		update_temple()

# warning-ignore:function_conflicts_variable
func debug_log(text: String) -> void:
	if !debug:
		return
	print(text)
	debug_log += text + "\n"

func _ready() -> void:
	#print(merge_dictionaries({"asdf": 1}, {"zxcv": 2}))
	#return
	process_parts(parts, "/", merge_params({}, parts))
	#translation.y += height / 2
	#update_temple()

#func update_temple() -> void:
#	return
#	update_stylobates()
#	update_columns()
#	update_roof()

func merge_params(params1: Dictionary, params2: Dictionary) -> Dictionary:
	for key in params2:
		if key[0].to_lower() == key[0]: # is it property (start with lowercase)?
			if params2[key] == null:
				debug_log("erasing param " + key)
				params1.erase(key)
			else:
				params1[key] = params2[key]
	return params1

func process_parts(subparts: Dictionary, path: String = "/", parent_params: Dictionary = {}):
	debug_log("processing: " + str(subparts.keys()))
	for key in subparts:
		# Skip when "create": false
		if !(subparts[key] is Dictionary) or (subparts[key].has("create") and !subparts[key]["create"]):
			continue

		debug_log(path + key)

		var params = parent_params.duplicate()
		params = merge_params(params, subparts[key])

		if params.has("width"):
			params["width"] = get_coords(params["width"], width)
		if params.has("height"):
			params["height"] = get_coords(params["height"], height)
		if params.has("depth"):
			params["depth"] = get_coords(params["depth"], depth)

		# Cumulative params for width, height & depth
		if params.has("extents_x"):
			parent_params["width"] = (parent_params["width"] if parent_params.has("width") else 0) + (2 * params["extents_x"])
			params["width"] = (params["width"] if params.has("width") else 0) + (2 * params["extents_x"])
		if params.has("extents_y"):
			parent_params["height"] = (parent_params["height"] if parent_params.has("height") else 0) + (2 * params["extents_y"])
			params["height"] = (params["height"] if params.has("height") else 0) + (2 * params["extents_y"])
		if params.has("extents_z"):
			parent_params["depth"] = (parent_params["depth"] if parent_params.has("depth") else 0) + (2 * params["extents_z"])
			params["depth"] = (params["depth"] if params.has("depth") else 0) + (2 * params["extents_z"])

		if params.has("x"):
			params["x"] = get_coords(params["x"], width)
		if params.has("y"):
			params["y"] = get_coords(params["y"], height)
		if params.has("z"):
			params["z"] = get_coords(params["z"], depth)

		# Cumulative params for x, y & z
		# @TODO shift_* should not shift by object's size but by specified value, use shift_by_*
		#if params.has("shift_y"):

		# Cumulative params for x, y & z by width, height & depth
		var shift_by_half_width
		var shift_by_half_height
		var shift_by_half_depth

		if params.has("shift_by_width"):
			shift_by_half_width = params["shift_by_width"] * params["width"] / 2 if params.has("width") else 0
			if subparts[key].has("x"):
				parent_params["x"] = params["x"] + shift_by_half_width
			else:
				parent_params["x"] = (parent_params["x"] if parent_params.has("x") else 0) + shift_by_half_width
			params["x"] = (params["x"] if params.has("x") else 0) + shift_by_half_width

		if params.has("shift_by_height"):
			shift_by_half_height = params["shift_by_height"] * params["height"] / 2 if params.has("height") else 0
			if subparts[key].has("y"):
				parent_params["y"] = params["y"] + shift_by_half_height
			else:
				parent_params["y"] = (parent_params["y"] if parent_params.has("y") else 0) + shift_by_half_height
			params["y"] = (params["y"] if params.has("y") else 0) + shift_by_half_height

		if params.has("shift_by_depth"):
			shift_by_half_depth = params["shift_by_depth"] * params["depth"] / 2 if params.has("depth") else 0
			if subparts[key].has("z"):
				parent_params["z"] = params["z"] + shift_by_half_depth
			else:
				parent_params["z"] = (parent_params["z"] if parent_params.has("z") else 0) + shift_by_half_depth
			params["z"] = (params["z"] if params.has("z") else 0) + shift_by_half_depth

		if key.ends_with("/"):
			process_parts(subparts[key], path + key, params)
		elif key[0].to_upper() == key[0]:
			build_part(path + key, params)
			if shift_by_half_width:
				parent_params["x"] += shift_by_half_width
			if shift_by_half_height:
				parent_params["y"] += shift_by_half_height
			if shift_by_half_depth:
				parent_params["z"] += shift_by_half_depth

func get_coords(value, full: float) -> float:
	#var result: float
	if value is String and value.ends_with("%"):
		return full * float(value) / 100 # - full / 2
	else:
		return float(value)
	#debug_log("get_coords(", value, ", ", full, ") = ", result)
	#return result

func part_coords(num, amount, start: Vector3, end: Vector3) -> Vector3:
	return Vector3(
		start.x + num * (end.x - start.x) / max(1, amount - 1),
		start.y + num * (end.y - start.y) / max(1, amount - 1),
		start.z + num * (end.z - start.z) / max(1, amount - 1)
	)

func mirror_and_translate(node: Spatial, mirror_vector: Vector3):
	var mirrored = node.duplicate()
	add_child(mirrored)
	mirrored.translation = node.translation * mirror_vector

func build_part(path: String, params: Dictionary):
	#debug_log("... building " + path + ": " + str(params))
	var node: Spatial
	var collision: CollisionShape
	var coords := Vector3(
#		get_coords(params["x"], width) if params.has("x") else 0,
#		get_coords(params["y"], height) if params.has("y") else 0,
#		get_coords(params["z"], depth) if params.has("z") else 0
		params["x"] if params.has("x") else 0,
		params["y"] if params.has("y") else 0,
		-params["z"] if params.has("z") else 0
	)

	if params.has("mesh"):
		debug_log(". creating mesh " + path)
		node = create_mesh(params["mesh"], params)
		node.translation = coords

	if params.has("collision"):
		debug_log(". creating collision " + path)
		collision = create_collision(params["collision"], params)
		collision.translation = node.translation if node else coords # Optimization: Copy translation from node if it exists or create it

	# Duplicate amount of meshes and/or collisions
	if params.has("amount"):
		var area_width = width # - 2 * params["bottom_radius"]
		var area_height = height
		var area_depth = depth # - 2 * params["bottom_radius"]
		var start = Vector3(
			get_coords(params["start_x"], area_width) if params.has("start_x") else coords.x,
			get_coords(params["start_y"], area_height) if params.has("start_y") else coords.y,
			-get_coords(params["start_z"], area_depth) if params.has("start_z") else coords.z
		)
		var end = Vector3(
			get_coords(params["end_x"], area_width) if params.has("end_x") else coords.x,
			get_coords(params["end_y"], area_height) if params.has("end_y") else coords.y,
			-get_coords(params["end_z"], area_depth) if params.has("end_z") else coords.z
		)

		debug_log(". duplicating {amount: " + str(params["amount"]) + ", start: " + str(start) + ", end: " + str(end) + "}")
		for num in range(params["amount"]):
			var num_node: Spatial
			var num_collision: CollisionShape
			# Use already created mesh for first item in a row
			if num == 0:
				if node:
					num_node = node
				if collision:
					num_collision = collision
			# Duplicate mesh and/or collision for others
			else:
				if node:
					num_node = node.duplicate()
					if params.has("random_uv_offset") and num_node.material_override:
						num_node.material_override = num_node.material_override.duplicate()
						num_node.material_override.uv1_offset = Vector3(randf(), randf(), randf())
					add_child(num_node)
				if collision:
					num_collision = collision.duplicate()
					add_child(num_collision)

			var part_coords = part_coords(num, params["amount"], start, end)
			if num_node:
				num_node.translation = part_coords
			if num_collision:
				num_collision.translation = part_coords

			# Mirror meshes and/or collisions for amount of objects
			if params.has("mirror_x") and params["mirror_x"]:
				if num_node:
					mirror_and_translate(num_node, Vector3(-1, 1, 1))
				if num_collision:
					mirror_and_translate(num_collision, Vector3(-1, 1, 1))
			if params.has("mirror_z") and params["mirror_z"]:
				if num_node:
					mirror_and_translate(num_node, Vector3(1, 1, -1))
				if num_collision:
					mirror_and_translate(num_collision, Vector3(1, 1, -1))
	# Single object
	else:
		# Mirror meshes and/or collisions for single object
		if params.has("mirror_x") and params["mirror_x"]:
			if node:
				mirror_and_translate(node, Vector3(-1, 1, 1))
			if collision:
				mirror_and_translate(collision, Vector3(-1, 1, 1))
		if params.has("mirror_z") and params["mirror_z"]:
			if node:
				mirror_and_translate(node, Vector3(1, 1, -1))
			if collision:
				mirror_and_translate(collision, Vector3(1, 1, -1))

func create_mesh(type: String, params: Dictionary = {}, add_child: bool = true) -> MeshInstance:
	type = type.to_lower()
	var node = MeshInstance.new()

	if type == "cube":
		node.mesh = CubeMesh.new()
		node.mesh.size = Vector3(
			(get_coords(params["width"], width) if params.has("width") else 1.0), # + (get_coords(params["extents_x"], width) if params.has("extents_x") else 0),
			get_coords(params["height"], height) if params.has("height") else 1.0,
			get_coords(params["depth"], depth) if params.has("depth") else 1.0
		)
		node.translation = node.mesh.size / 2
		debug_log(".. creating cube {size: " + str(node.mesh.size) + ", translation: " + str(node.translation) + "}")
	elif type == "capsule":
		node.mesh = CapsuleMesh.new()
		debug_log(".. creating capsule")
	elif type == "cylinder":
		node.mesh = CylinderMesh.new()
		if params.has("radial_segments"):
			node.mesh.radial_segments = params["radial_segments"]
		if params.has("rings"):
			node.mesh.rings = params["rings"]
		if params.has("top_radius"):
			node.mesh.top_radius = params["top_radius"]
		if params.has("bottom_radius"):
			node.mesh.bottom_radius = params["bottom_radius"]
		if params.has("height"):
			node.mesh.height = params["height"]
		node.translation.y += node.mesh.height / 2
		debug_log(".. creating cylinder {mesh: " + str(node.mesh) + ", top_radius: " + str(node.mesh.top_radius) + ", bottom_radius: " + str(node.mesh.bottom_radius) + ", height: " + str(node.mesh.height) + ", translation: " + str(node.translation) + "}")
	elif type == "prism":
		node.mesh = PrismMesh.new()
		node.mesh.size = Vector3(
			params["width"] if params.has("width") else 1,
			params["height"] if params.has("height") else 1,
			params["depth"] if params.has("depth") else 1
		)
		node.translation.y += node.mesh.size.y / 2
		if params.has("left_to_right"):
			node.mesh.left_to_right = params["left_to_right"]
		debug_log(".. creating prism")
	elif type == "sphere":
		node.mesh = SphereMesh.new()
		debug_log(".. creating sphere")

	if params.has("name"):
		node.name = params["name"]
	if params.has("material"):
		node.material_override = load(params["material"]).duplicate()
		if params.has("uv1_triplanar"):
			node.material_override.uv1_triplanar = params["uv1_triplanar"]
		if params.has("uv1_scale"):
			node.material_override.uv1_scale = Vector3(
				params["uv1_scale"].x if params["uv1_scale"].has("x") else 1,
				params["uv1_scale"].y if params["uv1_scale"].has("y") else 1,
				params["uv1_scale"].z if params["uv1_scale"].has("z") else 1
			)
		if params.has("random_uv_offset"): # has to be set per each instance when param "amount" is present too (on duplicated material)
			node.material_override.uv1_offset = Vector3(randf(), randf(), randf())
	if add_child:
		add_child(node)
	return node

# @TODO Copy from mesh, do not compute again
func create_collision(type: String, params: Dictionary = {}, add_child: bool = true) -> CollisionShape:
	type = type.to_lower()
	var collision = CollisionShape.new()

	if type == "capsule":
		collision.shape = CapsuleShape.new()
		debug_log(".. creating collision capsule")
	elif type == "cube":
		collision.shape = BoxShape.new()
		collision.shape.extents = Vector3(
			get_coords(params["width"], width) if params.has("width") else 1.0,
			get_coords(params["height"], height) if params.has("height") else 1.0,
			get_coords(params["depth"], depth) if params.has("depth") else 1.0
		) / 2
		collision.translation = collision.shape.extents
		debug_log(".. creating collision box {extents: " + str(collision.shape.extents) + ", translation: " + str(collision.translation) + "}")
	elif type == "cylinder":
		collision.shape = CylinderShape.new()
		collision.shape.radius = max(params["top_radius"], params["bottom_radius"])
		collision.shape.height = get_coords(params["height"], height)
		collision.translation.y = collision.shape.height / 2
		debug_log(".. creating collision cylinder {shape: " + str(collision.shape) + ", translation: " + str(collision.translation) + "}")
	elif type == "prism":
		collision.shape = ConvexPolygonShape.new()
		var prism_width_half = params["width"] / 2 if params.has("width") else 0.5
		var prism_height_half = params["height"] / 2 if params.has("height") else 0.5
		var prism_depth_half = params["depth"] / 2 if params.has("depth") else 0.5
		collision.shape.points = [
			Vector3(-prism_width_half, -prism_height_half, -prism_depth_half),
			Vector3(prism_width_half,  -prism_height_half, -prism_depth_half),
			Vector3(prism_width_half,  -prism_height_half,  prism_depth_half),
			Vector3(-prism_width_half, -prism_height_half,  prism_depth_half),
			Vector3(0,   prism_height_half,  prism_depth_half),
			Vector3(0,   prism_height_half, -prism_depth_half),
		]
		debug_log(".. creating prism as collision convexpolygon {shape points: " + str(collision.shape.points) + "}")
	elif type == "sphere":
		collision.shape = SphereShape.new()
		debug_log(".. creating collision sphere")

	if params.has("name"):
		collision.name = params["name"]
	if add_child:
		add_child(collision)
	return collision

# Columns and Entablature
func update_columns() -> void:
	#$Column.queue_free()
	#$ColumnCollision.queue_free()
	$Column.visible = false
	$ColumnCollision.disabled = true

	if parts.has("entablature"):
		for key in parts["entablature"]:
			var entablature = parts["entablature"][key]
			entablature["mesh"].visible = false
			entablature["collision"].disabled = true

	#var row_width := width - 2 * column_bottom_radius
	#var row_depth := depth - 2 * column_bottom_radius
	#for num_row in range(column_rows):
	for row in parts["column_rows"]:
		# Create a new row
		#if parts["columns"].size() < column_rows:
		#	parts["columns"].append([])
		#var row_x := num_row * row_width / (column_rows - 1) - (row_width / 2)
		var row_width = width - 2 * row["bottom_radius"]
		var row_depth = depth - 2 * row["bottom_radius"]
		var start_x = float(row["start_x"]) if !(row["start_x"] is String) or !("%" in row["start_x"]) else row_width * float(row["start_x"]) / 100 - row_width / 2
		var start_z = float(row["start_z"]) if !(row["start_z"] is String) or !("%" in row["start_z"]) else row_depth * float(row["start_z"]) / 100 - row_depth / 2
		var end_x = float(row["end_x"]) if !(row["end_x"] is String) or !("%" in row["end_x"]) else row_width * float(row["end_x"]) / 100 - row_width / 2
		var end_z = float(row["end_z"]) if !(row["end_z"] is String) or !("%" in row["end_z"]) else row_depth * float(row["end_z"]) / 100 - row_depth / 2
		#print("row x: ", start_x, " - ", end_x, ", z: ", start_z, " - ", end_z, ", width: ", row_width, ", depth: ", row_depth)
		for num in range(row["amount"]):
			update_column(row, num, start_x, end_x, start_z, end_z)
			if row.has("mirror_x") and row["mirror_x"]:
				update_column(row, num, -start_x, -end_x, start_z, end_z)
				if row.has("mirror_z") and row["mirror_z"]:
					update_column(row, num, -start_x, -end_x, -start_z, -end_z)
			if row.has("mirror_z") and row["mirror_z"]:
				update_column(row, num, start_x, end_x, -start_z, -end_z)

		# Entablature
		if row.has("entablature"):
			var entablature_translation_y = height
			for key in row["entablature"]:
				if !parts["entablature"].has(key): # or !parts["entablature"][key].has("mesh"):
					continue
				var add_height = update_entablature(row, key, start_x, end_x, start_z, end_z, entablature_translation_y)
				if row.has("mirror_x") and row["mirror_x"]:
					update_entablature(row, key, -start_x, -end_x, start_z, end_z, entablature_translation_y)
					if row.has("mirror_z") and row["mirror_z"]:
						update_entablature(row, key, -start_x, -end_x, -start_z, -end_z, entablature_translation_y)
				if row.has("mirror_z") and row["mirror_z"]:
					update_entablature(row, key, start_x, end_x, -start_z, -end_z, entablature_translation_y)
				entablature_translation_y += add_height

# Returns height of entablature
func update_entablature(row, key: String, start_x, end_x, start_z, end_z, entablature_translation_y) -> float:
	var row_entablature: Dictionary = row["entablature"][key]
	var entablature: Dictionary = parts["entablature"][key]
	var entablature_mesh: Node = entablature["mesh"].duplicate()
	entablature_mesh.visible = true
	entablature_mesh.mesh = entablature_mesh.mesh.duplicate()
	var entablature_x_size = end_x - start_x
	var entablature_z_size = end_z - start_z
	var entablature_length = sqrt(pow(entablature_x_size, 2) + pow(entablature_z_size, 2)) + 2 * row["top_radius"] + (2 * row_entablature["extent"] if row_entablature.has("extent") else (2 * entablature["extent"] if entablature.has("extent") else 0))
	var entablature_height = row_entablature["height"] if row_entablature.has("height") else entablature["height"]
	entablature_mesh.mesh.size = Vector3(
		row["top_radius"] * 2,
		entablature_height,
		entablature_length
	)
	entablature_mesh.translation = Vector3(
		start_x + entablature_x_size / 2,
		entablature_translation_y + entablature_height / 2,
		start_z + entablature_z_size / 2
	)
	entablature_mesh.rotation = Vector3(0, atan2(entablature_x_size, entablature_z_size), 0)
	add_child(entablature_mesh)

	var entablature_collision = entablature["collision"].duplicate()
	entablature_collision.disabled = false
	entablature_collision.shape = entablature_collision.shape.duplicate()
	entablature_collision.shape.extents = entablature_mesh.mesh.size / 2
	entablature_collision.translation = entablature_mesh.translation
	entablature_collision.rotation = entablature_mesh.rotation
	add_child(entablature_collision)

	return entablature_height

func update_column(row, num, start_x, end_x, start_z, end_z) -> void:
	var column_mesh: MeshInstance = row["mesh"].duplicate()
	column_mesh.visible = true
	column_mesh.mesh = column_mesh.mesh.duplicate()
	add_child(column_mesh)

	var is_fallen = (aging > 0 and randf() <= aging) # aging: is fallen?

	column_mesh.mesh.height = height
	if aging > 0: # aging
		var column_aging = (1 - aging) + randf() * (1 - aging)
		column_mesh.mesh.height *= min(1, column_aging)

	column_mesh.mesh.top_radius = row["top_radius"]
	column_mesh.mesh.bottom_radius = row["bottom_radius"]
	column_mesh.translation = Vector3(
		start_x + num * (end_x - start_x) / max(1, row["amount"] - 1),
		column_mesh.mesh.bottom_radius if is_fallen else column_mesh.mesh.height / 2, # aging: put on floor or to half of column height
		start_z + num * (end_z - start_z) / max(1, row["amount"] - 1)
	)
	if row.has("random_rotation") and row["random_rotation"]:
		column_mesh.rotation.y = randf() * TAU
	if row.has("random_uv_offset"):
		column_mesh.material_override = column_mesh.get_surface_material(0).duplicate()
		column_mesh.material_override.uv1_offset = Vector3(randf(), randf(), randf())

	if is_fallen: # aging: rotate & translate
		var column_x_sign = 1 if column_mesh.translation.x >= 0 else -1
		#var fallen_direction = (randf() * PI/2) - (PI/4) * (1 if randf() > 0.5 else -1)
		var fallen_direction: float = 0.0 if column_x_sign < 0 else PI # (PI/4)
		column_mesh.rotation = Vector3(0, fallen_direction, PI/2)
		column_mesh.translation += Vector3(
			(column_mesh.mesh.height / 2 + column_mesh.mesh.bottom_radius / 2) * cos(fallen_direction),
			0,
			(column_mesh.mesh.height / 2 + column_mesh.mesh.bottom_radius / 2) * sin(fallen_direction)
		)

	var column_collision = row["collision"].duplicate()
	column_collision.disabled = false
	add_child(column_collision)
	column_collision.shape.height = column_mesh.mesh.height
	column_collision.shape.radius = max(row["top_radius"], row["bottom_radius"])
	column_collision.translation = column_mesh.translation
	column_collision.rotation = column_mesh.rotation
#	parts["columns"][row].append({"mesh": column_mesh, "collision": column_collision})

func update_roof() -> void:
	if !parts.has("roof") or !parts["roof"].has("mesh"):
		#$Roof.queue_free()
		$Roof.visible = false
		return
	else:
		parts["roof"]["mesh"].visible = true

		#parts["roof"].mesh.size = Vector3(width + 2 * column_top_radius, width / 10, depth + 2 * column_top_radius)
		parts["roof"]["mesh"].mesh.size = Vector3(
			width + (2 * parts["roof"]["extent_x"] if parts["roof"].has("extent_x") else (2 * parts["roof"]["extent"] if parts["roof"].has("extent") else 0)),
			parts["roof"]["height"] if parts["roof"].has("height") else width / 10,
			depth + (2 * parts["roof"]["extent_z"] if parts["roof"].has("extent_z") else (2 * parts["roof"]["extent"] if parts["roof"].has("extent") else 0))
		)
		parts["roof"]["mesh"].translation.y = (parts["roof"]["start_z"] if parts["roof"].has("start_z") else height) + (parts["roof"]["mesh"].mesh.size.y / 2)

	if !parts.has("roof") or !parts["roof"].has("collision"):
		#$RoofCollision.queue_free()
		$RoofCollision.disabled = true
		return
	else:
		parts["roof"]["collision"].disabled = false
		parts["roof"]["collision"].scale = parts["roof"]["mesh"].mesh.size
		parts["roof"]["collision"].translation = parts["roof"]["mesh"].translation


#	var floor_material = SpatialMaterial.new()
#	floor_material.albedo_color = Color(randf(), randf(), randf())
#	material = floor_material
#	var ceiling_material = SpatialMaterial.new()
#	ceiling_material.albedo_color = floor_material.albedo_color.darkened(0.5)
#	$CSGMesh.material = ceiling_material
