extends CSGBox

func _ready():
	var floor_material = SpatialMaterial.new()
	floor_material.albedo_color = Color(randf(), randf(), randf())
	material = floor_material
	var ceiling_material = SpatialMaterial.new()
	ceiling_material.albedo_color = floor_material.albedo_color.darkened(0.5)
	$CSGMesh.material = ceiling_material
