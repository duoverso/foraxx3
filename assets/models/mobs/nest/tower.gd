extends Spatial

var logchan := "tower"

var unique := false

var pre_stats := {"power": 0}
onready var stats := $PowerStats # Stats "trait"

var child_power: float
var min_child_power: float
var max_child_power: float

export var money: int

var children := []
#export var child_scene = preload("res://assets/models/mobs/test/kiibaa.tscn")
export var child_scenes = [
	preload("res://assets/models/mobs/test/kiibaa.tscn"),
]
#export var max_children: int # pow(scale.y, 2)
export var children_at_once: int # scale.y
export var spawn_time_min := 2.0
export var spawn_time_max := 4.0
var spawn_timer := 0.0

#func _init():
#	Log.verbose("_init()", logchan)

func _ready():
	Log.debug(str(self) + "::_ready() at " + str(global_transform.origin), logchan)
	child_power = sqrt(stats.power)
	min_child_power = child_power * lerp(0.5, 1.0, randf())
	max_child_power = child_power * lerp(1.0, 2.0, randf())
	#enemies_at_once = lerp(power / max_enemy_power, power / min_enemy_power, randf()) # ease(randf(), 5.0))
	#children_at_once = 3
	#children_at_once = int(max(1, stats.power / max_child_power)) # ease(randf(), 5.0))
	children_at_once = int(max(1, lerp(0.1, 1.0, randf()) * stats.power / max_child_power)) # ease(randf(), 5.0))
	stats.reward = int(stats.power / 100)

	var spawn_time_reducer = max(1, log(children_at_once))
	spawn_time_min /= spawn_time_reducer
	spawn_time_max /= spawn_time_reducer

	update_label()

func update_label():
	Log.verbose(str(self) + "::update_label()", logchan)
	stats.level.text = str(max(1, floor(sqrt(stats.power))))  # Level is sqrt of power
	stats.title.text = "%d Power %.1f" % [get_instance_id(), max(0, stats.power)]
	if Log.get_log_level_of("tower") >= Log.Level.DEBUG:
		stats.title.text += "\n" \
			+ ("ene: %d in %.2f to %.2f\n" % [children_at_once, spawn_time_min, spawn_time_max]) \
			+ ("enepow: %d / %d / %d\n" % [min_child_power, child_power, max_child_power]) \
			+ ("dist: %.1f" % global_translation.length())

func _process(delta):
	spawn_timer -= delta
	if stats.target and spawn_timer < 0.0 and stats.power > 0 and children.size() < children_at_once:
		spawn_child()

func spawn_child():
	#var new_child = child_scene.instance()
	var new_child = child_scenes[randi() % child_scenes.size()].instance()
	var new_child_power = min(max(stats.power, min_child_power), lerp(min_child_power, max_child_power, randf()))
	new_child.pre_stats["power"] = new_child_power
	stats.power -= new_child_power
	# Move enemy above the tower
	new_child.translation = $SpawnPoint.translation
	var tween := create_tween().set_trans(Tween.TRANS_QUART).set_ease(Tween.EASE_OUT)
	tween.tween_property(new_child, "translation", Vector3((randf() - 0.5) * scale.x, 5.0 + randf() * scale.y, (randf() - 0.5) * scale.z), 1.0)
	# Keep the scale of spawned enemy by dividing the scale of this parent enemy
	new_child.scale = Vector3.ONE / scale
	children.append(new_child)
	Log.debug(str(self) + "::spawn_child(): " + str(new_child), logchan)
	add_child(new_child)
	Sound.play_enemy_spawned(new_child)
	update_label()
	reset_spawn_timer()

func reset_spawn_timer():
	spawn_timer = lerp(spawn_time_min, spawn_time_max, randf())

func child_died(enemy):
	if stats.power <= 0 and children.size() <= 1:
		destroy()
		return
	# do not produce next enemy immediately when one of all spawnable at once just died
	if (children.size() >= children_at_once):
		reset_spawn_timer()
	children.erase(enemy)
	# produce next enemy faster when there's none
	if (children.size() <= 0):
		spawn_timer /= 2

	update_label()
	Log.debug(str(self) + "::child_died(" + str(enemy) + ")", logchan)

func destroy() -> void:
	Effect.add_explosion(global_transform.origin, scale * 15, 4.0, int(scale.x * 8))
	Sound.play_tower_destroyed(self, false)
	Global.player.money += money
	queue_free()

#func _to_string():
#	return name + ":[" + get_class() + ":" + str(get_instance_id()) + "](" + str(enemies.size()) + " / " + str(max_enemies) + ")"

func _on_EnterArea_body_entered(body: Node) -> void:
	if stats.target or body != Global.player:
		return
	Log.debug(str(self) + ":: Player entered", logchan)
	stats.target = body
	#UI.hud.dialogue("Too close!\n\nMinions are spawning.", 1.0)
	if not unique:
		$Tower/Tower.mesh = $Tower/Tower.mesh.duplicate(true)
		unique = true
	#$Tower/Tower.mesh.material.albedo_color = Color(1, 0, 0)
	var tween := create_tween()
	tween.tween_property($Tower/Tower.mesh.material, "albedo_color", Color(1, 0, 0), 1.0)
	Sound.play_tower_entered()

func _on_ExitArea_body_exited(body: Node) -> void:
	if not stats.target or body != stats.target: # body == Global.player:
		return
	Log.debug(str(self) + ":: Player exited", logchan)
	stats.target = null
	if stats.power > 0:
		#$Tower/Tower.mesh.material.albedo_color = Color(1, 1, 1)
		var tween := create_tween()
		tween.tween_property($Tower/Tower.mesh.material, "albedo_color", Color(1, 1, 1), 1.0)
#		UI.hud.dialogue("Far enough..." \
#			+ ("\n\nbut minions will follow you either." if enemies.size() > 0 else ""), 2.0)
