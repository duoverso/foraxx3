# Thanks to Tom
# https://www.youtube.com/watch?v=OSYehj6oa3U
# (https://github.com/codewithtom/godot-fps/blob/lesson-4/Enemy/Enemy.gd)
extends KinematicBody

var logchan := "kiibaa"

var pre_stats := {"power": 0}
onready var stats := $PowerStats

var parent

var walk_speed := 50.0
var walk_rotate_speed := 1.0
var run_speed := 150.0
var run_rotate_speed := 2.0

var stop_range := 2.0
var time_to_hit := 1.0
var timer_hit := 0.0

var active_color := Color(randf(), randf(), randf(), randf())
var inactive_color := Color(1.0, 1.0, 1.0) - active_color

var run_at # Vector3 or null # := Vector3(20, 0, 0)
# @todo Hit and bounce back

onready var raycast := $Magma/RayCast

#var gravity := 1
#var velocity = Vector3.ZERO
var timer_rand_place := 0.0
var time_to_rand_place := 3.0
var space_state

func _ready():
	if "stats" in get_parent():
		parent = get_parent()

	Log.debug(str(self) + "::_ready(), parent.min_child_power: " + str(parent.min_child_power) + ", parent.max_child_power: " + str(parent.max_child_power) + ", stats.power: " + str(stats.power), logchan)

	stats.level.text = str(max(1, floor(sqrt(stats.power))))  # Level is sqrt of power
	name = Global.enemy_names[randi() % Global.enemy_names.size()]
	stats.title.text = name
	update_damage_label()

	# Enemy power from min to max of parent_enemy as color from green to red
	if parent:
		var power_ratio = inverse_lerp(parent.min_child_power, parent.max_child_power, stats.power)
		if power_ratio <= 0.5:
			active_color = Color(2 * power_ratio, 1, 0)
		else:
			active_color = Color(1, 1 - 2 * (power_ratio-0.5), 0)
		#Log.debug(str(self) + "::_ready(), power_ratio: " + str(power_ratio), logchan)

	# Unique material
	var material = $Body.get_surface_material(0).duplicate(true)
	#material.flags_transparent = true
	$Body.set_surface_material(0, material)
	$Body/Head.set_surface_material(0, material)
	set_color_active()
	#$Body.get_surface_material(0).set_albedo(active_color) # Color(1, 0, 0))
	#$Body/Head.get_surface_material(0).set_albedo(active_color) # Color(1, 0, 0))

	space_state = get_world().direct_space_state # for raycasting
	#target = Global.player
	stats.target = null
	run_at_random()

func update_damage_label():
	$Label3DDamage.text = str(stats.damage)

func _process(delta):
	if stats.target:
		timer_hit -= delta
		if timer_hit < 0.0:
			raycast.force_raycast_update()
			if raycast.is_colliding():
				var collider = raycast.get_collider()
				if collider.is_in_group("Player"):
					collider.hp -= stats.damage
					timer_hit = time_to_hit

		var position: Vector3 = stats.target.global_transform.origin + Vector3(0, 1.5, 0)
		# Does enemy see a player?
		#var my_origin = global_transform.origin # + Vector3(0, 1.0, 0)
		var result = space_state.intersect_ray(global_transform.origin, position)
		if result and result.collider.is_in_group("Player"):
			run_at = position
		# player hides, wander at last coordinates
		else:
			#print("target lost")
			#wandering_at = target.global_transform.origin
			#target = null
			set_color_active()
		rotate_to(position, delta * run_rotate_speed)
		move_to(position, delta * run_speed)
		#look_at(target_position, Vector3.UP) # rotate immediately
	else:
		timer_rand_place -= delta
		rotate_to(run_at, delta * walk_rotate_speed)
		move_to(run_at, delta * walk_speed)
		if timer_rand_place < 0.0:
			timer_rand_place = time_to_rand_place
			run_at_random()
#	if target:
#		var direction = translation.direction_to(to_local(target.translation))
#		#var direction = to_global(translation).direction_to(target.translation).normalized()
#		#translation -= direction * speed * delta
#		global_translate(direction * speed)

#func _physics_process(delta):
#	var direction := (target.translation - translation).normalized()
	#velocity += direction * walk_speed * delta
#	velocity = Vector3.ONE
	#velocity.x += (target.translation.x - translation.x) * walk_speed * delta
	#velocity.z += (target.translation.z - translation.z) * walk_speed * delta
	#velocity.y += (target.translation.y - translation.y) * walk_speed * delta
	#velocity.y -= gravity * delta
	#velocity = move_and_slide(velocity, Vector3.UP)
#	translation.move_toward(target.translation, delta)

# warning-ignore:unused_argument
func rotate_to(position: Vector3, delta: float):
	look_at(position, Vector3.UP)

#	var new_transform = transform
#	new_transform = new_transform.looking_at(transform.xform_inv(position), Vector3.UP) #.xform
	#var new_transform = transform.looking_at(position - global_transform.origin, Vector3.DOWN)
	#print(new_transform)
	#get_tree().quit()
	#transform.origin = get_parent().global_transform.origin
	#var new_transform = transform.looking_at(position - global_transform.origin, Vector3.UP)
#	transform = transform.interpolate_with(new_transform, delta)

func rotate_and_move_to(position: Vector3, delta: float):
	var new_transform = global_transform.looking_at(position, Vector3.UP) #.translated(position - transform.origin)
	new_transform.origin = position # move through walls
	# @todo ? change global_transform when working with global_transform until now ?
	transform = transform.interpolate_with(new_transform, delta)

func move_to(position: Vector3, delta_with_speed: float):
	var attack_range := (position - global_translation).length()
	if attack_range <= stop_range:
		return
	#var direction := (position - global_transform.origin - Vector3(0, -0.1, 0)).normalized()
	var direction := (position - global_transform.origin).normalized()
	move_and_slide(direction * delta_with_speed, Vector3.UP)

func run_at_random():
	# @todo random direction * distance
	run_at = global_transform.origin + Vector3(randf() * 10, randf() * 2, randf() * 10) - Vector3(5, 1, 5)

func hit(dmg_received: float):
	stats.health -= dmg_received
	if stats.health <= 0.0:
		die()
	else:
		Sound.play_enemy_hit(self)

func die():
	Log.debug(str(self) + "::die(), reward: " + str(stats.reward), logchan)
	Global.player.money += stats.reward
	# Inform parent enemy about hit
	if parent and parent.has_method("child_died"):
		parent.child_died(self)
	Sound.play_enemy_died(self, false)
	Effect.add_explosion(global_transform.origin, Vector3(3,3,3))
	queue_free() # Remove from scene

func _on_DetectArea_body_entered(body):
	#print("player is: ", Global.player)
	#if body is KinematicBody:
	if body == Global.player:
		#print("player entered: ", body, " [", body.global_transform.origin, "] (mine: ", global_transform.origin, ")")
		#print("detected: ", body.translation, " (mine: ", translation, ")")
		stats.target = body
		#set_color_active()
#	else:
#		print("body entered: ", body, " [", body.global_transform.origin, "] (mine: ", global_transform.origin, ")")

func _on_DetectArea_body_exited(body):
	if body == stats.target:
		#print("target exited: ", body)
		stats.target = null
		timer_rand_place = 0.0
		#set_color_inactive()
#	else:
#		print("body exited: ", body)

func _on_HitArea_body_entered(body):
	#if body is KinematicBody:
	if body == Global.player:
		print("player hit: ", body.hp)
		body.hp -= stats.damage
		#target = null
		#set_color_active()
#	else:
#		print("body hit: ", body)

func set_color_active():
	$Magma.get_surface_material(0).albedo_color = active_color
	$Body.get_surface_material(0).albedo_color = active_color # Color(1, 0, 0))
	#$Body.get_surface_material(0).emission = active_color # Color(1, 0, 0))
	$Body/Head.get_surface_material(0).albedo_color = active_color # Color(1, 0, 0))
	# not neccessery when materials are duplicated
	#$Body/Head.get_surface_material(0).set_albedo(active_color) # Color(1, 0, 0))

func set_color_inactive():
	$Body/Head.get_surface_material(0).albedo_color = inactive_color # Color(0, 1, 0))
	# not neccessery when materials are duplicated
	#$Body/Head.get_surface_material(0).set_albedo(inactive_color) # Color(0, 1, 0))
