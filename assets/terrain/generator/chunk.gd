# Thanks to:
# - https://www.youtube.com/watch?v=rWeQ30h25Yg
# - https://github.com/nikkiluzader/godot-3d-procedural-generation
#
extends Spatial
class_name Chunk

var logchan := "chunk"

var generator: Spatial

var mesh_instance
var x: float
var z: float
var chunk_size: float
var subdivide: int
var noises: Array
var noises_heights: Array
var min_height: float = 100000
var max_height: float = -100000

var should_remove: bool = false

var sea
var terrain
# Load values from generator or override
var sea_material # = preload("res://assets/materials/colors/lightblue.tres")
var terrain_material # = preload("res://assets/materials/shaders/grid.tres")

onready var space_state := get_world().direct_space_state
#var data

func _init(_generator: Spatial, _x: int, _z: int, _chunk_size: float, _subdivide: int, _noises: Array, _noises_heights: Array):
	generator = _generator
	x = _x * _chunk_size
	z = _z * _chunk_size
	chunk_size = _chunk_size
	subdivide = _subdivide
	noises = _noises
	noises_heights = _noises_heights
	translation.x = x
	translation.z = z
	translation.y = 0
	name = "Chunk-" + str(x) + "-" + str(z)
	Log.debug("%d,%d [%.1f,%.1f] (size: %.1f, subdivide: %d)" % [_x, _z, x, z, chunk_size, subdivide], logchan)

func _ready():
	if generator.options.has(generator.GENERATE_SEA):
		generate_sea()
	if generator.options.has(generator.GENERATE_TERRAIN):
		#yield(generate_terrain(), "completed")
		generate_terrain()

func ray_terrain(_x: float, _z: float): # -> false or Vector3:
	_x += x
	_z += z
	#var position = Vector3(_x, 10000.0, _z)
	#var chunk_ray = space_state.intersect_ray(position, position * Vector3(1, -1, 1), [Global.player])
	var ray = space_state.intersect_ray(Vector3(_x, 10000.0, _z), Vector3(_x, -10000.0, _z), [], 1)
	# @todo Sometimes error in chunk_ray: Invalid get index 'position' (on base: 'Dictionary')
	if !ray.has("position"):
		return false
	return ray

# _x_rel & _z_rel are relative -0.5 to 0.5 values to chunk_size
func ray_terrain_rel(_x_rel: float, _z_rel: float): # -> false or Vector3:
	return ray_terrain(_x_rel * chunk_size, _z_rel * chunk_size)

func ray_terrain_rand():
	return ray_terrain_rel(generator.rng.randf() - 0.5, generator.rng.randf() - 0.5)

func add_node_at(node: Spatial, loc = null, group = null): # loc: Vector3 = null, group: String = null
	node.visible = false
	add_child(node)
	if loc:
		node.translation = loc
	if group:
		node.add_to_group(group)
	node.visible = true

func get_terrain_material():
	if generator.terrain_material:
		return generator.terrain_material
	#return generate_terrain_material()

func generate_terrain_material():
	terrain_material = preload("res://assets/materials/shaders/splatmap.tres")
	#material.set_shader_param("splatmap", load("res://assets/materials/terrain/Gravel020/Gravel020_1K_Color.jpg"))
	var gravel = load("res://assets/materials/terrain/Gravel020/Gravel020_1K_Color.jpg")
	#var gravel = load("user://textures/data/1K-JPG/Gravel020/Gravel020_1K_Color.jpg")
	terrain_material.set_shader_param("splatmap", gravel)
	terrain_material.set_shader_param("tex_r", gravel)
	terrain_material.set_shader_param("tex_g", gravel)
	terrain_material.set_shader_param("tex_b", gravel)
	return terrain_material

func set_terrain_material(_material):
	if terrain:
		#terrain.material_override = _material
		terrain.mesh.surface_set_material(0, _material)

func generate_terrain():
	var plane = PlaneMesh.new()
	plane.size = Vector2(chunk_size, chunk_size)
	plane.subdivide_depth = subdivide # chunk_size * 0.5
	plane.subdivide_width = subdivide # chunk_size * 0.5
	plane.material = terrain_material if terrain_material else get_terrain_material()

	var surface_tool = SurfaceTool.new()
	#surface_tool.add_smooth_group(true)
	#surface_tool.add_normal(Vector3.UP)
	#surface_tool.add_uv(Vector2.ZERO)
	#surface_tool.append_from(plane, 0, Transform.IDENTITY)
	surface_tool.create_from(plane, 0) # create_from(existing: Mesh, surface: int) -> void: Creates a vertex array from an existing Mesh.
	var mesh: ArrayMesh = surface_tool.commit() # commit(existing: ArrayMesh = null, flags: int = 97280)

	var mdt = MeshDataTool.new()
	mdt.create_from_surface(mesh, 0) # create_from_surface(mesh: ArrayMesh, surface: int) -> Error
	for i in range(mdt.get_vertex_count()):
		var vertex = mdt.get_vertex(i)
		#vertex.y = noise.get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * 50 # get_noise_3d(x: float, y: float, z: float) -> float
		#vertex.y += noises[0].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * 50 # get_noise_3d(x: float, y: float, z: float) -> float

		#for noise in noises:
		#	vertex.y += noise.get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * noise.period / 2.0 # get_noise_3d(x: float, y: float, z: float) -> float

		var control_height = noises[0].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z)
		if control_height < 0:
			control_height /= 2.0
		var control_height_square = control_height * control_height
		vertex.y += noises[1].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * noises_heights[1] * control_height_square
		vertex.y += noises[2].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * noises_heights[2] * control_height_square

		vertex.y += noises[3].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * noises_heights[3]
#		vertex.y += noises[4].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * noises_heights[4] # * control_height # 10000 # * control_height_square
		#vertex.y += 200
#		if vertex.y > -50 and vertex.y < 100:
#			vertex.y = randf()

		#	vertex.y += -control_height * 10000 # randf()
		#elif control_height < -0.5:
		#	vertex.y += control_height # noises[0].period
		#elif control_height < -0.25:
		#	vertex.y += noises[1].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * noises[1].period * -control_height * 4
		#elif control_height < 0.0:
		#	vertex.y += noises[1].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * noises[1].period
		#elif control_height < 0.25:
		#	vertex.y += (noises[1].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * noises[1].period + noises[2].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * noises[2].period) / 2
		#elif control_height < 0.5:
		#	vertex.y += noises[2].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * noises[2].period
		#else:
		#	vertex.y += noises[2].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * noises[2].period * control_height * 2
		# @todo difference between get_noise_3d() & get_noise_2d() ??

#		if control_height < 0.0:
#			vertex.y = -control_height * 20
#		#elif control_height < 0.5:
#		#	vertex.y = control_height * 20
#		elif control_height < 0.0:
#			vertex.y += noises[1].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * noises[1].period
#		elif control_height < 0.5:
#			vertex.y += (noises[1].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * noises[1].period + noises[2].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * noises[2].period) / 2
#		else:
#			vertex.y += noises[2].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * noises[2].period
#			#vertex.inverse()

		mdt.set_vertex(i, vertex)
		min_height = min(min_height, vertex.y)
		max_height = max(max_height, vertex.y)

	Log.debug("min_height: " + str(min_height) + ", max_height: " + str(max_height), logchan)

	# Thanks! https://godotengine.org/qa/69339/regenerate-normals-after-meshdatatool-vertex-adjustments
#	for face in mdt.get_face_count():
#		# Set the face's normal as the normal for one of its points
#		var vertex = mdt.get_face_vertex(face, 0)
#		var normal = mdt.get_face_normal(face)
#		print(vertex, " :: ", normal)
#		mdt.set_vertex_normal(vertex, normal)

	var vertices = PoolVector3Array()
	var normals = PoolVector3Array()
	var uvs = PoolVector2Array()
	for face in mdt.get_face_count():
		var normal = mdt.get_face_normal(face)
		for vertex in range(0, 3):
			var face_vertex = mdt.get_face_vertex(face, vertex)
			vertices.push_back(mdt.get_vertex(face_vertex))
			uvs.push_back(mdt.get_vertex_uv(face_vertex))
			normals.push_back(normal)

	#for s in range(mesh.get_surface_count()):
	#	mesh.surface_remove(s)
	mesh.surface_remove(0)

	mdt.commit_to_surface(mesh)
	surface_tool.begin(Mesh.PRIMITIVE_TRIANGLES)
	#surface_tool.begin(Mesh.PRIMITIVE_TRIANGLE_STRIP)
	#surface_tool.add_smooth_group(true)
	surface_tool.create_from(mesh, 0)
	#surface_tool.generate_normals()
	#surface_tool.generate_tangents()

	terrain = MeshInstance.new()
	#terrain.add_to_group("Terrain")
	terrain.visible = false
	terrain.mesh = surface_tool.commit()
	add_child(terrain)
	terrain.visible = true
	terrain.create_trimesh_collision()
	#terrain.cast_shadow = GeometryInstance.SHADOW_CASTING_SETTING_OFF

	#yield(get_tree().create_timer(1.0), "timeout")
	if generator.options.has(generator.GENERATE_TREES):
		#call_deferred("generate_trees")
		generate_trees()
	if generator.options.has(generator.GENERATE_BUILDINGS):
		generate_buildings()
	if generator.options.has(generator.GENERATE_TOWERS):
		generate_towers()

func remove_terrain():
	if terrain:
		terrain.queue_free()
		terrain = null

func get_sea_material():
	if generator.sea_material:
		return generator.sea_material

func set_sea_material(_material):
	if sea:
		#sea.material_override = _material
		sea.mesh.surface_set_material(0, _material)

func generate_sea():
	var plane = PlaneMesh.new()
	plane.size = Vector2(chunk_size, chunk_size)
	plane.subdivide_depth = subdivide # chunk_size * 0.5
	plane.subdivide_width = subdivide # chunk_size * 0.5
	plane.material = sea_material if sea_material else get_sea_material()

	sea = MeshInstance.new()
	sea.mesh = plane
	sea.translation.y = generator.sea_level # faster than loc: Vector3 in add_node_at()
	add_node_at(sea)

func remove_sea():
	if sea:
		sea.queue_free()
		sea = null

func generate_corner():
	var ray = ray_terrain_rel(-0.5, -0.5)
	if !ray:
		return
	var tree = generator.tree_scene.instance()
	add_child(tree)

func generate_buildings():
	generator.rng.seed = hash(x * 8192 + z + 100)
	if generator.rng.randi() % 100 > 50:
		return

	var slope_density = pow(chunk_size / (max_height - min_height), 2)
	var avg_height = (max_height - min_height) / 2 + min_height
	var height_density = ease(avg_height / 20, -0.2)
	var amount := int(min(10, slope_density * height_density / 20))
	Log.debug("amount: " + str(amount) + ", height_density: " + str(height_density), "chunk.buildings")
	for _num in range(amount):
		var ray = ray_terrain_rand()
		if !ray:
			continue

		var size = 20 + (2000 * ease(generator.rng.randf(), -0.2))
		var floors = 1 + (generator.rng.randi() % 10)
		size /= floors
		Log.debug("Size: " + str(size) + ", Floors: " + str(floors), "chunk.building")
		var width = sqrt(size) # size / (5 + generator.rng.randi() % 5)
		var depth = size / width
		var floor_height = 2 + 3 * generator.rng.randf()
		var roof_height = 1 + (generator.rng.randi() % 4)
		var building = Builder.new(Architect.simple_house(width, depth, floors, floor_height, roof_height)) # Vector3(width, 10, depth))
		building.name = "Building" + str(_num)
		#building.rotation = Vector3(0, chunk_ray.normal.y, 0)
		add_node_at(building, to_local(ray["position"]), "Buildings")

func remove_buildings():
	for node in get_tree().get_nodes_in_group("Buildings"):
		node.queue_free()

func generate_towers():
	generator.rng.seed = hash(x * 8192 + z + 1001)
	if generator.rng.randi() % 100 > 5:
		return
	var ray = ray_terrain_rand()
	if !ray:
		return

	var tower = generator.tower_scene.instance()

	var distance = ray["position"].length()
	# Power of 2 each 3000m and skip first 3000m to shift too small powers on start
	var power = (distance * pow(2, int(distance / 3000)) + 3000) / 30
	# Randomness 50-200%
	power *= lerp(0.666, 1.333, generator.rng.randf())
	tower.pre_stats["power"] = power

	var position := to_local(ray["position"])
	Log.info(str(tower) + " at " + str(position) + ": power=" + str(power), logchan)
	tower.scale = Vector3.ONE * (1 + log(power))
	add_node_at(tower, position, "Towers")
	#tower.stats.init_power(power)
	#tower.update_label()
	#tower.label.text = str("dist: %.1f\npow: %.1f" % [distance, tower.power])

func remove_towers():
	for node in get_tree().get_nodes_in_group("Towers"):
		node.queue_free()

func generate_trees():
	# Skip generating trees for chunks with max_height lower than 15m
	if max_height < 15.0:
		return
	generator.rng.seed = hash(x * 8192 + z)
#	var chunk_ray = space_state.intersect_ray(Vector3(x, 10000, z), Vector3(x, -10000.0, z), [Global.player])
#	if !chunk_ray.has("position"):
#		return
	#if (chunk_ray["position"].y < 20 or chunk_ray["position"].y > 200):
	#	return false

	#if (chunk_ray["position"].y < 20):
	#	density = 0.04
	#else:
	#	density = 0.01

	# Density as amount of trees (or raycasts) per hectare (100x100m)
	#var max_density = 10
	var rand_density = ease(generator.rng.randf(), -0.2) # * 100
	var height_density = (-0.5 * pow(min_height, 2) + 1000*max_height) / 10000
	var slope_density = pow(chunk_size / (max_height - min_height), 2)
	var density = 10 # min(max_density, rand_density * height_density * slope_density)
	Log.debug("height: " + str(min_height) + " / " + str(max_height) + ", density: " + str(density) + " (height " + str(height_density) + ", slope " + str(slope_density) + ", rand " + str(rand_density) + ")", "chunk.trees")
	if density <= 0:
		return false

	var amount := int(density * chunk_size * chunk_size / 1e4) # density/ha * m * m / 1e6
	Log.debug("amount: " + str(amount), "chunk.trees")
	for i in range(amount): # / 250): #chunk_ray["position"].y):		# range(chunk_size * chunk_size / 100.0):
		var tree_ray = ray_terrain_rand()
		if !tree_ray or tree_ray["position"].y < 5.0: # or tree_ray["position"].y > 500.0:
			continue
		Log.debug(str(tree_ray), "chunk.tree")
		var tree = generator.tree_scene.instance()
		tree.name = "Tree" + str(i)
		#tree.visible = false
		var scale = generator.rng.randf() * 5.0 # 10.0
		#tree.scale = Vector3(scale, scale, scale)
		var hscale = scale * (generator.rng.randf() * 0.5 + 0.75)
		var vscale = scale * (generator.rng.randf() * 0.5 + 0.75)
		tree.scale = Vector3(hscale, vscale, hscale)
		tree.rotation.y = generator.rng.randf() * TAU
		add_node_at(tree, to_local(tree_ray["position"]) - Vector3(0, hscale * 0.5, 0), "Trees")

func remove_trees():
	for node in get_tree().get_nodes_in_group("Trees"):
		node.queue_free()
