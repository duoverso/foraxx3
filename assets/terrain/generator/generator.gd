#tool
extends Spatial
class_name TerrainGenerator

var logchan := "generator"

export var enabled: bool = true setget set_enabled
enum {
	GENERATE_TERRAIN, RELOAD_TERRAIN_MATERIAL_ON_CHANGE,
	GENERATE_SEA, RELOAD_SEA_MATERIAL_ON_CHANGE,
	GENERATE_TREES, RELOAD_TREES_ON_CHANGE,
	GENERATE_BUILDINGS,
	GENERATE_TOWERS
}
# Is the same as:
#const GENERATE_TERRAIN = 0
#const RELOAD_TERRAIN_ON_CHANGE = 1
var options := [
	GENERATE_TERRAIN, RELOAD_TERRAIN_MATERIAL_ON_CHANGE,
	GENERATE_SEA, RELOAD_SEA_MATERIAL_ON_CHANGE,
	GENERATE_TREES, RELOAD_TREES_ON_CHANGE,
	GENERATE_BUILDINGS,
	GENERATE_TOWERS
]

export var chunk_size: float = 100.0 setget set_chunk_size, get_chunk_size
var chunk_size_half: float
# Chunk Subdivide (in meters) independent of chunk_size
export var chunk_subdivide_per_size: float = 10.0 setget set_chunk_subdivide_per_size
# Cached chunk_size / chunk_subdivide_per_size by set_chunk_subdivide_per_size() and used for chunks with any chunk_size
var chunk_subdivide: int
export var chunk_dist: int = 5
export var from_center: bool = true
export var circular: bool = true

var chunks := {}
var unready_chunks := {}

#var mutex := Mutex.new()
#var semaphore := Semaphore.new()
var thread := Thread.new()
var exit_thread := false

# RNG should be probably per each thread (to generate same seeds for each chunk)
var rng := RandomNumberGenerator.new()

var update_timer:float = 100.0 # initial value high enough to trigger _process() at first frame

var last_pos: Vector3
var last_p_x: int
var last_p_z: int

var noises := []
var noises_heights := []

var available_terrain_materials := {
	"None": null,
	"Transparent": "res://assets/materials/transparent.material",
	"DarkGreen": "res://assets/materials/colors/darkgreen.tres",
	"Heightmap": "res://assets/materials/shaders/heightmap.tres",
	"Splatmap": "res://assets/materials/shaders/splatmap.tres",
	"SplatHeightmap": "res://assets/materials/shaders/splatheightmap.tres",
	"Grid": "res://assets/materials/shaders/grid.tres",
	"Carbon Fiber": "res://assets/materials/shaders/carbon-fiber.material",
	"Window Rain": "res://assets/materials/shaders/window-rain.material",
	"Gravel020": "res://assets/materials/terrain/Gravel020/Gravel020.tres",
	"Ground047": "res://assets/materials/terrain/Ground047/Ground047.tres",
	"Snow002": "res://assets/materials/terrain/Snow002/Snow002.tres",
}
var terrain_material_name := "SplatHeightmap" setget set_terrain_material_name
var terrain_material

var available_sea_materials := {
	"None": null,
	"Transparent": "res://assets/materials/transparent.material",
	"Water (Material)": "res://assets/materials/Water.material",
	"LightBlue": "res://assets/materials/colors/lightblue.tres",
	"Water (Shader)": "res://assets/materials/shaders/water.tres",
	"Water (GLES2)": "res://assets/materials/shaders/water_gles2.material",
	"Acid": "res://assets/materials/shaders/acid.tres",
}
var sea_material_name := "LightBlue" setget set_sea_material_name
var sea_material
export var sea_level := 0.0 setget set_sea_level
var sea_level_to setget set_sea_level_to # null for not changing, float for new level
export var sea_level_speed := 100.0 setget set_sea_level_speed # units/s

var available_tree_scenes := {
	"None": null,
	"Trunk": "res://assets/models/nature/tree/trunk.tscn",
	"Simple": "res://assets/models/nature/tree/simple.tscn",
	"Pine": "res://assets/models/nature/tree/pine/pine.tscn",
	"Pine (Non-Transparent)": "res://assets/models/nature/tree/pine/pine-non-transparent.tscn",
}
var tree_scene_name := "Simple" setget set_tree_scene_name
var tree_scene

var tower_scene

var init_time: int

#signal chunk_loaded

func _enter_tree():
	init_time = Time.get_ticks_usec()
	Log.verbose("enter_tree() at: " + get_path(), logchan)

func set_enabled(value: bool) -> void:
	enabled = value

########## Chunks ##########
func _ready() -> void:
	Log.verbose("_ready() after " + Log.str_usec_diff(init_time), logchan)
	#if not enabled: # or Engine.editor_hint:
	#	set_process(false)
	# Init chunk_size_half by chunk_size and chunk_subdivide by chunk_subdivide_per_size
	set_chunk_size(chunk_size)
	set_chunk_subdivide_per_size(chunk_subdivide_per_size)


	# Init assets
	if available_terrain_materials.has(terrain_material_name) and available_terrain_materials[terrain_material_name]:
		terrain_material = load(available_terrain_materials[terrain_material_name])
	if available_sea_materials.has(sea_material_name) and available_sea_materials[sea_material_name]:
		sea_material = load(available_sea_materials[sea_material_name])
	tower_scene = load("res://assets/models/mobs/nest/tower.tscn")
	if available_tree_scenes.has(tree_scene_name) and available_tree_scenes[tree_scene_name]:
		tree_scene = load(available_tree_scenes[tree_scene_name])

	#randomize()
	# see https://docs.godotengine.org/en/stable/classes/class_opensimplexnoise.html
	#noise.seed = 3 # randi()
	# Number of OpenSimplex noise layers that are sampled to get the fractal noise. Higher values result in more detailed noise but take more time to generate.
	#noise.octaves = 2  # 1-9, default: 3
	#noise.period = 80

	### Presets
	# islands: [{"o":2, "p":1000}, {"o":3, "p":5000}, {"o":2, "p":1000}]
	#	var control_height = noises[0].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z)
	#	vertex.y += noises[1].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * 2500 * control_height
	#	vertex.y += noises[2].get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * 2500 * control_height

	# add_noise(seed, octaves, period, height)
	add_noise(1, 2, 1000, 1000)
	add_noise(2, 2, 5000, 2000)
	add_noise(3, 2, 1000, 2000)
	add_noise(4, 2, 10000, 1000)
#	add_noise(5, 2, 100, 10)

	# update_chunks() on ready
	if enabled:
		update_chunks()
	#add_chunk(0, 0)

#	var plane_material = preload("res://assets/materials/colors/lime.tres")
#	for i in range(-10, 10):
#		for j in range(-10, 10):
#			var plane = PlaneMesh.new()
#			plane.size = Vector2(2.0, 2.0)
#			#plane.subdivide_depth = chunk_size * 0.5
#			#plane.subdivide_width = chunk_size * 0.5
#			plane.material = plane_material
#			var mi = MeshInstance.new()
#			mi.translation = Vector3(i*10, 50, j*10)
#			mi.mesh = plane
#			add_child(mi)

func set_chunk_size(_chunk_size: float) -> void:
	Log.put("set_chunk_size(" + str(_chunk_size) + ")", logchan)
	chunk_size = _chunk_size
	chunk_size_half = ceil(_chunk_size / 2.0)

func get_chunk_size() -> float:
	return chunk_size

func set_chunk_subdivide_per_size(new_chunk_subdivide_per_size: float) -> void:
	Log.put("set_chunk_subdivide_per_size(" + str(new_chunk_subdivide_per_size) + ")", logchan)
	chunk_subdivide_per_size = new_chunk_subdivide_per_size
	chunk_subdivide = int(chunk_size / new_chunk_subdivide_per_size)

func _process_chunks():
	#Log.put("_process_chunks()", logchan)
	if thread.is_active() or unready_chunks.empty():
		return
	var key = unready_chunks.keys()[0]
	var xz = key.split(",", true, 1)
	thread.start(self, "load_chunk", xz, Thread.PRIORITY_LOW)

func load_chunk(xz) -> Chunk:
	var x = int(xz[0])
	var z = int(xz[1])
	Log.debug("load_chunk(" + str(xz) + ")", logchan)
	var chunk = Chunk.new(self, x, z, chunk_size, chunk_subdivide, noises, noises_heights)
	call_deferred("load_done", xz)
	return chunk

func load_done(xz) -> void:
	Log.debug("load_done(" + str(xz) + ")", logchan)
	var chunk = thread.wait_to_finish()
	#call_deferred("emit_signal", "chunk_loaded")
	#chunk.translation = Vector3(chunk.x, 0, chunk.z)
	#chunk.visible = true
	add_child(chunk)
	var key = str(xz[0]) + "," + str(xz[1])
	chunks[key] = chunk
	if unready_chunks.has(key):
		unready_chunks.erase(key)

	# Continue with next unready chunk
	yield(get_tree().create_timer(0.1), "timeout")
	call_deferred("_process_chunks")
	#_process_chunks()

func _process(delta) -> void:
	# Gradually changing sea_level to non-null sea_level_to by non-zero sea_level_speed
	if sea_level_to != null:
		if abs(sea_level_to - sea_level) < delta * sea_level_speed:
			set_sea_level(sea_level_to)
			sea_level_to = null
		else:
			set_sea_level(sea_level + (delta if sea_level < sea_level_to else -delta) * sea_level_speed)

	update_timer += delta
	if update_timer < 0.1:
		return

	# Only when Player's position changed
	if last_pos != Global.player.translation:
		last_pos = Global.player.translation
		var p_x := int(ceil((last_pos.x - chunk_size_half) / chunk_size))
		var p_z := int(ceil((last_pos.z - chunk_size_half) / chunk_size))
		Log.debug("_process(" + str(p_x) + ", " + str(p_z) + "): pos.x=" + str(last_pos.x) + ", pos.z=" + str(last_pos.z), logchan)
		# Only when Player's chunk position changed
		if p_x != last_p_x or p_z != last_p_z:
			last_p_x = p_x
			last_p_z = p_z
			reset_chunks()
			update_chunks_at(p_x, p_z)
			clean_up_chunks()
	# Wake-up worker
	#call_deferred("_process_chunks")
	#_process_chunks()
	update_timer = 0.0

func update_chunks() -> void:
	Log.debug("update_chunks() started", logchan)
	last_pos = Global.player.translation
	last_p_x = int(ceil((last_pos.x - chunk_size_half) / chunk_size))
	last_p_z = int(ceil((last_pos.z - chunk_size_half) / chunk_size))
	reset_chunks()
	update_chunks_at(last_p_x, last_p_z)
	clean_up_chunks()

func update_chunks_at(p_x, p_z) -> void:
	Log.debug("update_chunks_at(" + str(p_x, ", ", p_z) + ") started with pos.x=" + str(last_pos.x) + ", pos.z=" + str(last_pos.z), logchan)
	if not from_center:
		for x in range(-chunk_dist, chunk_dist + 1):
			for z in range(-chunk_dist, chunk_dist + 1):
				if not circular or sqrt(x*x + z*z) <= chunk_dist:
					add_chunk(p_x + x, p_z + z)
	else:
		for rect_dist in range(0, chunk_dist + 1):
			for x in range(-rect_dist, rect_dist + 1):
				for z in range(-rect_dist, rect_dist + 1):
					if not circular or sqrt(x*x + z*z) <= chunk_dist:
						add_chunk(p_x + x, p_z + z)
	call_deferred("_process_chunks")

func add_chunk(x, z) -> void:
	var key := str(x, ",", z)
	#Log.debug("add_chunk(" + key + ")", logchan)
	if chunks.has(key):
		# Unselect chunks in range... to not be removed
		var chunk = chunks.get(key)
		if chunk != null:
			chunk.should_remove = false
		return
	if unready_chunks.has(key):
		return
	Log.put("add chunk: " + key, logchan)
	unready_chunks[key] = 1

# Select all chunks to be removed
func reset_chunks() -> void:
	Log.put("reset_chunks()", logchan)
	unready_chunks.clear()
	for key in chunks:
		chunks[key].should_remove = true

# Remove all chunks still selected to be removed
func clean_up_chunks() -> void:
	Log.put("clean_up_chunks()", logchan)
	for key in chunks.keys():
		var chunk = chunks.get(key)
		if chunk.should_remove:
			Log.debug("remove chunk: " + key, logchan)
			chunks.erase(key)
			chunk.queue_free()

func force_update_chunks() -> void:
	reset_chunks()
	clean_up_chunks()
	update_chunks()

########## Terrain ##########
func generate_terrain():
	Log.put("generate_terrain()", logchan)
	for key in chunks:
		var chunk = chunks.get(key)
		chunk.generate_terrain()

func remove_terrain():
	Log.put("remove_terrain()", logchan)
	for key in chunks:
		var chunk = chunks.get(key)
		chunk.remove_terrain()

func enable_terrain(generate := true):
	Log.put("enable_terrain(" + str(generate) + ")", logchan)
	if not options.has(GENERATE_TERRAIN):
		Log.info("Terrain enabled", logchan)
		options.append(GENERATE_TERRAIN)
	if generate:
		generate_terrain()

func disable_terrain(remove := true):
	Log.put("disable_terrain(" + str(remove) + ")", logchan)
	if options.has(GENERATE_TERRAIN):
		Log.info("Terrain disabled", logchan)
		options.erase(GENERATE_TERRAIN)
	if remove:
		remove_terrain()

func add_noise(noise_seed: int, octaves: int, period: int, height: float):
	var noise := OpenSimplexNoise.new()
	noise.seed = noise_seed
	noise.octaves = octaves
	noise.period = period
	noises.append(noise)
	noises_heights.append(height)

func set_reload_terrain_material_on_change(reload_on_change: bool):
	Log.put("set_reload_terrain_material_on_change(" + str(reload_on_change) + ")", logchan)
	if not reload_on_change: # Disable
		options.erase(RELOAD_TERRAIN_MATERIAL_ON_CHANGE)
	elif not options.has(RELOAD_TERRAIN_MATERIAL_ON_CHANGE): # Enable
			options.append(RELOAD_TERRAIN_MATERIAL_ON_CHANGE)

func set_terrain_material_name(new_terrain_material_name: String) -> void:
	Log.put("set_terrain_material_name(" + new_terrain_material_name + ")", logchan)
	if not available_terrain_materials.has(new_terrain_material_name):
		#set_terrain_material(null)
		Log.err("Unknown material name: " + str(new_terrain_material_name), logchan)
		return
	terrain_material_name = new_terrain_material_name
	var new_material = available_terrain_materials[terrain_material_name]
	set_terrain_material(load(new_material) if new_material is String else new_material)

func set_terrain_material(new_terrain_material):
	Log.put("set_terrain_material(" + str(new_terrain_material) + ")", logchan)
	terrain_material = new_terrain_material

#	if terrain_material != null:
#		if not options.has(GENERATE_TERRAIN):
#			options.append(GENERATE_TERRAIN)
#			Log.info("Terrain Material enabled", logchan)
#	else:
#		if options.has(GENERATE_TERRAIN):
#			options.erase(GENERATE_TERRAIN)
#			Log.info("Terrain Material disabled", logchan)

	if options.has(RELOAD_TERRAIN_MATERIAL_ON_CHANGE):
		for key in chunks:
			var chunk = chunks.get(key)
			chunk.set_terrain_material(terrain_material)

########## Sea ##########
func generate_sea():
	Log.put("generate_sea()", logchan)
	for key in chunks:
		var chunk = chunks.get(key)
		chunk.generate_sea()

func remove_sea():
	Log.put("remove_sea()", logchan)
	for key in chunks:
		var chunk = chunks.get(key)
		chunk.remove_sea()

func enable_sea(generate := true):
	Log.put("enable_sea()", logchan)
	if not options.has(GENERATE_SEA):
		Log.info("Sea enabled", logchan)
		options.append(GENERATE_SEA)
	if generate:
		generate_sea()

func disable_sea(remove := true):
	Log.put("disable_sea(" + str(remove) + ")", logchan)
	if options.has(GENERATE_SEA):
		Log.info("Sea disabled", logchan)
		options.erase(GENERATE_SEA)
	if remove:
		remove_sea()

func set_reload_sea_material_on_change(reload_on_change: bool):
	Log.put("set_reload_sea_material_on_change(" + str(reload_on_change) + ")", logchan)
	if not reload_on_change: # Disable
		options.erase(RELOAD_SEA_MATERIAL_ON_CHANGE)
	elif not options.has(RELOAD_SEA_MATERIAL_ON_CHANGE): # Enable
			options.append(RELOAD_SEA_MATERIAL_ON_CHANGE)

# @TODO: Almost duplicate of set_terrain_material_name()
func set_sea_material_name(new_sea_material_name: String) -> void:
	Log.put("set_sea_material_name(" + new_sea_material_name + ")", logchan)
	if not available_sea_materials.has(new_sea_material_name):
		#set_sea_material(null)
		Log.err("Unknown material name: " + str(new_sea_material_name), logchan)
		return
	sea_material_name = new_sea_material_name
	var new_material = available_sea_materials[sea_material_name]
	set_sea_material(load(new_material) if new_material is String else new_material)

func set_sea_material(new_sea_material):
	Log.put("set_sea_material(" + str(new_sea_material) + ")", logchan)
	sea_material = new_sea_material

#	if sea_material != null:
#		if not options.has(GENERATE_SEA):
#			options.append(GENERATE_SEA)
#			Log.info("Sea Material enabled", logchan)
#	else:
#		if options.has(GENERATE_SEA):
#			options.erase(GENERATE_SEA)
#			Log.info("Sea Material disabled", logchan)

	if options.has(RELOAD_SEA_MATERIAL_ON_CHANGE):
		for key in chunks:
			var chunk = chunks.get(key)
			chunk.set_sea_material(sea_material)

# Immediate change of sea_level
func set_sea_level(_sea_level: float):
	sea_level = _sea_level
	if Global.player:
		Global.player.water_level = sea_level

	for key in chunks:
		var chunk = chunks.get(key)
		if chunk.sea:
			chunk.sea.translation.y = sea_level

# Gradually changing sea_level to sea_level_to when sea_level_speed
func set_sea_level_to(new_sea_level_to):
	if sea_level_speed:
		sea_level_to = new_sea_level_to
		Log.info("Sea Level changing to: " + str(new_sea_level_to), logchan)
	else:
		set_sea_level(new_sea_level_to)
		Log.info("Sea Level changed to: " + str(new_sea_level_to), logchan)

func set_sea_level_speed(new_sea_level_speed):
	sea_level_speed = max(0.0001, new_sea_level_speed) if new_sea_level_speed else 0.0
	Log.info("Sea Level Speed changed to: " + str(sea_level_speed), logchan)
	# Change to zero speed means immediate propagation of sea_level_to to sea_level
	if not sea_level_speed and sea_level_to:
		set_sea_level(sea_level_to)
		sea_level_to = null

########## Trees ##########
func generate_trees():
	Log.put("generate_trees()", logchan)
	for key in chunks:
		var chunk = chunks.get(key)
		chunk.generate_trees()

func remove_trees():
	Log.put("remove_trees()", logchan)
	for key in chunks:
		var chunk = chunks.get(key)
		chunk.remove_trees()

func enable_trees(generate := true):
	Log.put("enable_trees(" + str(generate) + ")", logchan)
	if not options.has(GENERATE_TREES):
		Log.info("Trees enabled", logchan)
		options.append(GENERATE_TREES)
	if generate:
		generate_trees()

func disable_trees(remove := true):
	Log.put("disable_trees(" + str(remove) + ")", logchan)
	if options.has(GENERATE_TREES):
		Log.info("Trees disabled", logchan)
		options.erase(GENERATE_TREES)
	if remove:
		remove_trees()

func enable_reload_trees_on_change():
	Log.debug("enable_reload_trees_on_change()", logchan)
	options.append(RELOAD_TREES_ON_CHANGE)

func disable_reload_trees_on_change():
	Log.debug("disable_reload_trees_on_change()", logchan)
	options.erase(RELOAD_TREES_ON_CHANGE)

func set_tree_scene_name(_tree_scene_name: String):
	Log.put("set_tree_scene_name(" + _tree_scene_name + ")", logchan)
	tree_scene_name = _tree_scene_name

	if available_tree_scenes.has(tree_scene_name) and available_tree_scenes[tree_scene_name]:
		if options.has(RELOAD_TREES_ON_CHANGE):
			remove_trees()
		tree_scene = load(available_tree_scenes[tree_scene_name])
		enable_trees(options.has(RELOAD_TREES_ON_CHANGE))
	else:
		#tree_scene = null
		disable_trees(options.has(RELOAD_TREES_ON_CHANGE))

	print("tree_scene = ", tree_scene, " (RELOAD_TREES_ON_CHANGE: ", options.has(RELOAD_TREES_ON_CHANGE), ")")

########## Towers ##########
func generate_towers():
	for key in chunks:
		var chunk = chunks.get(key)
		chunk.generate_towers()

func remove_towers():
	for key in chunks:
		var chunk = chunks.get(key)
		chunk.remove_towers()

func enable_towers(generate := true):
	if not options.has(GENERATE_TOWERS):
		options.append(GENERATE_TOWERS)
	if generate:
		generate_towers()

func disable_towers(remove := true):
	if options.has(GENERATE_TOWERS):
		options.erase(GENERATE_TOWERS)
	if remove:
		remove_towers()

########## Buildings ##########
func generate_buildings():
	for key in chunks:
		var chunk: Chunk = chunks.get(key)
		chunk.generate_buildings()

func remove_buildings():
	for key in chunks:
		var chunk = chunks.get(key)
		chunk.remove_buildings()

func enable_buildings(generate := true):
	if not options.has(GENERATE_BUILDINGS):
		options.append(GENERATE_BUILDINGS)
	if generate:
		generate_buildings()

func disable_buildings(remove := true):
	if options.has(GENERATE_BUILDINGS):
		options.erase(GENERATE_BUILDINGS)
	if remove:
		remove_buildings()

func _exit_tree() -> void:
	# Stop the thread
	exit_thread = true
	# Remove all chunks
	reset_chunks()
	clean_up_chunks()
