extends Node
class_name Architect

const materials = {
	"walls": [
		"res://assets/materials/terrain/Gravel020/Gravel020.tres",
		"res://assets/materials/terrain/Ground047/Ground047.tres",
		"res://assets/materials/terrain/Snow002/Snow002.tres",
	]
}

static func wall(_position: Vector3) -> Dictionary:
	return {
		"width": abs(_position.x),
		"height": 4,
		"depth": 1,
		"material": "res://assets/materials/terrain/Gravel020/Gravel020.tres",
		"Wall": {"mesh": "cube", "collision": "cube"},
	}
	#var builder = Builder.new(building)
	#return builder

static func simple_house(width: float, depth: float, floors: int, floor_height: float, roof_height: float) -> Dictionary:
	return {
		"width": width,
		"height": floors * floor_height,
		"depth": depth,
		# "material": "res://assets/materials/terrain/Gravel020/Gravel020.tres",
		"material": materials["walls"][randi() % materials["walls"].size()],
		"Wall": {"mesh": "cube", "collision": "cube", "y": (floors * floor_height) / 2},
		"Roof": {"mesh": "prism", "collision": "prism", "y": (floors * floor_height) + roof_height / 2, "height": roof_height},
	}
