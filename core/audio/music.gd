# @TODO: Download music
extends Node

var _loaded := false
var logchan := "music"

var settings_section := "music"
var settings_properties := ["use_stream_player", "user_music_dirs", "use_midi_player", "midi_player_soundfont", "user_midi_dirs", "is_playing", "has_shuffle", "track_number", "bus_volume"]

export var use_stream_player := true
#export var music_dirs := ["res://music", "res://assets/music", "res://assets/audio/music"]
export var music_dirs := ["res://assets/audio/music"]
#export var user_music_dirs := ["user://music", "user://repos/antray/music"]
export var user_music_dirs := ["user://music"]
#var tracks := []
var tracks := [
#	"res://assets/audio/midi/doom-2/The Waste Tunnels.mid",
#	"res://assets/audio/midi/doom-2/The Spirit World.mid",
#	"res://assets/audio/midi/doom-2/Underhalls.mid",
#	"res://assets/audio/midi/doom-2/The Crusher.mid",
#	"res://assets/audio/midi/doom-2/Bloodfalls.mid",
#	"res://assets/audio/midi/doom-2/The Pit.mid",
#	"res://assets/audio/midi/doom-2/Icon of Sin.mid",
#	"res://assets/audio/midi/doom-2/Barrels 'o' Fun.mid",
#	"res://assets/audio/midi/doom-2/Tricks and Traps.mid",
#	"res://assets/audio/midi/doom-2/Dead Simple.mid",
#	"res://assets/audio/midi/abuse/ABUSE17.MID",
#	"res://assets/audio/midi/abuse/ABUSE04.MID",
#	"res://assets/audio/midi/abuse/ABUSE11.MID",
#	"res://assets/audio/midi/abuse/VICTORY.MID",
#	"res://assets/audio/midi/abuse/INTRO.MID",
#	"res://assets/audio/midi/abuse/ABUSE02.MID",
#	"res://assets/audio/midi/abuse/ABUSE15.MID",
#	"res://assets/audio/midi/abuse/ABUSE01.MID",
#	"res://assets/audio/midi/abuse/ABUSE09.MID",
#	"res://assets/audio/midi/abuse/ABUSE03.MID",
#	"res://assets/audio/midi/abuse/ABUSE06.MID",
#	"res://assets/audio/midi/abuse/ABUSE13.MID",
#	"res://assets/audio/midi/abuse/ABUSE08.MID",
#	"res://assets/audio/midi/abuse/ABUSE00.MID",
#	"res://assets/audio/midi/doom-2/ENTRYWAY.MID",
#	"res://assets/audio/midi/doom-2/DOOM95.MID",
#	"res://assets/audio/midi/doom-2/GOTCHA!.MID",
#	"res://assets/audio/midi/doom-2/GROSSE.MID",
#	"res://assets/audio/midi/doom-2/DOOM2I~1.MID",
#	"res://assets/audio/midi/doom-2/DOOM2T~1.MID",
#	"res://assets/audio/midi/doom-2/GANTLET.MID",
]
onready var music_player = $AudioStreamPlayer

export var use_midi_player := true
export var midi_player_soundfont := "res://assets/audio/soundfont/Aspirin-Stereo.sf2"
export var midi_dirs := ["res://assets/audio/midi"]
export var user_midi_dirs := ["user://midi"]
var midi_player

export var is_playing := true setget set_is_playing
export var track_number := 0
export var has_shuffle := true

export var bus_name := "Music"
var bus := AudioServer.get_bus_index(bus_name)
export var bus_volume: float setget set_bus_volume # 0 to 1.0

func set_is_playing(new_is_playing: bool) -> void:
	if is_playing == new_is_playing:
		return
	toggle_playing()

func set_bus_volume(new_volume: float) -> void:
	Log.verbose("set_bus_volume(" + str(new_volume) + ")", logchan)
	bus_volume = new_volume
	AudioServer.set_bus_volume_db(bus, linear2db(new_volume))

func _enter_tree() -> void:
	Log.verbose("_enter_tree() at: " + get_path(), logchan)
	if _loaded:
		Log.err("Error: Music is an AutoLoad singleton and it shouldn't be instanced elsewhere.", logchan)
		Log.err("Please delete the instance at: " + get_path(), logchan)
	else:
		_loaded = true
	Settings.connect("settings_loaded", self, "load_settings")
	Log.verbose("Settings.connect(settings_loaded) to load_settings()", logchan)
	Settings.connect("save_settings", self, "save_settings")
	Log.verbose("Settings.connect(save_settings) to save_settings()", logchan)
	bus_volume = db2linear(AudioServer.get_bus_volume_db(bus))

func load_settings() -> void:
	Settings.load_to(self, settings_section, settings_properties)

func save_settings() -> void:
	Settings.save_from(self, settings_section, settings_properties)

#func _get_property_list():
#	return [
#		{ name = "MIDI", type = TYPE_NIL, hint_string = "midi_", usage = PROPERTY_USAGE_GROUP }
#	]

func _ready() -> void:
	Log.verbose("_ready()", logchan)
	load_settings()

	randomize() # for shuffle
	load_tracks()

#	if use_stream_player:
#		if is_playing and not tracks.empty():
#	#			if has_shuffle:
#	#				play_next()
#	#			else:
#			play_track(track_number)

	if use_midi_player:
		midi_player = MidiPlayer.new()
		#midi_player = {}
		midi_player.name = "MidiPlayer"
		midi_player.load_all_voices_from_soundfont = false
		midi_player.soundfont = midi_player_soundfont
		midi_player.volume_db = -20 # 0 = toooo loud, -70 = too quiet
		midi_player.tempo = 120
		midi_player.connect("finished", self, "_on_finished")
		Log.debug("MidiPlayer " + str(midi_player, " with ", midi_player.soundfont), logchan)
		add_child(midi_player)

#		if is_playing:
#			midi_player.set_file("res://assets/audio/midi/abuse/INTRO.MID")
#			Log.debug("MidiPlayer.play()", logchan)
#			#midi_player.playing = true
#			midi_player.play()
#		#midi_player.stop()

	if is_playing and not tracks.empty():
#			if has_shuffle:
#				play_next()
#			else:
		play_track(track_number)

func _unhandled_input(event: InputEvent) -> void:
	#Log.verbose("_unhandled_input( " + str(event) + " )", logchan)
	if event.is_action("music_fast_forward"):
		music_player.pitch_scale = 4 if event.is_pressed() else 1
	elif event.is_pressed():
		if event.is_action("music_toggle_playing"):
			toggle_playing()
		elif event.is_action("music_next"):
			play_next()
		elif event.is_action("music_toggle_shuffle"):
			has_shuffle = not has_shuffle
		else:
			return
		Log.debug("handled " + str(event), logchan)
		get_tree().set_input_as_handled()

func play_next() -> int:
	if tracks.empty():
		stop()
		return -1
	if has_shuffle:
		track_number = randi() % tracks.size()
	else:
		track_number += 1
		if (track_number >= tracks.size()):
			track_number = 0
	play_track(track_number)
	return track_number

func set_shuffle(new_value : bool) -> void:
	Log.info("Shuffle " + str(new_value), logchan)
	has_shuffle = new_value

func toggle_shuffle() -> bool:
	set_shuffle(!has_shuffle)
	return has_shuffle

func stop() -> void:
	Log.info("Stopped", logchan)
	music_player.stop()

func toggle_playing() -> bool:
	is_playing = not is_playing
	if music_player:
		if is_playing:
			play_track(track_number)
		else:
			stop()
	return is_playing

func load_tracks_from_path(path: String, expr := "*.import", recursive := true) -> Array:
	var path_tracks := []
	var dir := Directory.new()

	if !dir.dir_exists(path):
		Log.err("Music dir '" + path + "' doesn't exist.", logchan)
		return []
#		var error = dir.make_dir(path)
#		if error != OK:
#			Log.err("Error '" + str(error) + "' when creating music dir '" + path + "'", logchan)

	var error: int
	error = dir.open(path)
	if error != OK:
		Log.err("An error " + str(error) + " occurred when looking up for music tracks at '" + path + "'", logchan)
		return path_tracks

	error = dir.list_dir_begin(true)
	if error != OK: # skip_navigational (. and ..) = true
		Log.err("An error " + str(error) + " occurred in Directory.list_dir_begin() at : '" + path + "'", logchan)
		return path_tracks

	var file_name := dir.get_next()
	while file_name != "":
		if dir.current_is_dir():
			if recursive:
				path_tracks.append_array(load_tracks_from_path(path + "/" + file_name, expr))
		# Only .import files are read on Android and HTML5
		elif file_name.match(expr):
			file_name = file_name.trim_suffix(".import")
			# @TODO: Check for empty or corrupted files?
			path_tracks.append(path + "/" + file_name)
		file_name = dir.get_next()
	#dir.list_dir_end() # Autoclosed?

	return path_tracks

func load_tracks() -> void:
	tracks = []

	if use_stream_player:
		if "music_dirs" in self:
			for path in self.music_dirs:
				tracks.append_array(load_tracks_from_path(path))
		if "user_music_dirs" in self:
			for path in self.user_music_dirs:
				tracks.append_array(load_tracks_from_path(path))

	if use_midi_player:
		if "midi_dirs" in self:
			for path in midi_dirs:
				tracks.append_array(load_tracks_from_path(path, "*.mid"))
				tracks.append_array(load_tracks_from_path(path, "*.MID"))
		if "user_midi_dirs" in self:
			for path in user_midi_dirs:
				tracks.append_array(load_tracks_from_path(path, "*.mid"))
				tracks.append_array(load_tracks_from_path(path, "*.MID"))

	if tracks.size() == 0:
		is_playing = false
		Log.warn("!! No tracks found", logchan)
	else:
		Log.info("Tracks found: " + str(tracks.size()), logchan)
		Log.debug("Tracks: " + str(tracks), logchan)

func play_file(track_url: String) -> bool:
	if track_url.matchn("*.mid"):
		return play_midi_file(track_url)
	else:
		return play_music_file(track_url)

func play_midi_file(track_url: String) -> bool:
	#midi_player.set_file("res://assets/audio/midi/abuse/INTRO.MID")
	midi_player.set_file(track_url)
	Log.debug("MidiPlayer.play()", logchan)
	#midi_player.playing = true
	midi_player.play()
	#midi_player.stop()
	return true

func play_music_file(track_url: String) -> bool:
	# Check if file still exists
	#if not dir.file_exists(track_url + ".import"):
	if not ResourceLoader.exists(track_url):
		Log.err("Track " + track_url + " is missing.", logchan)
		return false
	var stream: AudioStream = load(track_url)
	stream.loop = false
	music_player.stream = stream
	music_player.play()
	return true

func play_track(number: int) -> void:
	#var track_url = music_dir_user + "/" + tracks[number]
	var track_url = tracks[number]
	Log.debug("Playing: " + track_url, logchan)
	is_playing = play_file(track_url)

#func _on_AudioStreamPlayer_finished() -> void:
func _on_finished() -> void:
	if is_playing:
		play_next()

func _exit_tree():
	Log.verbose("_exit_tree()", logchan)
