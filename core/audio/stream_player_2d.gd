extends AudioStreamPlayer2D
# Duplicate of stream_player.gd & stream_player_3d.gd

func _ready():
	connect("finished", self, "destroy_self")
	stop()

func destroy_self():
	stop()
	queue_free()
