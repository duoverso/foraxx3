tool
extends RichTextEffect
class_name RichTextHover

#
# Adapted from
# https://docs.godotengine.org/en/latest/tutorials/gui/bbcode_in_richtextlabel.html
#
# Syntax: [hover speed=5.0]hover text[/hover]

var bbcode := "hover"

func _process_custom_fx(char_fx: CharFXTransform) -> bool:

	var speed: float = char_fx.env.get("speed", 5.0)

	var offset := Vector2(0, sin(char_fx.elapsed_time * speed))
	char_fx.offset = offset
	return true
