#tool
extends Camera

onready var path_follow = $"../PathFollow"
var timer = 0.0

func _ready():
	set_process(false)

func _process(delta):
	path_follow.offset += 0.001 * Global.player.speed
	translation = path_follow.translation
	rotation = path_follow.rotation + Vector3(0, PI, 0)
	timer += delta
	if timer > 1.0:
		refresh_clock()
		timer = 0.0

func refresh_clock():
	var time = OS.get_time()
	UI.hud.set_event_text("%02d:%02d:%02d" % [time.hour, time.minute, time.second])
