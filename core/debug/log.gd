#tool
extends Node
#class_name Log

var logchan := "log"

# Enabling and disabling whole debugging
var enabled := true

# Debug levels
#enum Level {
enum Level {
	EMERG,		# 0 Emergency
	ALERT,		# 1 Alerts
	CRIT,		# 2 Critical
	ERR,		# 3 Errors
	WARN,		# 4 Warnings
	NOTICE,		# 5 Notification
	INFO,		# 6 Information
	DEBUG,		# 7 Debug
	VERBOSE,	# 8 Verbose
	MAX_LEVEL,	# 9 last item as amount of levels
}
# Default level for all channels without specified custom logging level
var default_channel := {"log": Level.NOTICE, "stdout": Level.INFO, "stderr": Level.ERR}

# Debug channels
var channels := {
	#"download": {"log": Level.INFO, "stdout": Level.DEBUG, "stderr": Level.INFO},
	#"music": {"log": Level.DEBUG}
	"generator": {"stdout": Level.INFO},
	"chunk": {"stdout": Level.INFO},
	"tower": {"log": Level.INFO, "stdout": Level.INFO},
}

var msgs := [] # Array of {"t": tics usec, "m": string, "c": String|Array, "l": Level} - time, message, channel(s) and level

func _enter_tree():
	verbose("_enter_tree() at: " + get_path() + " at time: " + Time.get_datetime_string_from_system(false, true) + " (" + str(OS.get_unix_time()) + ")", logchan)

# Return channel Dictionary
func get_channel(chan: String) -> Dictionary:
	if not chan in channels:
		channels[chan] = default_channel
	return channels[chan]

func get_log_level_of(chan: String) -> int:
	return channels[chan]["log"] if chan in channels and "log" in channels[chan] else default_channel["log"]

func set_log_level_of(chan: String, lvl: int) -> void:
	if not chan in channels:
		channels[chan] = default_channel
	channels[chan]["log"] = lvl

func emerg(msg: String, chans = "log") -> void:
	put("(EMERG) " + msg, chans, Level.EMERG)

func alert(msg: String, chans = "log") -> void:
	put("(ALERT) " + msg, chans, Level.ALERT)

func crit(msg: String, chans = "log") -> void:
	put("(CRIT) " + msg, chans, Level.CRIT)

func err(msg: String, chans = "log") -> void:
	put("(ERR) " + msg, chans, Level.ERR)

func warn(msg: String, chans = "log") -> void:
	put("(WARN) " + msg, chans, Level.WARN)

func notice(msg: String, chans = "log") -> void:
	put("(NOTICE) " + msg, chans, Level.NOTICE)

func info(msg: String, chans = "log") -> void:
	put("(INFO) " + msg, chans, Level.INFO)

func debug(msg: String, chans = "log") -> void:
	put("(DEBUG) " + msg, chans, Level.DEBUG)

func verbose(msg: String, chans = "log") -> void:
	put("(VERBOSE) " + msg, chans, Level.VERBOSE)

# Add msg to channels, print it if any channel has "print": true
func put(msg: String, chans = "log", lvl := Level.INFO) -> void:
	if not enabled:
		return
	if not chans is Array:
		chans = [chans]
	var msg_log_lvl: int
	var msg_stderr_lvl: int
	var msg_stdout_lvl: int
	for chan in chans:
		var channel := get_channel(chan)

		var chan_log_lvl = channel["log"] if "log" in channel else default_channel["log"]
		var chan_stdout_lvl = channel["stdout"] if "stdout" in channel else default_channel["stdout"]
		var chan_stderr_lvl = channel["stderr"] if "stderr" in channel else default_channel["stderr"]

		if msg_log_lvl == null: # inital value for msg_* vars by first channel
			msg_log_lvl = chan_log_lvl
			msg_stdout_lvl = chan_stdout_lvl
			msg_stderr_lvl = chan_stderr_lvl
		else:
			msg_log_lvl = int(max(msg_log_lvl, chan_log_lvl))
			msg_stdout_lvl = int(max(msg_stdout_lvl, chan_stdout_lvl))
			msg_stderr_lvl = int(max(msg_stderr_lvl, chan_stderr_lvl))

	if lvl <= msg_log_lvl:
		msgs.append({"t": OS.get_ticks_usec(), "m": msg, "l": lvl, "c": chans})

	if lvl <= msg_stderr_lvl:
		printerr(str(chans) + " " + msg)
	elif lvl <= msg_stdout_lvl:
		print(str(chans) + " " + msg)

# Clear msgs in all channels
func clear_all() -> void:
	msgs.clear()

# Remove channels from msgs
func clear_chans(chans = "log"):
	if not chans is Array:
		chans = [chans]
	for num in range(msgs.size(), 0, -1): # Remove in inverse order for less reindexing
		# Remove every given channel from msg channels
		for chan in chans:
			msgs[num]["c"].remove(chan)
		# Remove messages not attached to any channel
		if msgs[num]["c"].empty():
			msgs.remove(num)

# Clear msgs in given channels from all channels
# If any_given is false then msg has to be attached to every given chan to be removed from all chans.
func clear_from_all_chans(chans = "log", any_given := true) -> void:
	if not chans is Array:
		chans = [chans]
	for num in range(msgs.size(), 0, -1): # Remove in inverse order for less reindexing
#        var remove: bool
#        if any_given:
#            remove = false
#            for chan in chans:
#                if msgs[num]["c"].has(chan):
#                    remove = true
#                    break
#        else: # any_given == false
#            remove = true
#            for chan in chans:
#                if not msgs[num]["c"].has(chan):
#                    remove = false
#                    break
		var remove := not any_given
		for chan in chans:
			if msgs[num]["c"].has(chan) == any_given:
				remove = any_given
				break
		if remove:
			msgs.remove(num)

func usec_diff(since: int) -> int:
	return Time.get_ticks_usec() - since

func str_usec_diff(since: int) -> String:
	return usec_diff_to_str(Time.get_ticks_usec() - since)

func usec_diff_to_str(diff: int) -> String:
	var result := ""
	if diff >= 1_000_000:
		result += str(diff / 1e6) + " s"
		diff %= 1_000_000
#	if diff >= 1000:
#		result += (" " if result else "") + str(diff / 1e3) + "ms"
#		diff %= 1000
	if diff > 0:
#		result += (" " if result else "") + str(diff) + "us"
		result += (" " if result else "") + str(diff / 1e3) + " ms"
	return result

func logtimes_usec_to_str(logtime: Dictionary) -> String:
	var str_logtime := ""
	for key in logtime:
		str_logtime += str(key, ": ", usec_diff_to_str(logtime[key][1] - logtime[key][0]), "\n")
	return str_logtime
