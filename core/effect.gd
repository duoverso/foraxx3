extends Node

var logchan := "effect"

var explosion_scene := preload("res://assets/effects/explosion/explosion.tscn")

func _enter_tree():
	Log.verbose("_enter_tree() at: " + get_path(), logchan)

func add_explosion(position: Vector3, scale := Vector3.ONE, time := 0.5, amount := 16) -> void:
	var explosion := explosion_scene.instance()
	explosion.lifetime = time
	explosion.amount = amount
	explosion.translate(position)
	explosion.scale_object_local(scale)
	$"/root/Level".add_child(explosion)
	explosion.emitting = true

	yield(get_tree().create_timer(time), "timeout")

	#_explosion.get_parent().remove_child(_explosion)
	explosion.queue_free()
