#extends KinematicBody
extends Player

#var speed = 10
var h_acceleration = 6
var air_acceleration = 1
var normal_acceleration = 6
var jump = 20
var full_contact = false

#var mouse_sensitivity = 0.3

var direction = Vector3()
var h_velocity = Vector3()
var movement = Vector3()
var gravity_vec = Vector3()

onready var head = $Head
onready var ground_check = $GroundCheck

func _ready():
	gravity = 9.8
	model = $Body
	cam_1st = $Head/Camera1st
	cam_3rd = $Camera3rd
	cam = cam_1st
	cam.current = true

func _unhandled_input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		if not mouse_captured:
			return
		#rotate_y(deg2rad(-event.relative.x * mouse_sensitivity))
		rotate_y(deg2rad(-event.relative.x * mouse_sensitivity / 30000))
		#head.rotate_x(deg2rad(-event.relative.y * mouse_sensitivity))
		head.rotate_x(deg2rad(-event.relative.y * mouse_sensitivity / 30000))
		head.rotation.x = clamp(head.rotation.x, deg2rad(-89), deg2rad(89))

		Log.debug("_unhandled_input( " + str(event) + " )", logchan)
		scene_tree.set_input_as_handled()

func _physics_process(delta):

	direction = Vector3()

	full_contact = ground_check.is_colliding()

	if not is_on_floor():
		gravity_vec += Vector3.DOWN * gravity * delta
		h_acceleration = air_acceleration
	elif full_contact:
		gravity_vec = -get_floor_normal() * gravity
		h_acceleration = normal_acceleration
	else:
		gravity_vec = -get_floor_normal()
		h_acceleration = normal_acceleration

	if Input.is_action_just_pressed("jump") and (is_on_floor() or ground_check.is_colliding()):
		gravity_vec = Vector3.UP * jump

	if Input.is_action_pressed("forward"):
		direction -= transform.basis.z
	elif Input.is_action_pressed("backward"):
		direction += transform.basis.z
	if Input.is_action_pressed("strafe_left"):
		direction -= transform.basis.x
	elif Input.is_action_pressed("strafe_right"):
		direction += transform.basis.x

	direction = direction.normalized()
	h_velocity = h_velocity.linear_interpolate(direction * speed, h_acceleration * delta)
	movement.z = h_velocity.z + gravity_vec.z
	movement.x = h_velocity.x + gravity_vec.x
	movement.y = gravity_vec.y

	move_and_slide(movement, Vector3.UP)
