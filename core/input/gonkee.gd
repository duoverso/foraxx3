#extends KinematicBody
extends Player
#class_name GonkeePlayerController

var settings_section_gonkee := "gonkee"
var settings_properties_gonkee := ["fly_mode", "jump_force", "gravity_default", "gravity_max"]

var velocity = Vector3()
export var acceleration: float = 1.0 # 1/FPS seconds due to processing in _process()
export var run_multiplier: float = 2.0
export var max_slope_angle: float = deg2rad(45)

export var gravity_default: float = -60
export var gravity_max: float = -150

export var jump_force: float = 20.0
var jumping := false
var crouching := false
export(int, "None", "Keyboard", "Mouse") var fly_mode := 0

onready var collision_ray := $CollisionRay
onready var foot_cast = $FootCast
#export var foot_cast_translation = Vector3(0, 0.5, 0)
var snap_normal = Vector3.DOWN setget set_snap_normal

#onready var anim = $AnimationPlayer
#onready var skeleton = $Model/Armature/Skeleton
#onready var crouchtween = $CollisionRay/CrouchTween
#var headbone
#var initial_head_transform

func _enter_tree():
	logchan = "player.gonkee"
	Log.verbose("_enter_tree() at: " + get_path(), logchan)

# overrides Player
func load_settings(onstart := false) -> void:
	.load_settings(onstart)
	Log.debug("GonkeePlayerController.load_settings(" + str(onstart) + ")", logchan)
	Settings.load_to(self, settings_section_gonkee, settings_properties_gonkee)

# overrides Player
func save_settings(send_saved := true):
	.save_settings(false)
	Log.debug("GonkeePlayerController.save_settings()", logchan)
	Settings.save_from(self, settings_section_gonkee, settings_properties_gonkee)
	Settings.set_value("player", "controller", "gonkee")
	if send_saved:
		Settings.saved(self)

func _ready() -> void:
	Log.verbose("_ready()", logchan)

	gravity = gravity_default
	#foot_cast.translation = foot_cast_translation
	model = $Model

	cam_1st = $Camera1st
	cam_3rd = $Camera3rd
	cam_arvr = $ARVROrigin/ARVRCamera
	raycast = $Camera1st/RayCast
	flashlight = $Camera1st/Flashlight

	#headbone = skeleton.find_bone("head")
	#cam_1st.translation = skeleton.get_bone_global_pose(headbone).origin
	#initial_head_transform = skeleton.get_bone_pose(headbone)
#	cam_1st = $Camera1st
#	cam_3rd = $Camera3rd
#	cam = cam_1st
#	cam.current = true
#	fov_reset()

func set_snap_normal(new_snap_normal):
	Log.debug("set_snap_normal(" + str(new_snap_normal) + ")", logchan)
	snap_normal = new_snap_normal

func use_primary(delta: float):
	primary_timer -= delta
	if primary_timer > 0.0:
		return
	primary_cooldown_timer -= delta
	if primary_cooldown_timer > 0.0:
		return
	primary_cooldown_timer = primary_cooldown
	# Reduce ammo (-1 = infinite, 0 no ammo, 1+ has ammo to reduce)
	if ammo[weapon] == 0:
		Sound.play_clink()
		#using_primary = false
		return
	elif ammo[weapon] > 0:
		ammo[weapon] -= 1

	raycast.force_raycast_update()
	Log.debug("weapon: " + str(weapon) + ", collision_mask: " + str(raycast.collision_mask) + " (raycast: " + str(raycast) + ")", logchan)

	Sound.play_weapon_primary()
	if weapon <= 11:
		if raycast.is_colliding():
			# Hit enemy
			var collider = raycast.get_collider()
			Log.debug("collider: " + str(collider), logchan)
			if collider.is_in_group("Enemies"):
				if primary_range < 0:
					collider.hit(weapons[weapon]["dmg"]) # full dmg at infinite distance
				else:
					var ray_length := (raycast.get_collision_point() - self.global_translation).length()
					Log.debug("ray length: " + str(ray_length), logchan)
					if ray_length <= primary_range:
						if primary_ease == 0.0:
							collider.hit(weapons[weapon]["dmg"]) # full dmg at weapon's range
						else:
							collider.hit(weapons[weapon]["dmg"] * ease(1.0 - ray_length / primary_range, primary_ease))
		else:
			Log.debug("not colliding", logchan)

func _physics_process(delta):
#func _process(delta):
	if using_primary:
		use_primary(delta)

	var direction_joystick: Vector2
	if Global.has_touch:
		direction_joystick = left_touch_joystick_button.get_value()


	var run
	if (abilities & CAN_RUN) and Input.is_action_pressed("run"):
		run = run_multiplier
	else:
		run = 1

	if Input.is_action_pressed("player_enlarge"):
		#scale_object_local(Vector3(1.01, 1.01, 1.01))
		set_size(size * 1.111)
	elif Input.is_action_pressed("player_shrink"):
		#scale_object_local(Vector3(0.99, 0.99, 0.99))
		set_size(size * 0.9)
	elif Input.is_action_pressed("player_size_reset"):
		#set_scale(Vector3(1.0, 1.0, 1.0))
		set_size(1.0)

	elif Input.is_action_pressed("speed_up"):
		#set_speed(min(1000000, speed*1.01))
		set_speed(speed * 1.05)
	elif Input.is_action_pressed("slow_down"):
		set_speed(max(1, speed/1.05))
	elif Input.is_action_just_pressed("speed_preset_1"):
		set_speed(50) # 100
		jump_force = 10 # 20
		#foot_cast.cast_to.y = -0.5
	elif Input.is_action_just_pressed("speed_preset_2"):
		set_speed(100) # 500
		jump_force = 15 # 40
		#foot_cast.cast_to.y = -1
	elif Input.is_action_just_pressed("speed_preset_3"):
		set_speed(200) # 2000
		jump_force = 20 # 15
		#foot_cast.cast_to.y = -2
	elif Input.is_action_just_pressed("speed_preset_4"):
		set_speed(500) # 10e3
		jump_force = 25 # 6
		#foot_cast.cast_to.y = -4
	elif Input.is_action_just_pressed("speed_preset_5"):
		set_speed(1000) # 50e3
		jump_force = 30 # 3
	elif Input.is_action_just_pressed("speed_preset_6"):
		set_speed(2000) # 200e3
		jump_force = 10 # 1.5
	elif Input.is_action_just_pressed("speed_preset_7"):
		set_speed(5000) # 1e6
		jump_force = 10 # 0.6
	elif Input.is_action_just_pressed("speed_preset_8"):
		set_speed(10e3) # 5e6
		jump_force = 10 # 0.24
	elif Input.is_action_just_pressed("speed_preset_9"):
		set_speed(20e3) # 10e6
		jump_force = 10 # 0.24
	elif Input.is_action_just_pressed("speed_preset_10"):
		set_speed(50e3) # 50e6
		jump_force = 10 # 0.96
		# @TODO: Cause falling at XZ plane
		#self.look_at(Vector3(0, 0, 0), Vector3.UP)

	if abilities & CAN_ROTATE:
		if Global.has_touch:
			rotation.y += -direction_joystick.x * 1.75 * delta

		if Input.is_action_pressed("ui_left"):
			rotation.y += y_rotation_speed * 3
		if Input.is_action_pressed("ui_right"):
			rotation.y -= y_rotation_speed * 3

		if Input.is_action_pressed("ui_down"):
			cam.rotation.x -= x_rotation_speed * 3
		if Input.is_action_pressed("ui_up"):
			cam.rotation.x += x_rotation_speed * 3

	#velocity.x = Input.get_action_strength("strafe_right") - Input.get_action_strength("strafe_left")
	#velocity.z = Input.get_action_strength("forward") - Input.get_action_strength("backward")

	if fly_mode:
		var direction := Vector3()

		if Global.has_touch:
			# Forward/backward
			direction.z = direction_joystick.y
			# Strafe left/right
			#direction.x = direction_joystick.x
			# Turn left/right
			rotation.y += -direction_joystick.x * 1.75 * delta

		# Fly with keyboard
		if fly_mode == 1:
			var normalized := true
			if Input.is_action_pressed("forward"):
				direction.z -= 1
				normalized = false
			if Input.is_action_pressed("backward"):
				direction.z += 1
				normalized = false
			if Input.is_action_pressed("strafe_left"):
				direction.x -= 1
				normalized = false
			if Input.is_action_pressed("strafe_right"):
				direction.x += 1
				normalized = false
			if Input.is_action_pressed("jump") or Input.is_action_pressed("ui_page_up"):
				direction.y += 1
				normalized = false
			if Input.is_action_pressed("crouch") or Input.is_action_pressed("ui_page_down"):
				direction.y -= 1
				normalized = false
			if not normalized:
				direction = direction.normalized()
			direction = direction.rotated(Vector3.UP, rotation.y)

		elif fly_mode == 2:
			direction.x = sin(rotation.y)
			direction.z = cos(rotation.y)

			if (cam_1st.current):
				#velocity.y = cam_1st.rotation.x * speed * delta # * 100 * delta
				direction.y = cam_1st.rotation.x
				#direction.x = rotation.x # cam_1st.rotation.y
				#direction.y = cam_1st.rotation.z
			else:
				#velocity.y = cam_3rd.rotation.x * speed * delta # * delta
				direction.y = cam_3rd.rotation.x
				#direction.x = rotation.x # cam_3rd.rotation.y
				#direction.y = cam_3rd.rotation.z

			direction = direction.normalized() #.rotated(Vector3.UP, rotation.y)

		#velocity.x = lerp(velocity.x, direction.x, speed * delta)
		#velocity.z = lerp(velocity.z, direction.y, speed * delta)
		var delta_speed = delta * speed * run
		velocity.x = direction.x * delta_speed
		velocity.y = direction.y * delta_speed
		velocity.z = direction.z * delta_speed

		#translate(direction)
		move_and_slide(velocity, Vector3.UP) #Vector3(0, 1, 0))
		#velocity = move_and_slide(velocity, Vector3.UP)

		#if is_on_floor() and velocity.y < 0:
		if velocity.y < 0 and foot_cast.is_colliding():
			velocity.y = 0
			if jumping:
				jumping = false

	# Falling
	else:
		var direction := Vector2()

		if abilities & CAN_WALK:
			if Global.has_touch:
				# Forward/backward
				direction.y = direction_joystick.y
				# Strafe left/right
				#direction.x = direction_joystick.x

			var normalized := true
			if Input.is_action_pressed("forward"):
				direction.y -= 1
				normalized = false
			if Input.is_action_pressed("backward"):
				direction.y += 1
				normalized = false
			if Input.is_action_pressed("strafe_left"):
				direction.x -= 1
				normalized = false
			if Input.is_action_pressed("strafe_right"):
				direction.x += 1
				normalized = false
			if not normalized:
				direction = direction.normalized()
			#Log.debug(str(direction), logchan)

#		if not (jumping or crouching):
#			set_anim(direction)

		# Gravity
		velocity.y += gravity * delta
		if velocity.y < gravity_max:
			velocity.y = gravity_max
		# Jump
		#if Input.is_action_just_pressed("jump") and is_on_floor():
		if (abilities & CAN_JUMP) and Input.is_action_just_pressed("jump") and foot_cast.is_colliding():
			velocity.y = jump_force * max(1, speed*run / 500)
			jumping = true
#			anim.play("idle")
#			anim.play("jump")

		# Crouch
		if (abilities & CAN_CROUCH) and Input.is_action_pressed("crouch"):
			if not (crouching or jumping):
				crouching = true
#				anim.play("crouch", 0.2, 1.0)
#				crouchtween.interpolate_property(collision_ray.shape, "length", collision_ray.shape.length, 0.5, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
#				crouchtween.interpolate_property(foot_cast, "translation", foot_cast.translation, foot_cast_translation+Vector3(0, 0.5, 0), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
#				crouchtween.start()
		# End of crouching
		elif crouching:
			crouching = false
#			crouchtween.interpolate_property(collision_ray.shape, "length", collision_ray.shape.length, 1, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
#			crouchtween.interpolate_property(foot_cast, "translation", foot_cast.translation, foot_cast_translation, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
#			crouchtween.start()

		#if direction.length() > 0:
		#	direction = direction.normalized()
		## TODO: ARVR doesn't update ARVRCamera rotation
		#if get_viewport().arvr:
		#	direction = direction.rotated(cam_arvr.rotation.y) # for Vector2 direction
		#else:
		#	direction = direction.rotated(-rotation.y) # for Vector2 direction
		direction = direction.rotated(-rotation.y) # for Vector2 direction

		#velocity.x = lerp(velocity.x, direction.x, speed * delta)
		#velocity.z = lerp(velocity.z, direction.y, speed * delta)
		velocity.x = direction.x * speed*run * delta
		velocity.z = direction.y * speed*run * delta

		#translate(direction)
		#velocity = move_and_slide(velocity, Vector3.UP)
		velocity = move_and_slide(velocity, Vector3.UP, true, 4, max_slope_angle) # Vector3(0, 1, 0)
		#move_and_slide_with_snap(velocity, snap_normal, Vector3.UP, true)

		#if is_on_floor() and velocity.y < 0:
		if velocity.y < 0 and foot_cast.is_colliding():
			velocity.y = 0
			if jumping:
				jumping = false

	#skeleton.set_bone_pose(headbone, initial_head_transform.rotated(Vector3.LEFT, cam_1st.rotation.x))

#	if Input.is_action_pressed("secondary"):
#	#if event.button_index == BUTTON_RIGHT:
#		var tree = Tree.instance()
#		tree.translate(translation) # + Vector3(5, 0, 0))
#		#tree.rotate_object_local(Vector3.UP, rand_range(0, 6.28))
#		#tree.rotate_object_local(Vector3.LEFT, rand_range(-0.25, 0.25)) # rand_range(-0.5, 0.5) like after storm
#		tree.rotate_x(rand_range(-0.1, -0.1))
#		#tree.rotate_object_local(Vector3.FORWARD, rand_range(-0.25, 0.25)) # rand_range(-0.5, 0.5) like after storm
#		tree.rotate_z(rand_range(-0.1, -0.1))
#		#tree.scale_object_local(Vector3(rand_range(9, 11)/10, rand_range(75, 150)/10, rand_range(9,11)/10))
#		tree.rotate_y(rand_range(0, 6.28))
#		get_parent().add_child(tree)

#func set_anim(direction):
#	if direction == Vector2.ZERO and anim.current_animation != "idle":
#		#Log.debug("idle", logchan)
#		anim.play("idle", 0.5)
#	elif direction == Vector2(0, 1) and anim.current_animation != "forward" and anim.get_playing_speed() > 0:
#		#Log.debug("forward", logchan)
#		anim.play("forward", 0.1)
#	elif direction == Vector2(1, 1) and anim.current_animation != "forwardLeft" and anim.get_playing_speed() > 0:
#		#Log.debug("forwardLeft", logchan)
#		anim.play("forwardLeft", 0.1)
#	elif direction == Vector2(-1, 1) and anim.current_animation != "forwardRight" and anim.get_playing_speed() > 0:
#		#Log.debug("forwardRight", logchan)
#		anim.play("forwardRight", 0.1)
#	elif direction == Vector2(1, 0) and anim.current_animation != "left":
#		#Log.debug("left", logchan)
#		anim.play("left", 0.1)
#	elif direction == Vector2(-1, 0) and anim.current_animation != "right":
#		#Log.debug("right", logchan)
#		anim.play("right", 0.1)
#	elif direction == Vector2(0, -1) and anim.current_animation != "forward": # and anim.get_playing_speed() < 0:
#		#Log.debug("backward", logchan)
#		anim.play_backwards("forward", 0.1)
#	elif direction == Vector2(-1, -1) and anim.current_animation != "forwardLeft" and anim.get_playing_speed() < 0:
#		#Log.debug("backwardLeft", logchan)
#		anim.play_backwards("forwardLeft", 0.1)
#	elif direction == Vector2(1, -1) and anim.current_animation != "forwardRight" and anim.get_playing_speed() < 0:
#		#Log.debug("backwardRight", logchan)
#		anim.play_backwards("forwardRight", 0.1)

#func process_input_event_key(event):
#	if Input.is_key_pressed(KEY_ESCAPE):
#		get_tree().quit()
#	#var velocity = Vector3()
#	if Input.is_action_pressed("forward"):
#		velocity.z -= 1
#	if Input.is_action_pressed("backward"):
#		velocity.z += 1
#	if Input.is_action_pressed("strafe_left"):
#		velocity.x -= 1
#	if Input.is_action_pressed("strafe_right"):
#		velocity.x += 1
#	if Input.is_action_pressed("ui_page_up"):
#		speed = min(100, speed+1)
#	if Input.is_action_pressed("ui_page_down"):
#		speed = max(1, speed-1)

func rotate_cam(event):
	if not abilities & CAN_ROTATE:
		return

	#if cam_arvr.current:
	#	cam_arvr.rotation.x -= relative.y / cam.fov / (mouse_sensitivity / cam.fov / cam.fov)
	#	cam_arvr.rotation.x = clamp(cam_arvr.rotation.x, x_rotation_limit_up, x_rotation_limit_down)

#	if cam_1st.current:
#		# cam_1st.rotation.x -= relative.y * x_rotation_speed
#		cam_1st.rotation.x -= relative.y / cam.fov / (mouse_sensitivity / cam.fov / cam.fov)
#		cam_1st.rotation.x = clamp(cam_1st.rotation.x, x_rotation_limit_up, x_rotation_limit_down)
#	else:
#		# cam_3rd.rotation.x -= relative.y * x_rotation_speed
#		cam_3rd.rotation.x -= relative.y / cam.fov / (mouse_sensitivity / cam.fov / cam.fov)
#		cam_3rd.rotation.x = clamp(cam_3rd.rotation.x, x_rotation_limit_up, x_rotation_limit_down)

	var relative_x: float = event.relative.y / cam.fov / (mouse_sensitivity / cam.fov / cam.fov)
	if x_rotation_invert:
		cam.rotation.x += relative_x
	else:
		cam.rotation.x -= relative_x
	if x_rotation_limit:
		cam.rotation.x = clamp(cam.rotation.x, x_rotation_limit_up, x_rotation_limit_down)

	# rotation.y -= event.relative.x * y_rotation_speed
	var relative_y: float = event.relative.x / cam.fov / (mouse_sensitivity / cam.fov / cam.fov)
	if y_rotation_invert:
		rotation.y += relative_y
	else:
		rotation.y -= relative_y
	if y_rotation_limit:
		rotation.y = clamp(rotation.y, y_rotation_limit_left, y_rotation_limit_right)

func _unhandled_input(event: InputEvent) -> void:
	#if Log.enabled:
	#    Log.verbose("_unhandled_input(" + str(event) + ")", logchan)
	#if event is InputEventKey:
	#	process_input_event_key(event)
	if event is InputEventMouseMotion:
		#Log.verbose("_unhandled_input(InputEventMouseMotion) " + str(event), logchan)
		# get_global_mouse_position()
		#if (Global.mouse_mode == 2): # Input.MOUSE_MODE_CAPTURED):
		if not mouse_captured:
			return
		rotate_cam(event)

	elif event is InputEventScreenDrag:
		#Log.verbose("_unhandled_input(InputEventScreenDrag) index: " + str(event.index), logchan)
		if event.index == left_touch_joystick_button.touch_idx or event.index == right_touch_joystick_button.touch_idx:
			return
		rotate_cam(event)

	elif event.is_pressed():
		#Log.verbose("_unhandled_input(event.is_pressed()) " + str(event), logchan)
		if UI.hud.debug_label.visible:
			UI.hud.set_event_text(event.as_text() + " (Dev" + str(event.get_device()) + ")", 2.0)

		# if event.button_index == BUTTON_LEFT:
		if event.is_action("primary"):
			if not get_viewport().arvr and not Global.has_touch and not mouse_captured:
				return
			Log.debug("handled " + str(event), logchan)
			scene_tree.set_input_as_handled()
			if not (abilities & CAN_USE_PRIMARY):
				return


			if primary_continuous:
				using_primary = true
				primary_timer = primary_time_to_start
				# @TODO: Cooldow timer has to reach primary_cooldown first (separately for each weapon?)
				primary_cooldown_timer = 0.0
				return # Input is already handled

			# Oneshot

			# Reduce ammo (-1 = infinite, 0 no ammo, 1+ has ammo to reduce)
			if ammo[weapon] == 0:
				Sound.play_clink()
				return
			elif ammo[weapon] > 0:
				ammo[weapon] -= 1

			raycast.force_raycast_update()
			Log.debug("weapon: " + str(weapon) + ", collision_mask: " + str(raycast.collision_mask) + " (raycast: " + str(raycast) + ")", logchan)

			if weapon == 12: # Seeder
				if not raycast.is_colliding():
					Log.debug("Seeder not colliding", logchan)
					return # Input is already handled

				var position = raycast.get_collision_point()
				Log.debug("collision point: " + str(position), logchan)
				#var normal = raycast.get_collision_normal()
				var tree = trees[randi() % trees.size()].instance()
				tree.translate(position) # + Vector3(5, 0, 0))
				#tree.rotate(Vector3.UP, rotation.y)
				tree.rotate(Vector3.UP, deg2rad(randi() % 360))

				#tree.scale_object_local(Vector3.ONE * (50+(randi() % 100)) / 100)
				tree.scale = Vector3.ZERO
				var tween = create_tween().set_trans(Tween.TRANS_ELASTIC).set_ease(Tween.EASE_OUT)
				var size := (50 + (randi() % 100)) / 100.0
				tween.tween_property(tree, "scale", Vector3.ONE * size, 1.35)
				#tween.parallel().tween_property(tree, "rotation:y", deg2rad(randi() % 360), 0.5)

				#get_parent().add_child(tree)
				$"/root/Level".add_child(tree)
				Sound.play(Sound.breaking_wood, tree)

#			elif weapon == 13: # Builder
#				if raycast.is_colliding():
#					var position = raycast.get_collision_point()
#					var building = buildings[randi() % buildings.size()].instance()
#					building.translate(position)
#					building.rotate(Vector3.UP, rotation.y + deg2rad(90))
#					#get_parent().add_child(building)
#					$"/root/Level".add_child(building)

			elif weapon == 13: # Builder
				if not raycast.is_colliding():
					Log.debug("Builder not colliding", logchan)
					return # Input is already handled

				var position = raycast.get_collision_point()
				Log.debug("collision point: " + str(position), logchan)
				#var building = load("res://assets/models/buildings/temple.tscn").instance()
				#var building = Builder.new(Vector3(randi() % 20, randi() % 20, randi() % 5 + 3))
				#var building = Builder.new(Vector3(30, 10, 60))
				#var building = Builder.new(Vector3(30.929, 10.443, 69.583))
				var building = Builder.new() #{"width": 2, "height": 2, "depth": 4, "Cube": {"mesh": "cube"}})
				#building.height = 10.443 # building.width / 4
				#building.depth = 69.583 # 69.589 - 0.06 # randi() % 100 + 10
				#building.width = 30.929 # 30.935 - 0.06 # randi() % 100 + 10
				building.translate(position) # + Vector3(0, 10, 0))
				building.rotate(Vector3.UP, rotation.y) # + deg2rad(180))
				$"/root/Level".add_child(building)
				#building.look_at(raycast.get_collision_normal(), Vector3.LEFT)
				#building.translate_object_local(Vector3(0, 10, 0))
				#building.rotate(Vector3.BACK, PI)
				#get_parent().add_child(building)
				#yield(get_tree().create_timer(2.0), "timeout")
				#building.width /= 2
				#yield(get_tree().create_timer(1.0), "timeout")
				#building.depth /= 2
#				yield(get_tree().create_timer(1.0), "timeout")
#				for row in range(building.parts["column_rows"].size()):
#					building.parts["column_rows"][row]["amount"] = 5
#				building.update_temple()
				Sound.play_door_opening(building)

			elif weapon == 14: # Architect
				if not raycast.is_colliding():
					Log.debug("Architect not colliding", logchan)
					return # Input is already handled

				var position = raycast.get_collision_point()
				Log.debug("collision point: " + str(position), logchan)
				var building = Builder.new(Architect.simple_house(5, 10, 1, 4, 3))
				building.translate(position) # + Vector3(0, 10, 0))
				building.rotate(Vector3.UP, rotation.y)
				$"/root/Level".add_child(building)
				Sound.play_clank(building)

			return # Input is already handled

		elif event.is_action("secondary"):
			if not get_viewport().arvr and not Global.has_touch and not mouse_captured:
				return

			Log.debug("handled " + str(event), logchan)
			scene_tree.set_input_as_handled()
			if not (abilities & CAN_USE_SECONDARY):
				return # Input is already handled

			# Reload all weapons
			#for key in range(ammo.size()):
			#	ammo[key] += 10
			# Reload current weapon
			if weapons[weapon]["clip"] <= 0 or ammo[weapon] >= weapons[weapon]["max_ammo"]:
				return
			if "cost" in weapons[weapon]:
				if money < weapons[weapon]["cost"]:
					UI.hud.set_event_text(str("$", weapons[weapon]["cost"]) + " needed", 0.5)
					Sound.play_oh()
					return
				money -= weapons[weapon]["cost"]
			#yield(get_tree().create_timer(1.0), "timeout")
			ammo[weapon] = min(ammo[weapon] + weapons[weapon]["clip"], weapons[weapon]["max_ammo"])
			return # Input is already handled

		elif event.is_action("flashlight"):
			flashlight.visible = not flashlight.visible

#		elif event.is_action("crouch"):
#			#if not crouching:
#			#crouching = true
#			anim.play("crouch", 0.2)
#			crouchtween.interpolate_property($CollisionRay.shape, "length", $CollisionRay.shape.length, 0.1, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
#			crouchtween.start()

		elif event.is_action("fly_mouse"):
			if not (abilities & CAN_FLY):
				return

			# Turn Mouse Fly Mode off
			if fly_mode == 2:
				Log.debug("falling", logchan)
				fly_mode = 0
				gravity = -60
			# Switch to Mouse Fly Mode
			else:
				Log.debug("fly with mouse", logchan)
				fly_mode = 2
				gravity = 0
				#velocity.y = 0

		elif event.is_action("fly_keyboard"):
			if not (abilities & CAN_FLY):
				return

			# Turn Keyboard Fly Mode off
			if fly_mode == 1:
				Log.debug("falling", logchan)
				fly_mode = 0
				gravity = -60
			# Switch to Keyboard Fly Mode
			else:
				Log.debug("fly with keyboard", logchan)
				fly_mode = 1
				gravity = 0
				velocity.y = 0

		else:
			return # Input is not handled

	# Released
	# elif Input.is_action_released():
	else: # not event.is_pressed()
		#Log.verbose("_unhandled_input(not event.is_pressed()) " + str(event), logchan)
		if event.is_action("primary"):
			using_primary = false
			Sound.stop_weapon_primary()
#		elif event.is_action("crouch"):
#			#if crouching:
#			#crouching = false
#			crouchtween.interpolate_property($CollisionRay.shape, "length", $CollisionRay.shape.length, 1, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
#			crouchtween.start()
		else:
			return # Input is not handled

	#Log.debug("handled " + str(event), logchan)
	scene_tree.set_input_as_handled()

func add_triangle_here() -> MeshInstance:
	return add_triangle(translation)

func add_triangle(position: Vector3) -> MeshInstance:
	# Add triangle composed by 3 vertices
	var vertices = PoolVector3Array()
	vertices.push_back(Vector3(position.x, position.y+1, position.z))
	vertices.push_back(Vector3(position.x+1, position.y, position.z))
	vertices.push_back(Vector3(position.x, position.y, position.z+1))
	# Initialize the ArrayMesh.
	var arr_mesh = ArrayMesh.new()
	var arrays = []
	arrays.resize(ArrayMesh.ARRAY_MAX)
	arrays[ArrayMesh.ARRAY_VERTEX] = vertices
	# Create the Mesh.
	arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
	var m = MeshInstance.new()
	m.mesh = arr_mesh
	get_tree().get_root().add_child(m)
	return m

func _exit_tree():
	Log.verbose("_exit_tree()", logchan)
