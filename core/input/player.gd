tool
extends KinematicBody
#extends Node
class_name Player
#class_name PlayerController

var logchan := "player"

var settings_section := "player"
var settings_properties := [
	"mouse_sensitivity", "x_rotation_invert", "y_rotation_invert", "x_rotation_speed", "y_rotation_speed",
	"x_rotation_limit", "x_rotation_limit_up", "x_rotation_limit_down",
	"y_rotation_limit", "y_rotation_limit_left", "y_rotation_limit_right",
	"zoom_speed", "translation", "rotation", "size", "min_size", "max_size",
#	# max_speed has to be set before speed
	"max_speed", "speed", "gravity", "money", "hp", "hp_max",
]

var settings_properties_ready := [
	"cam.rotation", "cam_far",
	"cam_fov_default", "cam_fov_min", "cam_fov_max", "cam.fov",
	"weapon",
]

onready var scene_tree := get_tree()
var mouse_captured = true

# @TODO: Should be int/100, int/1000 ?
var hp: float = 100.0 setget set_hp
# @TODO: Should be int/100, int/1000 ?
var hp_max: float = 100.0

# Skill class? for array of skills?
# Attribute class?
const CAN_ROTATE = 1
const CAN_WALK = 2
const CAN_RUN = 4
const CAN_JUMP = 8
const CAN_CROUCH = 16
const CAN_FLY = 32
const CAN_USE_PRIMARY = 64
const CAN_USE_SECONDARY = 128
const CAN_FREE_CAMERA = 256
const CAN_PLACE_RELOCATOR = 512
const CAN_STEAL_VEHICLE = 1024
const CAN_DRIVE = 2048
const CAN_SWIM = 4096
const CAN_BREATHE_UNDERWATER = 8192
#const CAN_ENLARGE
#const CAN_SHRINK
#const CAN_CLIMB
#const CAN_GRAB
var held_breath_limit := 10.0 # in seconds

export(int) var abilities = CAN_ROTATE | CAN_WALK | CAN_RUN | CAN_JUMP | CAN_CROUCH | CAN_FLY | CAN_USE_PRIMARY | CAN_USE_SECONDARY | CAN_PLACE_RELOCATOR
var underwater := false setget set_underwater
var underwater_timer: float # seconds over 0 is set by held_breath_limit
var underwater_no_breath_timer: float
var underwater_no_breath_damage: float = 10
var underwater_no_breath_damage_per: float = 2.0 # seconds
var water_level := 0.0 # updated by Generator's sea_level

#onready var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
#PhysicsServer.area_set_param(get_viewport().find_world().get_space(), PhysicsServer.AREA_PARAM_GRAVITY, 9.8)
#PhysicsServer.area_set_param(get_viewport().find_world().get_space(), PhysicsServer.AREA_PARAM_GRAVITY_VECTOR, Vector3(0, -1, 0))
var gravity

export var speed: float = 100.0 setget set_speed
export var max_speed: float = 10e3

export var size: float = 1.0 setget set_size
export var min_size: float = 0.1
export var max_size: float = 100.0

export var max_free_cam_distance: float = 10.0

export var max_relocators: int = 3

var cam: Camera
var cams := {}  # Dictionary of Cameras
#onready var cam_1st = $Camera1st
var cam_1st: Camera
#onready var cam_3rd = $Camera3rd
var cam_3rd: Camera
#onready var cam_arvr = $ARVROrigin/ARVRCamera
var cam_arvr: Camera
export var mouse_sensitivity: float = 10000
export var x_rotation_invert := false
export var y_rotation_invert := false
export var x_rotation_speed := 0.01
export var y_rotation_speed := 0.01
export var x_rotation_limit := true
export var x_rotation_limit_up := deg2rad(-90) # = -1.571
export var x_rotation_limit_down := deg2rad(90) # = 1.571
export var y_rotation_limit := false
export var y_rotation_limit_left := deg2rad(-90) # = -1.571
export var y_rotation_limit_right := deg2rad(90) # = 1.571
export var cam_fov_default : float = 90.0
export var cam_fov_min: float = 1.0
export var cam_fov_max: float = 120.0
export var zoom_speed: float = 1.1
export var cam_far: float = 1000 setget set_cam_far

var model: Spatial

var money: float = 0.0

#enum {WEAPON_RAILGUN, WEAPON_SEED_TREE, WEAPON_PLACE_BUILDING, WEAPON_UNKNOWN}
var weapons := [
	{"name": "Impact Hammer",    "ammo": -1,  "clip": -1,               "max_ammo": -1,   "dmg": 100.0,                 "cooldown": 1.0,  "range": 3.0,    "ease": 3.0, "sounds": {"raise": Sound.ut_impact_hammer_raise,   "primary": Sound.ut_impact_hammer_primary}},
	{"name": "Chainsaw",         "ammo": -1,  "clip": -1,               "max_ammo": -1,   "dmg": 10.0,                  "cooldown": 0.1,  "range": 3.0,    "ease": 0.2, "sounds": {"raise": Sound.ut_chainsaw_raise,        "primary": Sound.ut_chainsaw_primary, "primary_loop": true}},
	{"name": "Enforcer",         "ammo": 10,  "clip": 5,  "cost": 5,    "max_ammo": 10,   "dmg": 2.0,                   "cooldown": 0.4,  "range": 50.0,   "ease": 0.0, "sounds": {"raise": Sound.ut_enforcer_raise,        "primary": Sound.ut_enforcer_primary}}, # Sound.w3d_pistol_shot
	{"name": "Bio Rifle",        "ammo": 5,   "clip": 5,  "cost": 10,   "max_ammo": 50,   "dmg": 6.0,                   "cooldown": 0.33, "range": 15.0,   "ease": 0.0, "sounds": {"raise": Sound.ut_bio_rifle_raise,       "primary": Sound.ut_bio_rifle_primary}}, # Sound.w3d_pistol_shot
	{"name": "Shock Rifle",      "ammo": 50,  "clip": 2,  "cost": 20,   "max_ammo": 100,  "dmg": 15.0,                  "cooldown": 1.0,  "range": 50.0,   "ease": 1.0, "sounds": {"raise": Sound.ut_shock_rifle_raise,     "primary": Sound.ut_shock_rifle_primary}}, # Sound.ut_shock_rifle_secondary :: Sound.w3d_rifle_shot2
	{"name": "Pulse Gun",        "ammo": 50,  "clip": 50, "cost": 50,   "max_ammo": 100,  "dmg": 3.0,                   "cooldown": 0.2,  "range": 50.0,   "ease": 0.0, "sounds": {"raise": Sound.ut_pulse_gun_raise,       "primary": Sound.ut_pulse_gun_primary, "primary_loop": true}}, # Sound.ut_pulse_gun_secondary :: Sound.w3d_pistol_shot / Sound.w3d_salvo
	{"name": "Minigun",          "ammo": 500, "clip": 50, "cost": 100,  "max_ammo": 1000, "dmg": 2.0,                   "cooldown": 0.1,  "range": 100.0,  "ease": 0.0, "sounds": {"raise": Sound.ut_minigun_raise,         "primary": Sound.ut_minigun_primary, "primary_loop": true}}, # Sound.ut_minigun_secondary :: Sound.w3d_pistol_shot / Sound.w3d_salvo
	{"name": "Flak Cannon",      "ammo": 50,  "clip": 50, "cost": 200,  "max_ammo": 100,  "dmg": 100.0,                 "cooldown": 1.0,  "range": 20.0,   "ease": 1.0, "sounds": {"raise": Sound.ut_flak_raise,            "primary": Sound.ut_flak_primary}},
	{"name": "Rocket Launcher",  "ammo": 50,  "clip": 50, "cost": 500,  "max_ammo": 100,  "dmg": 200.0,                 "cooldown": 1.0,  "range": 1000.0, "ease": 0.0, "sounds": {"raise": Sound.ut_rocket_launcher_raise, "primary": Sound.ut_rocket_launcher_primary}},
	{"name": "Ripper",           "ammo": 100, "clip": 50, "cost": 1000, "max_ammo": 200,  "dmg": 200.0,                 "cooldown": 0.5,  "range": 100.0,  "ease": 1.0, "sounds": {"raise": Sound.ut_ripper_raise,          "primary": Sound.ut_ripper_primary}},
	{"name": "Sniper Rifle",     "ammo": 50,  "clip": 50, "cost": 2000, "max_ammo": 100,  "dmg": 200.0,                 "cooldown": 1.5,  "range": 5000.0, "ease": 0.0, "sounds": {"raise": Sound.ut_sniper_raise,          "primary": Sound.ut_sniper_primary}}, # Sound.w3d_rifle_shot
	{"name": "Redeemer",         "ammo": 1,   "clip": 1,  "cost": 1000, "max_ammo": 1,    "dmg": 1000.0,  "start": 2.0, "cooldown": 2.0,                                "sounds": {"raise": Sound.ut_redeemer_raise,        "primary": Sound.ut_redeemer_primary}}, # Sound.w3d_punch
	{"name": "Seeder",           "ammo": 50,  "clip": 20, "cost": 1,    "max_ammo": 100,  "colmask": 0b1,               "cooldown": 0.5, "continuous": false}, # terrain
	{"name": "Builder",          "ammo": 10,  "clip": 20, "cost": 1000, "max_ammo": 100,  "colmask": 0b1,               "cooldown": 1.0, "continuous": false}, # terrain
	{"name": "Architect",        "ammo": 10,  "clip": 20, "cost": 100,  "max_ammo": 100,  "colmask": 0b1,               "cooldown": 1.0, "continuous": false}, # terrain
]
export var weapon: int = -1 setget set_weapon # Starting weapon as index of weapons
var ammo = [] # Better optimized array for ammo from weapons dictionary
var primary_continuous: bool = true # opposite of oneshot
var primary_range: float = -1.0 # bellow 0 for infinity
var primary_ease: float = 0.0 # ease curve (2nd param of ease()) for damage by primary_range
var using_primary: bool = false
var primary_timer: float = 0.0
var primary_time_to_start: float = 0.0
var primary_cooldown_timer: float = 0.5 # has to be init by primary_cooldown time to use immediately after primary_time_to_start
var primary_cooldown: float = 0.5 # also reload time

var grass: MultiMeshInstance

export var trees = [
	preload("res://assets/models/nature/tree/trunk.tscn"),
	preload("res://assets/models/nature/tree/simple.tscn"),
	preload("res://assets/models/nature/tree/pine/pine.tscn"),
#	preload("res://assets/plants/trees-0/tree_dark1.scn"),
#	preload("res://assets/plants/trees-0/tree_dark2.scn"),
#	preload("res://assets/plants/trees-0/tree_dark3.scn"),
#	preload("res://assets/plants/trees-0/tree_light.scn"),
	#preload("res://assets/models/buildings/house.tscn")
]
export var buildings = [
	preload("res://assets/models/buildings/house.tscn"),
	preload("res://assets/models/mobs/nest/tower.tscn"),
	#preload("res://assets/models/buildings/simple_tower.tscn"),
]

var raycast: RayCast
var flashlight: SpotLight

#onready var left_touch_joystick_button = $"/root/Global/HUD/MarginContainer/VBoxContainer/BottomHBoxContainer/LeftTouch/Joystick/Button"
#onready var right_touch_joystick_button = $"/root/Global/HUD/MarginContainer/VBoxContainer/BottomHBoxContainer/RightTouch/Joystick/Button"
var left_touch_joystick_button
var right_touch_joystick_button

func _enter_tree():
	Log.verbose("_enter_tree() at: " + get_path(), logchan)
	load_settings(true)
	Settings.connect("settings_loaded", self, "load_settings")
	Log.verbose("Settings.connect(settings_loaded) to Player.load_settings()", logchan)
	Settings.connect("save_settings", self, "save_settings")
	Log.verbose("Settings.connect(save_settings) to Player.save_settings()", logchan)

func load_settings(onstart := false) -> void:
	Log.verbose("Player.load_settings(" + str(onstart) + ")", logchan)
	Settings.load_to(self, settings_section, settings_properties)
	if not onstart:
		load_settings_ready()

func load_settings_ready(onstart := false) -> void:
	Log.verbose("Player.load_settings_ready(" + str(onstart) + ")", logchan)
	Settings.load_to(self, settings_section, settings_properties_ready)

func save_settings(send_saved := true):
	Log.verbose("Player.save_settings()", logchan)
	Settings.save_from(self, settings_section, settings_properties + settings_properties_ready)
	if send_saved:
		Settings.saved(self)

func _ready() -> void:
	Log.verbose("_ready()", logchan)
	#set_process(false)
	#set_process_input(false)
	#set_process_unhandled_input(false)
	#set_process_internal(false)
	#set_physics_process(false)

	left_touch_joystick_button = UI.hud.left_touch_joystick.get_node("Button")
	right_touch_joystick_button = UI.hud.right_touch_joystick.get_node("Button")

	#cam = load("res://core/input/camera.gd").instance()
	cam = PlayerCamera.new()
	if has_node("CameraBase"):
		cam.translation = $CameraBase.translation
	#get_tree().get_root().add_child(cam)
	add_child(cam)
	cam.current = true
	#cam.set_as_toplevel(true)
	cams[0] = cam

	# Init ammo
	for _weapon in weapons:
		ammo.append(_weapon["ammo"] if _weapon.has("ammo") else 0)

	call_deferred("when_ready")

func when_ready():
	Log.verbose("when_ready()", logchan)
	# TODO: Mouse isn't captured on touch screen
	#mouse_captured = true if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED else false
	#mouse_captured = (Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED)
	mouse_captured = false if Global.has_touch else (Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED)

	load_settings_ready(true)
	set_hp(hp)
	if weapon < 0:
		set_weapon(0, false)

	# Move player's flashlight under cam
	if flashlight:
		flashlight.get_parent().remove_child(flashlight)
		cam.add_child(flashlight)
	# Move player's raycast under cam
	if raycast:
		raycast.get_parent().remove_child(raycast)
		cam.add_child(raycast)

	if LevelManager.current_scene.has_node("Grass"):
		grass = LevelManager.current_scene.get_node("Grass")

	#set_process_input(true)
	# Needed for left_touch_joysticks (when emulating by mouse)
	#set_physics_process(true)

# warning-ignore:unused_argument
func _process(delta):
	var cam_water_level = cam.global_translation.y - water_level
	if underwater:
		if cam_water_level > 0:
			set_underwater(false)
		elif not can(CAN_BREATHE_UNDERWATER):
			underwater_timer -= delta
			if underwater_timer < 0.0:
				underwater_no_breath_timer -= delta
				if underwater_no_breath_timer < 0.0:
					var half_damage := underwater_no_breath_damage / 2.0
					damage(half_damage, half_damage, underwater_no_breath_damage_per)
					Sound.play_underwater_no_breath()
					underwater_no_breath_timer = underwater_no_breath_damage_per
	else:
		if cam_water_level < 0:
			set_underwater(true)

	if grass:
		grass.material_override.set_shader_param("player_pos", global_transform.origin)

func set_underwater(new_underwater: bool) -> void:
	# Nothing changes
	if new_underwater == underwater:
		return
	# Toggle
	underwater = new_underwater
	Sound.play_bubbling(self, false)

	if underwater:
		#LevelEnvironment.set_environment("underwater")
		#LevelEnvironment.env.environment.fog_enabled = false
		LevelEnvironment.push()
		if not can(CAN_BREATHE_UNDERWATER):
			underwater_timer = held_breath_limit
	else:
		#LevelEnvironment.set_environment("lightblue")
		LevelEnvironment.pop()

func _unhandled_input(event: InputEvent) -> void:
	#if Log.enabled:
	#Log.verbose("_unhandled_input( " + str(event) + " )", logchan)

	if event.is_pressed():
#		#if event.button_index == BUTTON_RIGHT:
#			var tree = Tree.instance()
#			tree.translate(translation) # + Vector3(5, 0, 0))
#			#tree.rotate_object_local(Vector3.UP, rand_range(0, 6.28))
#			#tree.rotate_object_local(Vector3.LEFT, rand_range(-0.25, 0.25)) # rand_range(-0.5, 0.5) like after storm
#			tree.rotate_x(rand_range(-0.25, -0.25))
#			#tree.rotate_object_local(Vector3.FORWARD, rand_range(-0.25, 0.25)) # rand_range(-0.5, 0.5) like after storm
#			tree.rotate_z(rand_range(-0.25, -0.25))
#			#tree.scale_object_local(Vector3(rand_range(9, 11)/10, rand_range(75, 150)/10, rand_range(9,11)/10))
#			tree.rotate_y(rand_range(0, 6.28))
#			get_parent().add_child(tree)

		#elif event.is_action("camera_zoom_in"):
		if event is InputEventMouse and event.button_index == BUTTON_WHEEL_UP:
			if event.control:
				if not mouse_captured:
					return
				if cam.projection == Camera.PROJECTION_PERSPECTIVE:
					cam.fov = max(cam_fov_min, cam.fov / zoom_speed)
				else:
					cam.size /= 1.1
			else:
				set_weapon(weapon - 1 if weapon > 0 else weapons.size() - 1, true, true)

		#elif event.is_action("camera_zoom_out"):
		elif event is InputEventMouse and event.button_index == BUTTON_WHEEL_DOWN:
			if event.control:
				if not mouse_captured:
					return
				if cam.projection == Camera.PROJECTION_PERSPECTIVE:
					cam.fov = min(cam_fov_max, cam.fov * zoom_speed)
				else:
					cam.size *= 1.1
			else:
				set_weapon(weapon + 1 if weapon < weapons.size() - 1 else 0, true, true)

		elif event.is_action("camera_zoom_reset"):
			fov_reset()

		elif event.is_action("switch_speed"):
			if speed >= 5000:
				set_speed(50)
			elif speed >= 2000:
				set_speed(5000)
			elif speed >= 1000:
				set_speed(2000)
			elif speed >= 500:
				set_speed(1000)
			elif speed >= 200:
				set_speed(500)
			elif speed >= 100:
				set_speed(200)
			elif speed >= 50:
				set_speed(100)

		#elif event.button_index == BUTTON_MIDDLE:
		elif event.is_action("camera_toggle_view"):
			cam_toggle_view()

		elif event.is_action("camera_bottom_view"):
			cam_bottom_view()
		elif event.is_action("camera_top_view"):
			cam_top_view()

		elif event.is_action("camera_back_view"):
			cam_back_view()
		elif event.is_action("camera_front_view"):
			cam_front_view()

		elif event.is_action("camera_left_view"):
			cam_left_view()
		elif event.is_action("camera_right_view"):
			cam_right_view()

		elif event.is_action("camera_arvr"):
			if Global.toggle_arvr():
				cam_arvr.far = cam.far
				cam.current = false
				cam = cam_arvr
				cam.current = true
			else:
				cam_1st()

		elif event.is_action("camera_toggle_projection"):
			cam_toggle_projection()

		elif event.is_action("camera_far_further"):
			set_cam_far(min(2000000, cam_far * 1.01))
		elif event.is_action("camera_far_closer"):
			set_cam_far(max(10, cam_far / 1.01))

		elif event.is_action("tool_1"):
			set_weapon(0, true, true)
		elif event.is_action("tool_2"):
			set_weapon(1, true, true)
		elif event.is_action("tool_3"):
			set_weapon(2, true, true)
		elif event.is_action("tool_4"):
			set_weapon(3, true, true)
		elif event.is_action("tool_5"):
			set_weapon(4, true, true)
		elif event.is_action("tool_6"):
			set_weapon(5, true, true)
		elif event.is_action("tool_7"):
			set_weapon(6, true, true)
		elif event.is_action("tool_8"):
			set_weapon(7, true, true)
		elif event.is_action("tool_9"):
			set_weapon(8, true, true)
		elif event.is_action("tool_10"):
			set_weapon(9, true, true)

		# Input is not handled
		else:
			return

		Log.debug("handled " + str(event), logchan)
		scene_tree.set_input_as_handled()

func can(ability: int) -> bool:
	#return abilities[ability]
	#return bool(randi() % 2)
	return abilities & ability

func allow(ability: int) -> void:
	abilities |= ability

func deny(ability: int) -> void:
	abilities &= ~ability

func toggle_ability(ability: int) -> void:
	abilities ^= ability

func set_ability(ability: int, value: bool) -> void:
#	if abilities & ability:
	if value:
		#allow(ability)
		abilities |= ability
	else:
		#deny(ability)
		abilities &= ~ability

func show_model():
	if not get_viewport().arvr:
		model.show()

func hide_model():
	if get_viewport().arvr or cam != cam_3rd:
		model.hide()

# warning-ignore:function_conflicts_variable
func cam_1st() -> void:
	cam_1st.far = cam.far
	cam_1st.projection = cam.projection
	cam_1st.fov = cam.fov
	cam_1st.size = cam.size
	cam.current = false
	cam = cam_1st
	cam.current = true
	UI.hud.crosshair.show()
	hide_model()

# warning-ignore:function_conflicts_variable
func cam_3rd() -> void:
	cam_3rd.far = cam.far
	cam_3rd.projection = cam.projection
	cam_3rd.fov = cam.fov
	cam_3rd.size = cam.size
	cam.current = false
	cam = cam_3rd
	cam.current = true
	UI.hud.crosshair.hide()
	show_model()

func cam_toggle_view() -> void:
	if cam_1st.current:
		cam_3rd()
	else:
		cam_1st()

func cam_bottom_view() -> void:
	rotation.y = 0
	#cam_1st()
	#cam_orthogonal()
	cam.rotation.x = deg2rad(90)

func cam_top_view() -> void:
	rotation.y = 0
	#cam_1st()
	#cam_orthogonal()
	cam.rotation.x = deg2rad(-90)

func cam_back_view() -> void:
	rotation.y = deg2rad(180)
	#cam_1st()
	#cam_orthogonal()
	cam.rotation.x = deg2rad(0)

func cam_front_view() -> void:
	rotation.y = 0
	#cam_1st()
	#cam_orthogonal()
	cam.rotation.x = deg2rad(0)

func cam_left_view() -> void:
	rotation.y = deg2rad(-90)
	#cam_1st()
	#cam_orthogonal()
	cam.rotation.x = deg2rad(0)

func cam_right_view() -> void:
	rotation.y = deg2rad(90)
	#cam_1st()
	#cam_orthogonal()
	cam.rotation.x = deg2rad(0)

func cam_perspective() -> void:
	cam.projection = Camera.PROJECTION_PERSPECTIVE
	# @TODO There should be some conversion ratio
	#cam.fov = cam.size

func cam_orthogonal() -> void:
	cam.projection = Camera.PROJECTION_ORTHOGONAL
	# @TODO There should be some conversion ratio
	#cam.size = cam.fov

func cam_toggle_projection() -> void:
	# Projection: PROJECTION_PERSPECTIVE=0, PROJECTION_ORTHOGONAL=1, PROJECTION_FRUSTUM=2
	if cam.projection == Camera.PROJECTION_PERSPECTIVE:
		cam_orthogonal()
	else:
		cam_perspective()

func fov_reset() -> void:
	cam.fov = cam_fov_default

func set_cam_far(new_far: float) -> void:
	cam_far = new_far
	cam.far = new_far
	LevelEnvironment.set_fog_depth(new_far)

func set_size(new_size: float, update_ui := true) -> void:
	size = min(max_size, max(min_size, new_size))
	Log.debug("set_size(" + str(new_size) + "): " + str(size), logchan)
	scale = Vector3(size, size, size)
	if update_ui and not mouse_captured:
		UI.hud.top_menu.update_sizebar()

func set_zoom(new_zoom: float, update_ui := true) -> void:
	Log.debug("set_zoom(" + str(new_zoom) + ")", logchan)
	if cam.projection == Camera.PROJECTION_PERSPECTIVE:
		cam.fov = max(cam_fov_min, cam.fov / zoom_speed)
	else:
		cam.size /= 1.1
	if update_ui and not mouse_captured:
		UI.hud.top_menu.update_zoombar()

# @TODO: Changing speed by other way than Speedbar leads to next change propagated by Speedbar (when mouse isn't captured)
func set_speed(new_speed: float, update_ui := true) -> void:
	speed = min(max_speed, new_speed)
	Log.debug("set_speed(" + str(new_speed) + ", " + str(update_ui) + "): " + str(speed) + " / " + str(max_speed), logchan)
	if update_ui and not mouse_captured:
		UI.hud.top_menu.update_speedbar()

func set_weapon(num: int, update_ui := true, play_weapon_raise := false) -> void:
	if update_ui:
		UI.hud.top_menu.update_weapon(weapon, num)

	weapon = num
	if raycast:
		raycast.collision_mask = weapons[num]["colmask"] if weapons[num].has("colmask") else 0b10000

	Sound.reset_weapon()
	if weapons[num].has("sounds"):
		if weapons[num]["sounds"].has("raise"):
			Sound.weapon_raise = weapons[num]["sounds"]["raise"]
		if weapons[num]["sounds"].has("primary"):
			Sound.weapon_primary = weapons[num]["sounds"]["primary"]
		if weapons[num]["sounds"].has("primary_loop"):
			Sound.weapon_primary_loop = weapons[num]["sounds"]["primary_loop"]
	Sound.update_weapon_by_default()
	if play_weapon_raise:
		Sound.play_weapon_raise()

	primary_continuous    = weapons[num]["continuous"] if "continuous" in weapons[num] else true
	primary_range         = weapons[num]["range"]      if "range"      in weapons[num] else -1.0
	primary_ease          = weapons[num]["ease"]       if "ease"       in weapons[num] else 0.0
	primary_time_to_start = weapons[num]["start"]      if "start"      in weapons[num] else 0.0
	primary_cooldown      = weapons[num]["cooldown"]   if "cooldown"   in weapons[num] else 0.0
	Log.debug("change_weapon(): " \
		+ ("collision_mask=" + str(raycast.collision_mask) + ", " if raycast else "") \
		+ "continuous: " + str(primary_continuous) \
		+ ", primary_time_to_start: " + str(primary_time_to_start) \
		+ ", primary_cooldown=" + str(primary_cooldown), logchan)

func set_hp(new_hp: float) -> void:
	# negative hp is useful to see how much damage player received when is dead
	hp = min(hp_max, new_hp)
	if hp <= 0.0:
		UI.hud.dead.visible = true
	elif hp <= 50.0:
		UI.hud.blood.visible = true
		UI.hud.blood.modulate = Color(1, 1, 1, ease((hp_max - hp * 2) / hp_max, 2.0))
	else:
		UI.hud.blood.visible = false

func change_hp(instant: float, overtime := 0.0, duration := 0.0) -> bool:
	set_hp(hp + instant)
	if duration > 0.0:
		var tween := create_tween()
		tween.tween_property(self, "hp", hp + overtime, duration)
	return hp > 0	# true for alive, false for dead

func damage(amount: float, overtime := 0.0, duration := 0.0) -> bool:
	return change_hp(-amount, -overtime, duration)
