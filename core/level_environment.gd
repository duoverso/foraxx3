extends Node

var logchan := "env"

# @TODO it looks like switching environment is fast but loading is blocking,
# so better threaded loading could solve problems

var environments := [
	#"default_env",
	#"fs003_day_sunless",
#	"fs003_day",
	#"fs003_night_moonless",
#	"fs003_night",
#	"fs003_rainy",
#	"fs003_snowy",
#	"fs003_sunrise", # dark orange ground with sun & clouds
#	"fs003_sunset", # orange ground with sun & clouds
#	"starmap",		# space night with real stars seen from Earth
#	"tutsplus_001", # (sunrise blue) ocean with clouds
#	"tutsplus_002", # (sunrise orange) ocean with clouds
#	"tutsplus_003", # (dark) ocean with clouds
#	"tutsplus_004", # (half-light) ocean with big cloud at half of the sky
#	"tutsplus_005", # (pink) ocean at sunset
#	"tutsplus_006", # ocean at sunset with stars & moon
#	"tutsplus_007", # ocean at sunset with clouds & moon
#	"tutsplus_008", # (blue) ocean at night with stars, planet & moon
]
var current := "res://assets/environment/lightblue.tres"

export var environments_dirs := [
	"res://assets/environment",
	"user://environment",
	#"user://repos/antray/environment",
]
#export var user_environments_dir = "user://environment"

var light: DirectionalLight

var env: WorldEnvironment
var previous := []
var underwater = preload("res://assets/environment/underwater.tres")
var fade_time := 1.0

# See https://github.com/godotengine/godot-demo-projects/blob/3.0-d69cc10/misc/threads/thread.gd
#onready var mutex = Mutex.new()
onready var thread = Thread.new()

func _enter_tree():
	Log.verbose("_enter_tree() at: " + get_path(), logchan)

func _ready():
	Log.verbose("_ready()", logchan)
	#env = get_tree().get_root().get_node("Global/WorldEnvironment")
	env = get_node("/root/Global/WorldEnvironment")
	light = get_node("/root/Global/DirectionalLight")
	load_all_environments()

func load_all_environments() -> void:
	environments = []
	for env_dir in environments_dirs:
		environments += load_environments_from(env_dir)

	Log.info("Environments: " + str(environments), logchan)

func load_environments_from(env_dir: String) -> Array:
	var result = []
	var dir = Directory.new()
	if !dir.dir_exists(env_dir):
		Log.err("!! Environment directory " + env_dir + " doesn't exist.", logchan)
		#dir.make_dir(env_dir)
	if dir.open(env_dir) != OK:
		Log.err("!! Environment directory " + env_dir + " can't be opened.", logchan)
	else:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			#if dir.current_is_dir():
				# @todo Recursive look up for levels
				#pass
			# Only .import files are read on Android (see https://github.com/godotengine/godot/issues/39474)
			if not dir.current_is_dir() and file_name.match("*.tres"):
				Log.debug("Environment found: " + file_name, logchan)
				#result.append(file_name.get_basename())
				result.append(env_dir + "/" + file_name)
			file_name = dir.get_next()

	if result.size() == 0:
		Log.err("!! No environments found at " + env_dir, logchan)

	return result

#func env():
	#return current_scene.get_node("WorldEnvironment")
	#return $"/root/Level/WorldEnvironment"
	#return $"/root/Global/WorldEnvironment"

# @TODO currently switch only underwater environment
func push() -> void:
	previous.push_back(env.environment)
	env.environment = underwater
	if Log.enabled:
		Log.debug("env.push()", logchan)

func pop() -> void:
	env.environment = previous.pop_back()
	if Log.enabled:
		Log.debug("env.pop()", logchan)

func set_environment(new_env: String, fade := true): # -> bool|GDScriptFunctionState (by yield())
	if (thread.is_active()):
	#if (thread.is_alive()):
		Log.err("Thread is still active", logchan)
		return false

	if not new_env in environments:
		Log.err("!! Unknown environment: " + new_env, logchan)
		return false

	# Fade out current env
	if fade:
		Log.debug("Fade out background_energy", logchan)
		var tween := create_tween().set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_OUT)
		tween.tween_property(env.environment, "background_energy", 0.0, fade_time)
		if env.environment.background_mode == Environment.BG_COLOR:
			Log.debug("Fade out background_color", logchan)
			tween.parallel().tween_property(env.environment, "background_color", Color(0, 0, 0), fade_time)
		yield(tween, "finished")
	current = new_env
	Log.debug("@@ Environemnt: " + new_env, logchan)
	thread.start(self, "set_environment_threaded", [new_env, fade], Thread.PRIORITY_LOW)
	#thread.call_deferred("wait_to_finish")
	return true

func set_environment_threaded(userdata):
	# @TODO: isn't unlocked on next calls
	#mutex.lock()
	var new_env: String = userdata[0]
	var fade: bool = userdata[1]
	var fog_enabled = env.environment.fog_enabled
	var fog_depth_begin = env.environment.fog_depth_begin
	var fog_depth_end = env.environment.fog_depth_end
	#env.environment = load(user_environments_dir + "/" + new_env + ".tres")
	env.environment = load(new_env)
	# Start with environment's energy 0 if it should be faded in
	env.environment.fog_enabled = fog_enabled
	env.environment.fog_depth_begin = fog_depth_begin
	env.environment.fog_depth_end = fog_depth_end
	env.environment.fog_color = Color.black
	if fade:
		Log.debug("Fade in background_energy", logchan)
		var background_energy = env.environment.background_energy
		env.environment.background_energy = 0.0
		var tween = create_tween().set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_IN)
		tween.tween_property(env.environment, "background_energy", background_energy, fade_time)
		if env.environment.background_mode == Environment.BG_COLOR:
			Log.debug("Fade in background_color", logchan)
			var background_color = env.environment.background_color
			env.environment.background_color = Color(0, 0, 0)
			tween.parallel().tween_property(env.environment, "background_color", background_color, fade_time)
		yield(tween, "finished")
	thread.call_deferred("wait_to_finish")
	#mutex.unlock()
#	call_deferred("set_environment_done")
#	#return res

#func set_environment_done():
#	#var res = thread.wait_to_finish()
#	thread.wait_to_finish()

func rand_environment(fade := true) -> void:
	# Select another random environment
	var another_env := environments.duplicate()
	another_env.erase(current)
	if another_env.empty():
		Log.err("!! No other environment is loaded", logchan)
		UI.hud.set_event_text("No other environment is loaded")
		return
	var rand_env = another_env[randi() % another_env.size()]

	# Inform in HUD (and change in menu)
	UI.hud.set_event_text("Random environment: " + rand_env, 1.0)
	var menu = UI.hud.top_menu.menus["environment"]
	for item_idx in range(menu.get_item_count()):
		if !menu.is_item_checkable(item_idx):
			continue
		var item_text: String = menu.get_item_text(item_idx)
		if item_text == rand_env:
			menu.set_item_checked(item_idx, true)
		else:
			menu.set_item_checked(item_idx, false)
	Log.debug("@@ Random environment: " + rand_env, logchan)

	set_environment(rand_env, fade)

func set_fog_depth(depth: float) -> void:
	if env:
		env.environment.fog_depth_end = depth
		env.environment.fog_depth_begin = env.environment.fog_depth_end / 2
