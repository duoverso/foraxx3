# https://docs.godotengine.org/en/stable/tutorials/networking/high_level_multiplayer.html
extends Node

var peer
#var addr = "antray.duoverso.com"  # for client
var addr = "127.0.0.1"  # for client
var port = 30123  # for server & client
var max_peers = 50  # for server

# Player info, associate ID to data
var players = {}
var players_ready = []
var my_info = { name = "Johnson Magenta", favorite_color = Color8(255, 0, 255) }

func _ready():
	print("(client) _ready()")
	## Server & Clients
	get_tree().connect("network_peer_connected", self, "_peer_connected")
	get_tree().connect("network_peer_disconnected", self, "_peer_disconnected")
	## Clients
	get_tree().connect("connected_to_server", self, "_connected_to_server")
	get_tree().connect("connection_failed", self, "_connection_failed")
	get_tree().connect("server_disconnected", self, "_server_disconnected")

	#start_server()
	#return
	#connect_client()

	# Get peer ID (server has always 1, clients higher)
	#var peer_id = get_network_unique_id()

	#var lobby = preload("./lobby.gd")
	#print(lobby)

	#scene_tree.quit()

### Client
func connect_client():
	peer = NetworkedMultiplayerENet.new()
	peer.create_client(addr, port)
	get_tree().set_network_peer(peer)
	print("Connecting to server... ", peer)

func disconnect_client():
	get_tree().set_network_peer(null)
	print("Client stopped")

### Server
func start_server():
	peer = NetworkedMultiplayerENet.new()
	peer.create_server(port, max_peers)
	get_tree().set_network_peer(peer)
	print("Server started: ", peer)

func server_stop():
	get_tree().set_network_peer(null)
	print("Server stopped")

func server_allow_connections(allow):
	get_tree().set_refuse_new_network_connections(!allow)

### Signals

# Server & Clients
func _peer_connected(id):
	print("Peer connected: ", id)
	# Called on both clients and server when a peer connects. Send my info to it.
	rpc_id(id, "register_player", my_info)

func _peer_disconnected(id):
	print("Peer disconnected: ", id)
	players.erase(id)

# Clients
func _connected_to_server():
	print("Connected to server")

func _connection_failed():
	print("Connection failed")

func _server_disconnected():
	print("Server disconnected")

### RPC
# remote: that the rpc() call will go via network and execute remotely
# remotesync: that the rpc() call will go via network and execute remotely, but will also execute locally (do a normal function call)
# master
# puppet

remote func register_player(info):
	print("register_player(): ", info)
	# Get the id of the RPC sender.
	var id = get_tree().get_rpc_sender_id()
	# Store the info
	players[id] = info

	# Call function to update lobby UI here

remote func pre_configure_game():
	var selfPeerID = get_tree().get_network_unique_id()
	get_tree().set_pause(true) # Pre-pause

	# Load world
	#var world = load(which_level).instance()
	#get_node("/root").add_child(world)

	# Load my player
##	var my_player = preload("res://assets/vehicles/low-poly-ducati/ducati.tscn").instance()
##	my_player.set_name(str(selfPeerID))
##	my_player.set_network_master(selfPeerID) # Will be explained later
##	get_node("/root/Global/Players").add_child(my_player)

	# Load other players
##	for p in players:
##		var player = preload("res://assets/vehicles/low-poly-car/low-poly-car.tscn").instance()
##		player.set_name(str(p))
##		player.set_network_master(p) # Will be explained later
##		get_node("/root/Global/Players").add_child(player)

	# Tell server (remember, server is always ID=1) that this peer is done pre-configuring.
	# The server can call get_tree().get_rpc_sender_id() to find out who said they were done.
	rpc_id(1, "done_preconfiguring")

remote func done_preconfiguring():
	var who = get_tree().get_rpc_sender_id()
	# Here are some checks you can do, for example
	assert(get_tree().is_network_server())
	assert(who in players) # Exists
	assert(not who in players_ready) # Was not added yet

	players_ready.append(who)

	if players_ready.size() == players.size():
		rpc("post_configure_game")

remote func post_configure_game():
	# Only the server is allowed to tell a client to unpause
	if 1 == get_tree().get_rpc_sender_id():
		get_tree().set_pause(false)
		# Game starts now!
