extends Node

# By https://docs.godotengine.org/en/3.4/tutorials/networking/http_client_class.html
func http_client():
	var http := HTTPClient.new()
	var error := http.connect_to_host("antray.duoverso.com", 443, true)
	print("HTTP Error: ", error)
	assert(error == OK)

	# Wait until resolved and connected.
	while http.get_status() == HTTPClient.STATUS_CONNECTING or http.get_status() == HTTPClient.STATUS_RESOLVING:
		http.poll()
		print("Connecting...")
		if not OS.has_feature("web"):
			OS.delay_msec(100)
		else:
			yield(Engine.get_main_loop(), "idle_frame")

	assert(http.get_status() == HTTPClient.STATUS_CONNECTED) # Check if the connection was made successfully.

	# Some headers
	var headers = [
		"User-Agent: AntRay/3.3 (Godot)",
		"Accept: */*"
	]

	error = http.request(HTTPClient.METHOD_GET, "/assets/music/bamboo-forest.ogg.oggstr", headers)
	assert(error == OK)

	while http.get_status() == HTTPClient.STATUS_REQUESTING:
	# Keep polling for as long as the request is being processed.
		http.poll()
		print("Requesting...")
		if OS.has_feature("web"):
		# Synchronous HTTP requests are not supported on the web,
		# so wait for the next main loop iteration.
			yield(Engine.get_main_loop(), "idle_frame")
		else:
			OS.delay_msec(500)

	assert(http.get_status() == HTTPClient.STATUS_BODY or http.get_status() == HTTPClient.STATUS_CONNECTED) # Make sure request finished well.

	if http.has_response():
		# If there is a response...
		headers = http.get_response_headers_as_dictionary() # Get response headers.
		print("code: ", http.get_response_code()) # Show response code.
		print("headers: ", headers) # Show headers.

		# Getting the HTTP Body
		if http.is_response_chunked():
			# Does it use chunks?
			print("Response is Chunked!")
		else:
			# Or just plain Content-Length
			var bl = http.get_response_body_length()
			print("Response Length: ", bl)

			# This method works for both anyway

		var rb = PoolByteArray() # Array that will hold the data.

		while http.get_status() == HTTPClient.STATUS_BODY:
			# While there is body left to be read
			http.poll()
			# Get a chunk.
			var chunk = http.read_response_body_chunk()
			if chunk.size() == 0:
				if not OS.has_feature("web"):
					# Got nothing, wait for buffers to fill a bit.
					OS.delay_usec(1000)
				else:
					yield(Engine.get_main_loop(), "idle_frame")
			else:
				rb = rb + chunk # Append to read buffer.
			# Done!

		print("bytes got: ", rb.size())
		#var hex = rb.hex_encode()
		#print("Hex: ", hex)
