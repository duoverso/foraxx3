##### AntRay Downloader
#
# Download to file each time download() is called (do not check file size, duplicity in download queues...)
# download({"https://via.placeholder.com/512": {"file": "user://placeholder-512.png"}}, false)
#
# Download to file if size is lower than specified
# download({"https://via.placeholder.com/512": {"file": "user://placeholder-512.png", "size": 1746}})
#
# Download multiple files
# download({
#   "https://via.placeholder.com/256": {"file": "user://placeholder-256.png", "size": 1115},
#   "https://via.placeholder.com/512": {"file": "user://placeholder-512.png", "size": 1746},
# })

extends Node

var logchan := "download"

var is_downloaded := false
# Dictionary queues with url as key and options as value
var to_download := {}
var downloading := {}

onready var use_threads := OS.can_use_threads()
var max_children := 2

func _enter_tree():
	Log.verbose("_enter_tree() at: " + get_path(), logchan)

func _ready():
	Log.verbose("_ready()", logchan)
	if Global.is_web:
		if use_threads:
			use_threads = false
			Log.notice("Disabling use_threads for web", logchan)

# Add to download queue
func download(items: Dictionary = {}, check_duplicity := true):
	# Add items to to_download if not present in to_download or downloading
	Log.verbose("download( " + str(items) + " )", logchan)
	for url in items: # items.keys():
		if check_duplicity and (to_download.has(url) or downloading.has(url)):
			continue
		to_download[url] = items[url]
	process_downloading()

# Process download queue
func process_downloading():
	# Is there something to process? How many new children could be created?
	var max_new_children = min(to_download.size(), max_children - downloading.size())
	Log.verbose("process_downloading(): max_new_children = " + str(max_new_children) + " = min(" + str(to_download.size()) + ", " + str(max_children) + " - " + str(downloading.size()) + ")", logchan)
	if max_new_children <= 0:
		return

	var dir = Directory.new()
	var urls_to_download = to_download.keys() # .slice(0, max_new_children)
	# count created children because some urls_to_download could be already downloaded and skipped
	var created_children := 0
	Log.verbose("urls_to_download = " + str(urls_to_download), logchan)
	for url in urls_to_download:
		Log.verbose("processing url: " + url, logchan)
		# Move item from to_download to downloading
		var options = to_download[url]
		downloading[url] = options
		to_download.erase(url)

		# Create parent directories recursively
		if "file" in options:
			var base_dir = options["file"].get_base_dir()
			if not dir.dir_exists(base_dir):
				Log.info("Creating directory " + base_dir, logchan)
				dir.make_dir_recursive(base_dir)
			if "size" in options:
				var file = File.new()
				if file.file_exists(options["file"]):
					file.open(options["file"], File.READ)
					if file.get_len() >= options["size"]:
						Log.debug("File " + options["file"] + " already downloaded (" + str(options["size"]) + " B)", logchan)
						downloading.erase(url)
						continue
					else:
						Log.debug("File " + options["file"] + " downloaded partialy (" + str(file.get_len(), " / ", options["size"]) + " B)", logchan)

		# Create an HTTP request node and connect its completion signal.
		var http_request = HTTPRequest.new()
		add_child(http_request)
		http_request.connect("request_completed", self, "_http_request_completed", [http_request, url, options])
		if use_threads:
			http_request.set_use_threads(true) # Doesn't work on HTML5
		# Write file while downloading
		#if item.has("file"):
		#	http_request.set_download_file(item["file"])
		var error = http_request.request(url)
		Log.debug("<" + str(error) + "> Downloading " + url \
			+ (" to " + options["file"] if "file" in options else "") \
			, logchan)

		created_children += 1
		if created_children >= max_new_children:
			break

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _http_request_completed(result, response_code, headers, body: PoolByteArray, http_request: HTTPRequest, url: String, options: Dictionary):
	#yield(get_tree().create_timer(1.0), "timeout")

	#var response = parse_json(body.get_string_from_utf8())
	#Log.debug(response.headers["User-Agent"], logchan)
	#Log.debug(str(body.hex_encode()), logchan)

	#if response_code == 200:
	Log.info(str(response_code) + " Downloaded " + url, logchan)
	Log.verbose(str("Size: ", body.size()), logchan)

	# Write file when downloaded
	if "file" in options:
		var file = File.new()
		file.open(options["file"], File.WRITE)
		Log.verbose(str("get_len() after open(): ", file.get_len()), logchan)
		# Write file without replacements
		if !options.has("replace"):
			file.store_buffer(body)
		# Write file after replacements
		else:
			Log.verbose("Replace: " + str(options["replace"]), logchan)
			# Convert PoolByteArray to String to apply replacements
			var content: String = body.get_string_from_utf8()
			for what in options["replace"]:
				content = content.replace(what, options["replace"][what])
			#file.store_buffer(content.to_wchar()) # @todo Convert String back to PoolByteArray by to_wchar() or to_utf8() ?
			file.store_string(content)
		Log.verbose(str("get_len() before close(): ", file.get_len()), logchan)
		file.close()
		file.open(options["file"], File.READ)
		Log.verbose(str("get_len() after READ: ", file.get_len()), logchan)
		file.close()

	downloading.erase(url)
	http_request.queue_free()

	process_downloading()

	#var image = Image.new()
	#image.load_png_from_buffer(body)

	#var texture = ImageTexture.new()
	#texture.create_from_image(image)
	#LevelManager.current_scene.get_node("Billboard").mesh.material.albedo_texture = texture

func check_items(path: String, dict: Dictionary):
	var result = []
	var file_check = File.new()
	for key in dict:
		var value = dict[key]
		# Traverse Dictionaries recursively
		if value is Dictionary:
			result += check_items(path + "/" + key, value)
		# Traverse Array as list of filenames
		elif value is Array:
			for file in value:
				var file_path
				# "." key of Dictionary is current directory, not subdirectory
				if key == ".":
					file_path = path + "/" + str(file)
				else:
					file_path = path + "/" + str(key) + "/" + str(file)
				if (!file_check.file_exists(file_path)):
					result.append(file_path)
		# Use String value as different filename
		elif value is String:
			var file_path = path + "/" + value
			if (!file_check.file_exists(file_path)):
				result.append(file_path)
		else:
			printerr("!! Bad type of user_file (path: ", path, "/", key, "): ", value)
	return result

func download_user_files() -> void:
	#download({"https://antray.duoverso.com/assets/environment/sky/FS003_Day.png.stex": {"file": "user://fs003_day.png.stex"}})

	download({#"https://antray.duoverso.com/assets/music/house-in-a-forest/House In a Forest Loop.ogg": {"file": "user://music/house-in-a-forest/House In a Forest Loop.ogg", "size": 1430743},
		"https://antray.duoverso.com/assets/music/house-in-a-forest/House In a Forest Loop.ogg.oggstr": {"file": "user://music/house-in-a-forest/House In a Forest Loop.ogg.oggstr", "size": 1431039},
		"https://antray.duoverso.com/assets/music/house-in-a-forest/House In a Forest Loop.ogg.import": {"file": "user://music/house-in-a-forest/House In a Forest Loop.ogg.import", "size": 257},
		#"https://antray.duoverso.com/assets/music/house-in-a-forest/House In a Forest Loop.ogg.md5": {"file": "user://music/house-in-a-forest/House In a Forest Loop.ogg.md5", "size": 91},
	})

	download({#"https://antray.duoverso.com/assets/music/for-the-king.ogg": {"file": "user://music/for-the-king.ogg", "size": 3585614},
		"https://antray.duoverso.com/assets/music/for-the-king.ogg.oggstr": {"file": "user://music/for-the-king.ogg.oggstr", "size": 3585911},
		"https://antray.duoverso.com/assets/music/for-the-king.ogg.import": {"file": "user://music/for-the-king.ogg.import", "size": 201},
		#"https://antray.duoverso.com/assets/music/for-the-king.ogg.md5": {"file": "user://music/for-the-king.ogg.md5", "size": 91},
	})

	download({#"https://antray.duoverso.com/assets/music/bamboo-forest.ogg": {"file": "user://music/bamboo-forest", "size": 1921265},
		"https://antray.duoverso.com/assets/music/bamboo-forest.ogg.oggstr": {"file": "user://music/bamboo-forest.ogg.oggstr", "size": 1921563},
		"https://antray.duoverso.com/assets/music/bamboo-forest.ogg.import": {"file": "user://music/bamboo-forest.ogg.import", "size": 203},
		#"https://antray.duoverso.com/assets/music/bamboo-forest.ogg.md5": {"file": "user://music/bamboo-forest.ogg.md5", "size": 91},
	})

	download({#"https://antray.duoverso.com/assets/music/forest-ambience.ogg": {"file": "user://music/forest-ambience.ogg", "size": 566163},
		"https://antray.duoverso.com/assets/music/forest-ambience.ogg.oggstr": {"file": "user://music/forest-ambience.ogg.oggstr", "size": 566459},
		"https://antray.duoverso.com/assets/music/forest-ambience.ogg.import": {"file": "user://music/forest-ambience.ogg.import", "size": 207},
		#"https://antray.duoverso.com/assets/music/forest-ambience.ogg.md5": {"file": "user://music/forest-ambience.ogg.md5", "size": 91},
	})

	download({#"https://antray.duoverso.com/assets/music/forest.ogg": {"file": "user://music/forest.ogg", "size": 1048335},
		"https://antray.duoverso.com/assets/music/forest.ogg.oggstr": {"file": "user://music/forest.ogg.oggstr", "size": 1048631},
		"https://antray.duoverso.com/assets/music/forest.ogg.import": {"file": "user://music/forest.ogg.import", "size": 189},
		#"https://antray.duoverso.com/assets/music/forest.ogg.md5": {"file": "user://music/forest.ogg.md5", "size": 91},
	})

	download({#"https://antray.duoverso.com/assets/music/he-will-never-see-her-again.ogg": {"file": "user://music/he-will-never-see-her-again.ogg", "size": 3480952},
		"https://antray.duoverso.com/assets/music/he-will-never-see-her-again.ogg.oggstr": {"file": "user://music/he-will-never-see-her-again.ogg.oggstr", "size": 3481247},
		"https://antray.duoverso.com/assets/music/he-will-never-see-her-again.ogg.import": {"file": "user://music/he-will-never-see-her-again.ogg.import", "size": 231},
		#"https://antray.duoverso.com/assets/music/he-will-never-see-her-again.ogg.md5": {"file": "user://music/forest.ogg.md5", "size": 91},
	})

	download({#"https://antray.duoverso.com/assets/music/magical-forest.ogg": {"file": "user://music/magical-forest.ogg", "size": 2242148},
		"https://antray.duoverso.com/assets/music/magical-forest.ogg.oggstr": {"file": "user://music/magical-forest.ogg.oggstr", "size": 2242443},
		"https://antray.duoverso.com/assets/music/magical-forest.ogg.import": {"file": "user://music/magical-forest.ogg.import", "size": 205},
		#"https://antray.duoverso.com/assets/music/magical-forest.ogg.md5": {"file": "user://music/magical-forest.ogg.md5", "size": 91},
	})

	download({
		"https://antray.duoverso.com/assets/environment/fs003_day.tres": {"file": "user://environment/fs003_day.tres", "size": 588},
		#"https://antray.duoverso.com/assets/environment/sky/FS003_Day.png": {"file": "user://environment/sky/FS003_Day.png", "size": 2008327},
		"https://antray.duoverso.com/assets/environment/sky/FS003_Day.png.import": {"file": "user://environment/sky/FS003_Day.png.import", "size": 573},
		#"https://antray.duoverso.com/assets/environment/sky/FS003_Day.png.md5": {"file": "user://environment/sky/FS003_Day.png.md5", "size": 91},
		"https://antray.duoverso.com/assets/environment/sky/FS003_Day.png.stex": {"file": "user://environment/sky/FS003_Day.png.stex", "size": 2070512},
	})

	download({
		"https://antray.duoverso.com/assets/environment/fs003_night.tres": {"file": "user://environment/fs003_night.tres", "size": 271},
		#"https://antray.duoverso.com/assets/environment/sky/FS003_Night.png": {"file": "user://environment/sky/FS003_Night.png", "size": 3133186},
		"https://antray.duoverso.com/assets/environment/sky/FS003_Night.png.import": {"file": "user://environment/sky/FS003_Night.png.import", "size": 577},
		#"https://antray.duoverso.com/assets/environment/sky/FS003_Night.png.md5": {"file": "user://environment/sky/FS003_Night.png.md5", "size": 91},
		"https://antray.duoverso.com/assets/environment/sky/FS003_Night.png.stex": {"file": "user://environment/sky/FS003_Night.png.stex", "size": 3218137},
	})

	download({
		"https://antray.duoverso.com/assets/environment/fs003_rainy.tres": {"file": "user://environment/fs003_rainy.tres", "size": 271},
		#"https://antray.duoverso.com/assets/environment/sky/FS003_Rainy.png": {"file": "user://environment/sky/FS003_Rainy.png", "size": 2641823},
		"https://antray.duoverso.com/assets/environment/sky/FS003_Rainy.png.import": {"file": "user://environment/sky/FS003_Rainy.png.import", "size": 577},
		#"https://antray.duoverso.com/assets/environment/sky/FS003_Rainy.png.md5": {"file": "user://environment/sky/FS003_Rainy.png.md5", "size": 91},
		"https://antray.duoverso.com/assets/environment/sky/FS003_Rainy.png.stex": {"file": "user://environment/sky/FS003_Rainy.png.stex", "size": 2773400},
	})

	download({
		"https://antray.duoverso.com/assets/environment/fs003_snowy.tres": {"file": "user://environment/fs003_snowy.tres", "size": 271},
		#"https://antray.duoverso.com/assets/environment/sky/FS003_Snowy.png": {"file": "user://environment/sky/FS003_Snowy.png", "size": 2177240},
		"https://antray.duoverso.com/assets/environment/sky/FS003_Snowy.png.import": {"file": "user://environment/sky/FS003_Snowy.png.import", "size": 577},
		#"https://antray.duoverso.com/assets/environment/sky/FS003_Snowy.png.md5": {"file": "user://environment/sky/FS003_Snowy.png.md5", "size": 91},
		"https://antray.duoverso.com/assets/environment/sky/FS003_Snowy.png.stex": {"file": "user://environment/sky/FS003_Snowy.png.stex", "size": 2360141},
	})

	download({
		"https://antray.duoverso.com/assets/environment/fs003_sunrise.tres": {"file": "user://environment/fs003_sunrise.tres", "size": 273},
		#"https://antray.duoverso.com/assets/environment/sky/FS003_Sunrise.png": {"file": "user://environment/sky/FS003_Sunrise.png", "size": 2665831},
		"https://antray.duoverso.com/assets/environment/sky/FS003_Sunrise.png.import": {"file": "user://environment/sky/FS003_Sunrise.png.import", "size": 581},
		#"https://antray.duoverso.com/assets/environment/sky/FS003_Sunrise.png.md5": {"file": "user://environment/sky/FS003_Sunrise.png.md5", "size": 91},
		"https://antray.duoverso.com/assets/environment/sky/FS003_Sunrise.png.stex": {"file": "user://environment/sky/FS003_Sunrise.png.stex", "size": 2772860},
	})

	download({
		"https://antray.duoverso.com/assets/environment/fs003_sunset.tres": {"file": "user://environment/fs003_sunset.tres", "size": 272},
		#"https://antray.duoverso.com/assets/environment/sky/FS003_Sunset.png": {"file": "user://environment/sky/FS003_Sunset.png", "size": 2761937},
		"https://antray.duoverso.com/assets/environment/sky/FS003_Sunset.png.import": {"file": "user://environment/sky/FS003_Sunset.png.import", "size": 579},
		#"https://antray.duoverso.com/assets/environment/sky/FS003_Sunset.png.md5": {"file": "user://environment/sky/FS003_Sunset.png.md5", "size": 91},
		"https://antray.duoverso.com/assets/environment/sky/FS003_Sunset.png.stex": {"file": "user://environment/sky/FS003_Sunset.png.stex", "size": 2854778},
	})

	download({
		"https://antray.duoverso.com/assets/environment/starmap.tres": {"file": "user://environment/starmap.tres", "size": 204},
		"https://antray.duoverso.com/assets/environment/sky/starmap.tres": {"file": "user://environment/sky/starmap.tres", "size": 178},
		#"https://antray.duoverso.com/assets/environment/sky/starmap_4k.jpg": {"file": "user://environment/sky/starmap_4k.jpg", "size": 2349902},
		"https://antray.duoverso.com/assets/environment/sky/starmap_4k.jpg.import": {"file": "user://environment/sky/starmap_4k.jpg.import", "size": 575},
		#"https://antray.duoverso.com/assets/environment/sky/starmap_4k.jpg.md5": {"file": "user://environment/sky/starmap_4k.jpg.md5", "size": 91},
		"https://antray.duoverso.com/assets/environment/sky/starmap_4k.jpg.stex": {"file": "user://environment/sky/starmap_4k.jpg.stex", "size": 9663472},
	})

	download({
		"https://antray.duoverso.com/assets/environment/tutsplus_001.tres": {"file": "user://environment/tutsplus_001.tres", "size": 272},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_001.hdr": {"file": "user://environment/sky/tutsplus_001.hdr", "size": 11258461},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_001.hdr.import": {"file": "user://environment/sky/tutsplus_001.hdr.import", "size": 579},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_001.hdr.md5": {"file": "user://environment/sky/tutsplus_001.hdr.md5", "size": 91},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_001.hdr.stex": {"file": "user://environment/sky/tutsplus_001.hdr.stex", "size": 21595612},
	})

	download({
		"https://antray.duoverso.com/assets/environment/tutsplus_002.tres": {"file": "user://environment/tutsplus_002.tres", "size": 273},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_002.hdr": {"file": "user://environment/sky/tutsplus_002.hdr", "size": 8914620},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_002.hdr.import": {"file": "user://environment/sky/tutsplus_002.hdr.import", "size": 579},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_002.hdr.md5": {"file": "user://environment/sky/tutsplus_002.hdr.md5", "size": 91},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_002.hdr.stex": {"file": "user://environment/sky/tutsplus_002.hdr.stex", "size": 21595612},
	})

	download({
		"https://antray.duoverso.com/assets/environment/tutsplus_003.tres": {"file": "user://environment/tutsplus_003.tres", "size": 273},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_003.hdr": {"file": "user://environment/sky/tutsplus_003.hdr", "size": 10577982},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_003.hdr.import": {"file": "user://environment/sky/tutsplus_003.hdr.import", "size": 579},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_003.hdr.md5": {"file": "user://environment/sky/tutsplus_003.hdr.md5", "size": 91},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_003.hdr.stex": {"file": "user://environment/sky/tutsplus_003.hdr.stex", "size": 21595612},
	})

	download({
		"https://antray.duoverso.com/assets/environment/tutsplus_004.tres": {"file": "user://environment/tutsplus_004.tres", "size": 273},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_004.hdr": {"file": "user://environment/sky/tutsplus_004.hdr", "size": 12983563},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_004.hdr.import": {"file": "user://environment/sky/tutsplus_004.hdr.import", "size": 579},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_004.hdr.md5": {"file": "user://environment/sky/tutsplus_004.hdr.md5", "size": 91},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_004.hdr.stex": {"file": "user://environment/sky/tutsplus_004.hdr.stex", "size": 21595612},
	})

	download({
		"https://antray.duoverso.com/assets/environment/tutsplus_005.tres": {"file": "user://environment/tutsplus_005.tres", "size": 273},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_005.hdr": {"file": "user://environment/sky/tutsplus_005.hdr", "size": 8798699},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_005.hdr.import": {"file": "user://environment/sky/tutsplus_005.hdr.import", "size": 579},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_005.hdr.md5": {"file": "user://environment/sky/tutsplus_005.hdr.md5", "size": 91},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_005.hdr.stex": {"file": "user://environment/sky/tutsplus_005.hdr.stex", "size": 21595612},
	})

	download({
		"https://antray.duoverso.com/assets/environment/tutsplus_006.tres": {"file": "user://environment/tutsplus_006.tres", "size": 273},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_006.hdr": {"file": "user://environment/sky/tutsplus_006.hdr", "size": 13150456},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_006.hdr.import": {"file": "user://environment/sky/tutsplus_006.hdr.import", "size": 579},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_006.hdr.md5": {"file": "user://environment/sky/tutsplus_006.hdr.md5", "size": 91},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_006.hdr.stex": {"file": "user://environment/sky/tutsplus_006.hdr.stex", "size": 21595612},
	})

	download({
		"https://antray.duoverso.com/assets/environment/tutsplus_007.tres": {"file": "user://environment/tutsplus_007.tres", "size": 273},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_007.hdr": {"file": "user://environment/sky/tutsplus_007.hdr", "size": 7299485},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_007.hdr.import": {"file": "user://environment/sky/tutsplus_007.hdr.import", "size": 579},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_007.hdr.md5": {"file": "user://environment/sky/tutsplus_007.hdr.md5", "size": 91},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_007.hdr.stex": {"file": "user://environment/sky/tutsplus_007.hdr.stex", "size": 21595612},
	})

	download({
		"https://antray.duoverso.com/assets/environment/tutsplus_008.tres": {"file": "user://environment/tutsplus_008.tres", "size": 336},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_008.hdr": {"file": "user://environment/sky/tutsplus_008.hdr", "size": 12291246},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_008.hdr.import": {"file": "user://environment/sky/tutsplus_008.hdr.import", "size": 578},
		#"https://antray.duoverso.com/assets/environment/sky/tutsplus_008.hdr.md5": {"file": "user://environment/sky/tutsplus_008.hdr.md5", "size": 91},
		"https://antray.duoverso.com/assets/environment/sky/tutsplus_008.hdr.stex": {"file": "user://environment/sky/tutsplus_008.hdr.stex", "size": 21595612},
	})

	#var items = check_items("user:/", user_files) # second slash will be appeded to filenames as root folder
	#Log.debug(str(items), logchan)
	#return
	#for item in items:
	#	Log.debug(str(item), logchan)

	#for item in items:
		#var url = assets_url + file.replace("user:/", "")
		#download(url, file)
	is_downloaded = true
	#user_files = {} # Free memory
