extends Node
#class_name AntRepo

var logchan := "repo"

var repos: Dictionary
var repos_dir_user := "user://repos"

### AntRay Asset Repository JSON format v2

# User files multiple-level Dictionary to map assets to local filesystem
# Array value is for list of files
# "." key of Dictionary is for list of files too but allows to map subdirectories

## Example:
# var repo = {
#   "name": "antray",
#   "base_url": "https://antray.duoverso.com/assets",
#   "files": {
#     "file_to_download": true,
#     "ignored_file": false,
#     ".": ["file1", "file2"],
#     "directory": {
#       ".": ["fileA", "fileB"],
#       "subdirectory": ["fileX", "fileY"]
#     }
#   }
# }

func _enter_tree():
	Log.verbose("_enter_tree() at: " + get_path(), logchan)

func load_all() -> void:
	load_repos()
	for key in repos:
		var repo = repos[key]
		var items = get_missing_items(repo["base_url"], repos_dir_user + repo["path"], repo["items"])

		# Replacements in files content
		var replace := {}
		if repo.has("orig_root"):
			replace[ repo["orig_root"] ] = repos_dir_user + repo["path"]

		for item in items:
			var apply_replacements := false
			if !replace.empty():
				var ext = item["file"].get_extension()
				#Log.debug("Extension: " + ext, logchan)
				if ext in ["import", "tres", "tscn"]:
					#Log.debug("Repo replacements: " + replace, logchan)
					apply_replacements = true

			Downloader.download({"url": item["url"], "file": item["file"], "replace": true if apply_replacements else false})

func load_repos() -> void:
	repos = {}
	var dir = Directory.new()
	if !dir.dir_exists(repos_dir_user):
		dir.make_dir(repos_dir_user)
	if dir.open(repos_dir_user) != OK:
		Log.err("!! An error occurred when trying to look up for repositories at " + repos_dir_user, logchan)
	else:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			# Only .import files are read on Android (see https://github.com/godotengine/godot/issues/39474)
			if not dir.current_is_dir() and file_name.match("*.json"):
				var file = File.new()
				file.open(repos_dir_user + "/" + file_name, File.READ)
				var repo = JSON.parse(file.get_as_text()).result
				file.close()
				# Do not overwrite already loaded repo with same name
				# @TODO it's still not secure because of loading order
				if !repo.has("name") or repos.has(repo["name"]):
					continue
				# Repo path has to start and end with / and automaticaly it's repo name
				if !repo.has("path") or repo.path[0] != "/":
					repo["path"] = "/" + repo["name"] + "/"
				# Save repo to repos
				repos[repo["name"]] = repo
			file_name = dir.get_next()

	if repos.size() == 0:
		Log.err("!! No repositories found", logchan)

	Log.info("Repos: " + str(repos.keys()), logchan)
	for key in repos:
		Log.info(key + " = " + repos[key]['base_url'], logchan)

func get_missing_items(url: String, local_dir: String, items: Dictionary) -> Array:
	var result = []
	var file_check = File.new()
	for key in items:
		var value = items[key]
		var item_url = url + ("/" + key if key != "." else "")
		# Traverse Dictionaries recursively
		if value is Dictionary:
			result += get_missing_items(item_url, local_dir + "/" + key, value)
		# Traverse Array as list of filenames
		elif value is Array:
			for file in value:
				# "." key of Dictionary is current directory, not subdirectory
				var local_file = local_dir + "/" + (str(key) + "/" if key != "." else "") + str(file)
				if (!file_check.file_exists(local_file)):
					result.append({"url": item_url + "/" + str(file), "file": local_file})
		# Use String value as different filename
		elif value is String:
			var local_file = local_dir + "/" + value
			if (!file_check.file_exists(local_dir)):
				result.append({"url": item_url, "file": local_file})
		else:
			Log.err("!! Bad type of item (url: " + item_url + ", path: " + local_dir + "/" + key + "): " + value, logchan)
	return result
