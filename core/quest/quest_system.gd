### TODO
# - different dialogue windows can be used together
# - output & input to 3D text/controls
# - choose options
# - input numbers & text for questions
# - multiquest (with sidequests, or main quest could be separated to multiple parts)
# - different (animated) avatars per character could be used on dialogues (with facial animations), or explicitly no avatar (for hidden npc)
# - complex branching:
#   - quest line could have stages
#   - character could use quest line stages and/or own stages together
# - GDScript, VisualScript, NativeScript... can be used together with dialogues (dialogue signal)
# - parse/save all dialogues (or single quest) to plain text file (something like Clyde, but with actions)
# - completion can be based on game state (so quest can be completed before it starts) or on ad-hoc actions (have to be done by runtime data)
# - rewards
#
# Optimize:
# - hold only data of current stage of active quests
#
# Extended:
# - waypoint system for NPC run towards location of quest, wait for player, give another quest...
# - localization
# - SALSA (https://crazyminnowstudio.com/unity-3d/lip-sync-salsa/)
# - voices
#
#
#tool
extends Node
#class_name QuestSystem

var _loaded := false

# Dictionary to store/cache/save all quests data
export var all := {
#	"lba": {
#		"name": "Little Big Adventure",
#	},
	"quest1": {
		"name": "Quest 1",
#		"stage": null,
		"stages": {
			"start": {
				"Jixie": {
					"text": "Do you want Quest 1?",
					"choices": {
						"Yes": "1",
						"No": null,
					}
				}
			},
			"1": {
				"Jixie": {
					"text": "Cool! Then find my cat please.",
					"next": "2"
				}
			},
			"2": {
				"Jixie": {
					"text": "Please find my cat.",
				}
			},
			"end": {
			},
		}
	},
	"quest2": {
		"name": "Quest 2",
		"stages": {
			"start": {
				"Jixie": {
					"text": "Do you want Quest 2?",
					"choices": {
						"Yes": "1",
						"No": null
					}
				}
			},
			"1": {
			},
			"2": {
			},
			"end": {
			},
		}
	},
	"quest3": {
		"name": "Quest 3",
		"requirements": {
			"quests": ["quest1", "quest2"],
#			"level": 5,
		},
		"stages": {
			"start": {
				"Jixie": {
					"text": "Do you want Quest 3?",
					"choices": {
						"Yes": "1",
						"No": null
					}
				}
			},
			"1": {
			},
			"2": {
			},
			"end": {
			},
		}
	}
}
# Arrays for basic quest types by state
var pending := []   # not available, requirements not met
var available := [] # available but not started
var active := []    # started & in progress
var completed := [] # finished but reward is still waiting
var done := []      # reward delivered and done
var canceled := []  # event quests? too simple quests for advanced players (based on xp/level)?
var last_progress : int

func _enter_tree():
	if _loaded:
		printerr("Error: Quests is an AutoLoad singleton and it shouldn't be instanced elsewhere.")
		printerr("Delete the instance at: " + get_path())
	else:
		_loaded = true
