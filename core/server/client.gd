# https://docs.godotengine.org/en/stable/tutorials/networking/high_level_multiplayer.html
extends SceneTree

#const SERVER_ADDR = "127.0.0.1"
const SERVER_ADDR = "::1"
const SERVER_PORT = 30123

func _init():
	print("(client_test) _init()")
	# Server & Clients
	connect("network_peer_connected", self, "_peer_connected")
	connect("network_peer_disconnected", self, "_peer_disconnected")
	# Clients
	connect("connected_to_server", self, "_connected_to_server")
	connect("connection_failed", self, "_connection_failed")
	connect("server_disconnected", self, "_server_disconnected")

	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(SERVER_ADDR, SERVER_PORT)
	print(peer)
	network_peer = peer
	print("(client-test) Client connection...")

	#var lobby = preload("./lobby.gd")
	#print(lobby)

	#quit()

# Server & Clients
func _peer_connected(id):
	print("(client-test) _peer_connected(", id, ")")


func _peer_disconnected(id):
	print("(client-test) _peer_disconnected(", id, ")")

# Clients
func _connected_to_server():
	print("(client-test) _connected_to_server()")

func _connection_failed():
	print("(client-test) _connection_failed()")

func _server_disconnected():
	print("(client-test) _server_disconnected()")

