#class_name AntSettings
extends Node
#extends Resource

var logchan := "settings"
var _loaded := false

var data := ConfigFile.new()
var default_config_file := "res://settings.antray"
var onready_load_user_config := true
var user_config_file := "user://settings.antray"

signal settings_loaded
signal save_settings
var observers: Array
var saving_timeout := 2.0

func _init():
	Log.verbose("_init()", logchan)
	if load_from(default_config_file, false) == OK:
		if has_key("settings", "user_config_file"):
			user_config_file = get_value("settings", "user_config_file")
		if has_key("settings", "onready_load_user_config"):
			onready_load_user_config = get_value("settings", "onready_load_user_config")
	if onready_load_user_config:
		if load_from(user_config_file, true) == OK:
			Log.info("User's config file loaded.", logchan)
		else:
			Log.notice("Missing user's config file.", logchan)

func _enter_tree():
	Log.verbose("_enter_tree() at: " + get_path(), logchan)
	if _loaded:
		Log.err("Error: Settings is an AutoLoad singleton and it shouldn't be instanced elsewhere.", logchan)
		Log.err("Please delete the instance at: " + get_path(), logchan)
	else:
		_loaded = true

func set_value(section: String, key: String, value) -> void:
	Log.verbose("set_value(" + section + ", " + key + ", " + str(value) + ")", logchan)
	data.set_value(section, key, value)

#func get_value(section: String, key: String, default = null):
func get_value(section: String, key: String):
	#Log.verbose("get_value(" + section + ", " + key + ", " + str(default) + ")", logchan)
	Log.verbose("get_value(" + section + ", " + key + ")", logchan)
	#return config.get_value(section, key, default)
	# Missing key will raise error: Invalid get index '<key>' (on base: 'Node (settings.gd)').
	# So default values has to be set in default_config_file
	return data.get_value(section, key)

func has_key(section: String, key: String) -> bool:
	return data.has_section_key(section, key)

func save_user_config():
	save_to(user_config_file)

func load_user_config():
	load_from(user_config_file)

func reset():
	data.clear()
	load_from(default_config_file)
	#remove_user_config()

# Every (observer) connected to save_settings signal have to call Settings.saved(self) when finish saving
func saved(observer) -> void:
	var idx := observers.find(observer)
	if idx < 0:
		Log.err("saved(" + str(observer) + ") already removed", logchan)
		return
	observers.remove(idx)

func save_to(config_file: String, emit := true) -> int:
	Log.debug("save_to(" + config_file + ", " + str(emit) + ")", logchan)
	if emit:
		observers = []
		for observer in get_signal_connection_list("save_settings"):
			observers.push_back(observer.target)
		emit_signal("save_settings")
		# Waiting on idle frames for all observers to finish saving or timeout
		var timer := get_tree().create_timer(saving_timeout)
		while true:
			yield(get_tree(), "idle_frame")
			if observers.size() <= 0 or timer.time_left <= 0:
				break

	var result: int = data.save(config_file)
	if result != OK:
		Log.err("(Error " + str(result) + ") Can't save data to: " + config_file, logchan)
		return result
	UI.hud.set_event_text("Saved", 1.0)
	return OK

func load_from(config_file: String, emit := true) -> int:
	Log.info("load_from(" + config_file + ", " + str(emit) + ")", logchan)
	var result: int = data.load(config_file)
	if result != OK:
		Log.err("(Error " + str(result) + ") Can't load data from: " + config_file, logchan)
		return result

	Log.info("Data loaded from: " + config_file, logchan)
	if emit:
		emit_signal("settings_loaded")
	return OK

func save_from(object: Object, section: String, properties: Array, send_saved := true) -> void:
	Log.verbose("save_from(" + str(object) + ", \"" + section + "\"" + str(properties) + ", " + str(send_saved) + ")", logchan)
	# Simplest implementation to save direct properties only
	#for property in properties:
	#	set_value(section, property, object[property])
	# Recursive traversing through objects, properties and components
	for property in properties:
		var components: PoolStringArray = property.split(".")
		var value = object
		for component in components:
			value = value[component]
		set_value(section, property, value)
	if send_saved:
		saved(object)

func load_to(object: Object, section: String, properties: Array) -> void:
	Log.verbose("load_to(" + str(object) + ", \"" + section + "\", " + str(properties) + ")", logchan)
	for property in properties:
		if not has_key(section, property):
			continue
		# Simplest implementation to load direct properties only
		#object[property] = get_value(section, property)
		# Recursive traversing through objects, properties and components
		var ref = object
		var components: PoolStringArray = property.split(".")
		var last_component: String = components[components.size()-1]
		components.remove(components.size()-1)
		for component in components:
			ref = ref[component]
		ref[last_component] = get_value(section, property)

func remove(config_file: String) -> int:
	var dir = Directory.new()
	var result = dir.remove(config_file)
	if result != OK:
		Log.err("(Error " + str(result) + ") Config couldn't be deleted", logchan)
		return result
	Log.info("Config deleted", logchan)
	return OK

func remove_user_config() -> void:
	remove(user_config_file)

func _exit_tree():
	Log.verbose("_exit_tree()", logchan)
