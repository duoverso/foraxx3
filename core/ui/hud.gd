extends CanvasLayer

var logchan := "hud"

# Debug overlay by Gonkee - full tutorial https://youtu.be/8Us2cteHbbo
var help := false

onready var crosshair: Control = $"%Crosshair"

# MenuButtons with PopupMenus
#const MENU_TERRAIN_GENERATOR = 1 # 0 # doesn't work

onready var top_menu: TopMenu = $"%TopMenu"

onready var debug_label: Control = $"%Debug"
onready var log_container: Control = $"%Log"
var last_log_msg := 0

onready var event_label: Control = $"%EventLabel"

onready var healthbar: Control = $"%Healthbar"
onready var blood: Control = $"%Blood"
onready var dead: Control = $"%Dead"
onready var weaponbar: Control = $"%Weaponbar"
onready var moneybar: Control = $"%Moneybar"
onready var left_joystick: Node = $"%LeftJoystick"
onready var right_joystick: Node = $"%RightJoystick"
onready var left_touch_joystick: Node = $"%LeftJoystick/Joystick"
onready var right_touch_joystick: Node = $"%RightJoystick/Joystick"
onready var left_buttons: Node = $"%LeftButtons"
onready var right_buttons: Node = $"%RightButtons"

onready var about_dialog: Control = $"%About"
onready var file_dialog: Control = $"%FileDialog"

onready var dialogue_container: Control = $"%Dialogue"
onready var dialogue_big_node: Control = $"%BigDialogue"
onready var dialogue_small_node: Control = $"%SmallDialogue"

var update_timer := 0.0

var stats := []
#onready var env = $"/root/Level/WorldEnvironment".environment

export var details := 1
onready var materials := [
	# Terrain
	#$"../Platform".material,
	#$"../Terrain/Canyon".mesh.surface_get_material(0),
	#$"../Terrain/Canyons".mesh.surface_get_material(0),
	#$"../Terrain/Desert".mesh.surface_get_material(0),
	#$"../Terrain/Riverland".mesh.surface_get_material(0),
	#$"../Terrain/Flatstones".mesh.surface_get_material(0),
	#$"../Terrain/Lakes1/Lakes1".mesh.surface_get_material(0),
	#$"../Terrain/DragonCastle/DragonCastle".mesh.surface_get_material(0),
	#$"../Terrain/DragonCastle/Dungeon".mesh.surface_get_material(0),
	#$"../Terrain/Hills".mesh.surface_get_material(0),
	# Animals
	#$"../Animals/DragonTutu/Armature/Skeleton/Tutu".mesh.surface_get_material(0),
	]

func _enter_tree():
	Log.verbose("_enter_tree() at: " + get_path(), logchan)

func _ready() -> void:
	dialogue_big_close()
	dialogue_close()

	Settings.connect("settings_loaded", self, "set_event_text", ["Loaded", 1.0])
	Log.verbose("Settings.connect(settings_loaded) to load_settings()", logchan)

	# env.fog_enabled = true
	call_deferred("when_ready")

func when_ready() -> void:
	set_event_text("Welcome in Antray (" + OS.get_name() + ") at " + Time.get_time_string_from_system() + "\n\nUse WASD keys to move around.", 5.0)

func _on_Play_pressed() -> void:
	crosshair.show()
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	Global.player.mouse_captured = true
	hide_top_menu()

func _on_Button_Quests_pressed() -> void:
	toggle_quests()

func _on_Button_Inventory_pressed() -> void:
	toggle_inventory()

func hide_top_menu() -> void:
	top_menu.hide()

func show_top_menu() -> void:
	top_menu.update_bars()
	top_menu.show()

func toggle_top_menu() -> bool:
	#if $VBoxContainer/Top.is_visible_in_tree():
	if top_menu.visible:
		hide_top_menu()
		return true
	else:
		show_top_menu()
		return false

func show_joysticks() -> void:
	#$VBoxContainer/Bottom.show()
	left_joystick.show()
	left_buttons.show()
	right_buttons.show()
	right_joystick.show()

func hide_joysticks() -> void:
	#$VBoxContainer/Bottom.hide()
	left_joystick.hide()
	left_buttons.hide()
	right_buttons.hide()
	right_joystick.hide()

func toggle_joysticks() -> void:
	#if $VBoxContainer/Bottom.visible:
	if left_joystick.visible:
		hide_joysticks()
	else:
		show_joysticks()

func enable_touch_control() -> void:
	show_top_menu()
	show_joysticks()

func disable_touch_control() -> void:
	hide_top_menu()
	hide_joysticks()

func add_stat(stat_name, object, stat_ref, is_method):
	stats.append([stat_name, object, stat_ref, is_method])

func _process(delta) -> void:
	update_timer -= delta
	if update_timer > 0.0:
		return
	update_timer = 0.2

	if healthbar.visible:
		update_healthbar()
	if weaponbar.visible:
		update_weaponbar()
	if moneybar.visible:
		update_moneybar()
	if debug_label.visible:
		update_debug()
	if log_container.visible:
		update_log()

func _unhandled_input(event: InputEvent) -> void:
	#if Log.enabled:
	#Log.verbose("_unhandled_input( " + str(event) + " )", logchan)

	if event is InputEventKey and event.is_pressed():

		if event.is_action("ui_dialog_toggle"):
			UI.hud.hide_event_text()
			UI.hud.dialogue_close()
			UI.hud.dialogue_big_close()

		elif event.is_action("ui_help"):
			toggle_help()

		elif event.is_action("ui_toggle_fog"):
			LevelEnvironment.env.environment.fog_enabled = !LevelEnvironment.env.environment.fog_enabled

		elif event.is_action("ui_details"):
			# Switch level of details
			details += 1
			if details > 2:
				details = 0

			# Apply settings for current level of details
			if details == 0:
				for material in materials:
					# DiffuseMode: DIFFUSE_BURLEY=0, DIFFUSE_LAMBERT=1, DIFFUSE_LAMBERT_WRAP=2, DIFFUSE_OREN_NAYAR=3, DIFFUSE_TOON=4
					#material.set_diffuse_mode(SpatialMaterial.DIFFUSE_BURLEY)

					# Flags: FLAG_UNSHADED=0, FLAG_USE_VERTEX_LIGHTING=1, FLAG_DISABLE_DEPTH_TEST=2, FLAG_ALBEDO_FROM_VERTEX_COLOR=3,
					# FLAG_SRGB_VERTEX_COLOR=4, FLAG_USE_POINT_SIZE=5, FLAG_FIXED_SIZE=6, FLAG_BILLBOARD_KEEP_SCALE=7,
					# FLAG_UV1_USE_TRIPLANAR=8, FLAG_UV2_USE_TRIPLANAR=9, FLAG_TRIPLANAR_USE_WORLD=10, FLAG_AO_ON_UV2=11,
					# FLAG_EMISSION_ON_UV2=12, FLAG_USE_ALPHA_SCISSOR=13, FLAG_ALBEDO_TEXTURE_FORCE_SRGB=14,
					# FLAG_DONT_RECEIVE_SHADOWS=15, FLAG_ENSURE_CORRECT_NORMALS=16, FLAG_DISABLE_AMBIENT_LIGHT=17,
					# FLAG_USE_SHADOW_TO_OPACITY=18
					material.set_flag(SpatialMaterial.FLAG_UNSHADED, 1)
					material.set_flag(SpatialMaterial.FLAG_USE_VERTEX_LIGHTING, 1)

					# SpecularMode: SPECULAR_SCHLICK_GGX=0, SPECULAR_BLINN=1, SPECULAR_PHONG=2, SPECULAR_TOON=3, SPECULAR_DISABLED=4
					material.set_specular_mode(SpatialMaterial.SPECULAR_DISABLED)

					#  BlendMode: BLEND_MODE_MIX=0, BLEND_MODE_ADD=1, BLEND_MODE_SUB=2, BLEND_MODE_MUL=3
					#material.set_blend_mode(SpatialMaterial.BLEND_MODE_MIX)

					# CullMode: CULL_BACK=0, CULL_FRONT=1, CULL_DISABLED=2
					#material.set_cull_mode(SpatialMaterial.CULL_DISABLED)

					# TEXTURE_ALBEDO=0, TEXTURE_METALLIC=1, TEXTURE_ROUGHNESS=2, TEXTURE_EMISSION=3, TEXTURE_NORMAL=4
					# TEXTURE_RIM=5, TEXTURE_CLEARCOAT=6, TEXTURE_FLOWMAP=7, TEXTURE_AMBIENT_OCCLUSION=8, TEXTURE_DEPTH=9
					# TEXTURE_SUBSURFACE_SCATTERING=10, TEXTURE_TRANSMISSION=11, TEXTURE_REFRACTION=12, TEXTURE_DETAIL_MASK=13
					# TEXTURE_DETAIL_ALBEDO=14, TEXTURE_DETAIL_NORMAL = 15

			elif details == 1:
				for material in materials:
					material.set_flag(SpatialMaterial.FLAG_UNSHADED, 0)

			elif details == 2:
				for material in materials:
					material.set_specular_mode(SpatialMaterial.SPECULAR_SCHLICK_GGX)
					material.set_flag(SpatialMaterial.FLAG_USE_VERTEX_LIGHTING, 0)

		elif event.is_action("ui_toggle_fullscreen"):
			OS.window_fullscreen = !OS.window_fullscreen

		elif event.is_action("ui_toggle_joysticks"):
			toggle_joysticks()

		elif event.is_action("toggle_log"):
			Log.enabled = not Log.enabled

		elif event.is_action("clear_log"):
			Log.clear_all()
			log_container.text = ""
			last_log_msg = 0

		elif event.is_action("ui_debug"):
			set_debug_visibility(not debug_label.visible)

		else:
			return

		Log.debug("handled " + str(event), logchan)
		get_tree().set_input_as_handled()

func set_event_text(text: String, autohide: float = 0.0) -> void:
	Log.debug("set_event_text(" + text + ", " + str(autohide) + ")", logchan)
	event_label.text = text
	event_label.visible = true
	if autohide:
		yield(get_tree().create_timer(autohide), "timeout")
		hide_event_text()

func hide_event_text() -> void:
	Log.debug("hide_event_text()", logchan)
	event_label.visible = false
	event_label.text = ""

func dialogue(text: String, autohide: float = 0.0, close_big := true) -> void:
	Log.debug(str("dialogue(", text, ", ", autohide, ", ", close_big, ")"), logchan)
	if close_big:
		dialogue_big_close()
	dialogue_small_node.get_node("Text").bbcode_text = text
	dialogue_small_node.visible = true
	dialogue_container.visible = true
	if autohide:
		yield(get_tree().create_timer(autohide), "timeout")
		dialogue_close()

func dialogue_close() -> void:
	dialogue_small_node.visible = false
#	if (dialogue_big.visible == false):
	dialogue_container.visible = false
	help = false

func dialogue_big(text: String, autohide: float = 0.0, close_small := true) -> void:
	Log.debug(str("dialogue_big(", text, ", ", autohide, ", ", close_small, ")"), logchan)
	if close_small:
		dialogue_close()
	dialogue_big_node.get_node("Text").bbcode_text = text
	dialogue_big_node.visible = true
	dialogue_container.visible = true
	if autohide:
		yield(get_tree().create_timer(autohide), "timeout")
		dialogue_big_close()

func dialogue_big_close() -> void:
	dialogue_big_node.visible = false
#	if (dialogue_small.visible == false):
	dialogue_container.visible = false

func toggle_help() -> void:
	help = not help
	if help:
		show_help()
	else:
		dialogue_big_close()

func show_help() -> void:
	# Hide Event text
	hide_event_text()

	var info_text = """[code]Esc | U : Toggle mouse mode
Left click | Ctrl+1 : Use tool         Right click | Ctrl+2 : Add ammunition
Ctrl+Scroll with mouse : Change tool
1,2,3,4,5,6 : Speed presets
+ and - : Speed up and Slow down (min 1)
WASD : Move                   Left, Right, Up, Down : Rotate
Space | Q : Jump              C | E : Crouch
Shift : Run (double speed)    F : Flashlight
Tab : Fly with keyboard (stop/start falling) + Space (fly up) + C|E (fly down)
Shift+Tab : Fly with mouse (stop/start falling)
Scroll with mouse : Change Camera FOV
/ : Reset Camera FOV         [ and ] : Camera Far and Fog closer and further
Home : Back to Start Point   B / N / Alt+Home : Previous / Next / Lobby Level
F2 / F3 : Save / Load
F5 / F6 : Switch 1st & 3rd person view / Camera projection Perspective and Ortho
F7 : Random environment
F9 / F10 / F11 / F12 : Switch Details / Fog / Fullscreen / Debug
X : Hide all dialogs
. (period) / , (comma) / '/' (slash) : Music Play/Pause / Next track / Shuffle
[/code]"""
#
# [img]res://assets/ui/keyboard.png[/img]
#
# [img]res://assets/ui/gamepad.png[/img]
#
	dialogue_big(info_text)

func set_healthbar_visibility(new_val: bool) -> void:
	healthbar.visible = new_val
	if not healthbar.visible:
		healthbar.text = ""
	top_menu.update_healthbar_visibility()

func update_healthbar() -> void:
	healthbar.text = "Health: %d / %d" % [Global.player.hp, Global.player.hp_max]

func set_weaponbar_visibility(new_val: bool) -> void:
	weaponbar.visible = new_val
	if not weaponbar.visible:
		weaponbar.text = ""
	top_menu.update_weaponbar_visibility()

func update_weaponbar() -> void:
	var weapon_info = Global.player.weapons[Global.player.weapon]
	var weapon_ammo = Global.player.ammo[Global.player.weapon]
	weaponbar.text = "%s: %s" % [weapon_info["name"], weapon_ammo if weapon_ammo >= 0 else "(infinite)"]

func set_moneybar_visibility(new_val: bool) -> void:
	moneybar.visible = new_val
	if not moneybar.visible:
		moneybar.text = ""
	top_menu.update_moneybar_visibility()

func update_moneybar() -> void:
	moneybar.text = "$ %d" % [Global.player.money]

func set_debug_visibility(new_val: bool) -> void:
	debug_label.visible = new_val
	if not debug_label.visible:
		debug_label.text = ""
	UI.hud.top_menu.update_debug_visibility()

func update_debug() -> void:
	var debug_text := ""

	var player = Global.player
	var cam = player.cam
	#var cam = player.get_node("Player").cam
	debug_text += str("FPS: ", Engine.get_frames_per_second()) + "\n"
	debug_text += str("Memory: ", String.humanize_size(OS.get_static_memory_usage())) + "\n"
	debug_text += str("Level of Details: ", details) + "\n"
	if cam.projection == Camera.PROJECTION_PERSPECTIVE:
		debug_text += str("Camera FOV: ", cam.fov, ",  Far: ", cam.far) + "\n"
	else:
		debug_text += str("Camera Size: ", cam.size, ",  Far: ", cam.far) + "\n"

	debug_text += "Status: "
	if player.jumping:
		debug_text += "[jumping]"
	if player.crouching:
		debug_text += "[crouching]"
	if player.fly_mode:
		if (player.fly_mode == 1):
			debug_text += "[fly with keyboard]"
		elif (player.fly_mode == 2):
			debug_text += "[fly with mouse]"
	else:
		debug_text += "[falling]"
	debug_text += "\n"

	debug_text += str("Speed: ", player.speed) + "\n"
	if LevelManager.current_scene.has_node("Generator"):
		var generator = LevelManager.current_scene.get_node("Generator")
		debug_text += "Cell: [X] " + str(generator.last_p_x) + " [Z] " + str(generator.last_p_z) + "\n"
	debug_text += "Player Velocity: " + str_vec3(player.velocity) + "\n"
	debug_text += "Player Translation: " + str_vec3(player.translation) + "\n"
	debug_text += "Player Rotation: " + str_vec3(player.rotation_degrees) + "\n"
	debug_text += "Camera Rotation: " + str_vec3(player.cam.rotation_degrees) + "\n"

	if Sensors.enabled:
		debug_text += "Accelerometer: " + str(Sensors.acc) + "\n"
		debug_text += "Gravity: " + str(Sensors.grav) + "\n"
		debug_text += "Magnetometer: " + str(Sensors.mag) + "\n"
		debug_text += "Gyroscope: " + str(Sensors.gyro) + "\n"
		debug_text += "North: " + str(Sensors.north) + "\n"

	for s in stats:
		var value = null

		if s[1] and weakref(s[1]).get_ref():
			if s[3]:
				value = s[1].call(s[2])
			else:
				value = s[1].get(s[2])
		debug_text += str(s[0], ": ", value)
		debug_text += "\n"

	debug_label.text = debug_text

func set_log_visibility(new_val: bool) -> void:
	log_container.visible = new_val
	#if not log_container.visible:
	#	log_container.text = ""
	UI.hud.top_menu.update_log_visibility()

func update_log() -> void:
	if Log.msgs.size() <= last_log_msg:
		return

	var text := ""
	for num in range(last_log_msg, Log.msgs.size(), 1): # Array of {"t": int, "m": String, "c": Array} - time, msg & chan
		var msg = Log.msgs[num]
		# @TODO Add messages from selected channels only
		#for chan in Log.channels.keys():
		text += str(msg["c"]) + " " + msg["m"] + "\n"
	log_container.text += text
	last_log_msg = Log.msgs.size()

func show_about() -> void:
	about_dialog.popup()

func toggle_inventory() -> void:
	pass

func toggle_quests() -> void:
	pass

func str_vec3(vec3: Vector3) -> String:
	return "[X] %.2f [Y] %.2f [Z] %.2f" % [vec3.x, vec3.y, vec3.z]
	#return "[X] " + str(stepify(vec3.x, 0.01)) + " [Y] " + str(stepify(vec3.y, 0.01)) + " [Z] " + str(stepify(vec3.z, 0.01))
