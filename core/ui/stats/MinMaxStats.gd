#class_name MinMaxMob
extends Stats

export var init_min_health: float = 100.0
export var init_max_health: float = 1000.0
export var init_min_damage: float = 10.0
export var init_max_damage: float = 20.0
export var init_min_reward: int = 100
export var init_max_reward: int = 2000

func init_data() -> void:
	set_max_health(rand_range(init_min_health, init_max_health))
	damage = rand_range(init_min_damage, init_max_damage)
	reward = int(rand_range(init_min_reward, init_max_reward))
	Log.debug(str(parent) + ".stats.init_data(): health=" + str(health) + " / " + str(max_health) + ", damage=" + str(damage) + ", reward=" + str(reward), parent.logchan)
	.init_data()
