# Expandable Container to hold Character Stats and UI/HUD presentation
class_name Stats extends Spatial

# It doesn't hold body (meshes, materials, physics, audio...). It's stored in parent.
# This class is API for stats stored in var stats at parent.
onready var parent := get_parent()

var health: float setget set_health
var max_health: float setget set_max_health
var damage: float
var reward: int

# When target is set and not wandering_at then target is still visible and enemy is following it
# When target is set and wandering_at too then enemy doesn't see target but heading to last known position of target
# When target is not set then wandering_at is random location around
var target: Node

onready var title := $ViewportBars/Title
onready var level := $ViewportBars/Level

onready var viewport_bars: Viewport = $ViewportBars
onready var health_bar: ProgressBar = $ViewportBars/HealthBar
onready var stamina_bar: ProgressBar = $ViewportBars/StaminaBar
onready var mana_bar: ProgressBar = $ViewportBars/ManaBar
onready var angry_bar: ProgressBar = $ViewportBars/AngryBar
onready var xp_bar: ProgressBar = $ViewportBars/XpBar

func _ready() -> void:
	#Log.verbose(str(parent) + " -- Stats::_ready()", parent.logchan)
	init_data()

func init_data() -> void:
	set_max_health(max_health)
	set_health_to_max()
	#Log.verbose(str(parent) + " ++ Stats::init_data(): health=" + str(health) + " / " + str(max_health) + ", damage=" + str(damage) + ", reward=" + str(reward), parent.logchan)

func get_label_width() -> float:
	#Log.verbose(str(parent) + " -- Stats::get_label_width()", parent.logchan)
	return viewport_bars.size.x

func set_label_width(width: float) -> void:
	#Log.verbose(str(parent) + " -- Stats::set_label_width(" + str(width) + ")", parent.logchan)
	viewport_bars.size.x = width
	health_bar.margin_right = width

func set_max_health(new_max_health: float) -> void:
	#Log.verbose(str(parent) + " -- Stats::set_max_health(" + str(new_max_health) + ")", parent.logchan)
	max_health = new_max_health
	health_bar.max_value = max_health

func set_health(new_health: float) -> void:
	#Log.verbose(str(parent) + " -- Stats::set_health(" + str(new_health) + ")", parent.logchan)
	health = new_health
	health_bar.value = new_health

func set_health_to_max() -> void:
	#Log.verbose(str(parent) + " -- Stats::set_health_to_max()", parent.logchan)
	set_health(max_health)

func cure(amount: float) -> void:
	#Log.verbose(str(parent) + " -- Stats::cure(" + str(amount) + ")", parent.logchan)
	set_health(health + amount)
