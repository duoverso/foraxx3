extends MarginContainer
class_name TopMenu

var logchan := "topmenu"

onready var menu_container: Control = $"%Menu"
#onready var menu_toggler: Control = $"%MenuToggler"
onready var speedbar: Control = $"%Speedbar"
onready var zoombar: Control = $"%Zoombar"
onready var sizebar: Control = $"%Sizebar"
onready var menus := {
	#"game": $Menu/GameButton.get_popup(),
	#"level": $"Menu/LevelButton".get_popup(),
	#"player": $"Menu/PlayerButton".get_popup(),
}

var generator: TerrainGenerator

# Array keys are used for item_ids, Dictionaries to hold value and label
var chunk_size_options := [
	{10:   "10 × 10 m"},
	{20:   "20 × 20 m"},
	{50:   "50 × 50 m"},
	{100:  "100 × 100 m"},
	{200:  "200 × 200 m"},
	{500:  "500 × 500 m"},
	{1000: "1,000 × 1,000 m"},
	{2000: "2,000 × 2,000 m"},
	{5000: "5,000 × 5,000 m"},
	{10000: "10,000 × 10,000 m"},
]
var grid_options := [
	{0:  "1×1"},
	{1:  "3×3"},
	{2:  "5×5"},
	{3:  "7×7"},
	{4:  "9×9"},
	{5:  "11×11"},
	{6:  "13×13"},
	{7:  "15×15"},
	{8:  "17×17"},
	{9:  "19×19"},
	{10: "21×21"},
	{11: "23×23"},
	{12: "25×25"},
	{13: "27×27"},
	{14: "29×29"},
	{15: "31×31"},
	{16: "33×33"},
]
var chunk_subdivide_options := [
	{100.0: "Subdivide 100.0"},
	{50.0: "Subdivide 50.0"},
	{20.0: "Subdivide 20.0"},
	{10.0: "Subdivide 10.0"},
	{5.0: "Subdivide 5.0"},
	{2.0:  "Subdivide 2.0"},
	{1.0:  "Subdivide 1.0"},
]
var terrain_periods_options := [
	{10: "Period 10 m"},
	{20: "Period 20 m"},
	{50: "Period 50 m"},
	{100: "Period 100 m"},
	{200: "Period 200 m"},
	{500: "Period 500 m"},
	{1000: "Period 1,000 m"},
	{2000: "Period 2,000 m"},
	{5000: "Period 5,000 m"},
	{10000: "Period 10,000 m"},
	{20000: "Period 20,000 m"},
	{50000: "Period 50,000 m"},
	{100000: "Period 100,000 m"},
]
var terrain_heights_options := [
	{10: "10 m"},
	{20: "20 m"},
	{50: "50 m"},
	{100: "100 m"},
	{200: "200 m"},
	{500: "500 m"},
	{1000: "1,000 m"},
	{2000: "2,000 m"},
	{5000: "5,000 m"},
	{10000: "10,000 m"},
]

#onready var player: Player = Global.player
var player: Player
var abilities_options: Dictionary

var speed_presets_first_idx := 3
var speed_presets := [
	{"value": 50, "text": "50 (1)"},
	{"value": 100, "text": "100 (2)"},
	{"value": 200, "text": "200 (3)"},
	{"value": 500, "text": "500 (4)"},
	{"value": 1_000, "text": "1.000 (5)"},
	{"value": 2_000, "text": "2.000 (6)"},
	{"value": 5_000, "text": "5.000 (7)"},
	{"value": 10_000, "text": "10.000 (8)"},
]
var speed_presets_checked_idx: int

func new_menu(parent, label: String, key = null, pressed_func = null, pressed_binds := []) -> PopupMenu:
	if key == null:
		key = label.to_lower().replace(" ", "_")
	if pressed_func == null:
		pressed_func = "_on_" + key + "_menu_pressed"
	# Create new PopupMenu, name it, save it and connect it with pressed func
	var menu: PopupMenu
	if parent is MenuButton:
		menu = parent.get_popup()
		#menu.clear()
	elif parent is PopupMenu:
		menu = PopupMenu.new()
	menus[key] = menu
	menu.name = key + "_menu"
	menu.hide_on_checkable_item_selection = false
	menu.allow_search = true

	if has_method("_on_" + key + "_menu_popup"):
		menu.connect("about_to_show", self, "_on_" + key + "_menu_popup")

	menu.connect("id_pressed", self, pressed_func, pressed_binds)

	# Add menu to parrent
	if parent is PopupMenu:
		parent.add_child(menu)
		parent.add_submenu_item(label, key + "_menu")

	Log.verbose("new_menu(" + str(parent.name) \
		+ ", \"" + label + "\"" \
		+ ", " + ("\"" + key + "\"" if key is String else str(key)) \
		+ ", " + ("\"" + pressed_func + "\"" if pressed_func is String else str(pressed_func)) \
		+ ", " + str(pressed_binds) + ")", logchan)

	return menu

#func add_checkbox(menu: PopupMenu, label: String, check := false, tooltip := "") -> int:
func add_checkbox(menu: PopupMenu, label: String, check := false, tooltip := "") -> void:
	menu.add_check_item(label)
	# nothing works to get idx of item added to PopupMenu, only get_item_count()-1 just when it's added
	var idx := menu.get_item_count()-1
	if check:
		menu.set_item_checked(idx, true)
	if tooltip:
		menu.set_item_tooltip(idx, tooltip)
	#return idx

#func add_radio(parent: PopupMenu, label: String, check := false, tooltip := "") -> int:
func add_radio(menu: PopupMenu, label: String, check := false, tooltip := "") -> void:
	menu.add_radio_check_item(label)
	# nothing works to get idx of item added to PopupMenu, only get_item_count()-1 just when it's added
	var idx := menu.get_item_count()-1
	if check:
		menu.set_item_checked(idx, true)
	if tooltip:
		menu.set_item_tooltip(idx, tooltip)
	#return idx

func uncheck_all(menu: PopupMenu, first := 0, last := -1) -> void:
	if last == -1:
		last = menu.get_item_count()
	for item_idx in range(first, last):
		menu.set_item_checked(item_idx, false)

func toggle_checkable(menu: PopupMenu, idx: int) -> bool:
	var new_state: bool = not menu.is_item_checked(idx)
	menu.set_item_checked(idx, new_state)
	return new_state

func level_has_generator() -> bool:
	if LevelManager.current_scene.has_node("Generator"):
		return true
	UI.hud.set_event_text("Current level doesn't have Generator", 2.0)
	Log.warn("Current scene doesn't have Generator node", logchan)
	return false

func _ready():
	Log.verbose("_ready()", logchan)
	call_deferred("when_ready")

func when_ready() -> void:
	player = Global.player
	abilities_options = {
		"Can Rotate": player.CAN_ROTATE,
		"Can Walk": player.CAN_WALK,
		"Can Run": player.CAN_RUN,
		"Can Jump": player.CAN_JUMP,
		"Can Crouch": player.CAN_CROUCH,
		"Can Fly": player.CAN_FLY,
		"Can Use Primary": player.CAN_USE_PRIMARY,
		"Can Use Secondary": player.CAN_USE_SECONDARY,
		"Can Breathe Underwater": player.CAN_BREATHE_UNDERWATER,
	}
	if LevelManager.current_scene.has_node("Generator"):
		generator = LevelManager.current_scene.get_node("Generator")
	add_main_menu()

func _on_MenuToggler_pressed() -> void:
	menu_container.visible = not menu_container.visible
	#menu_toggler.text = "<" if menu_container.visible else ">"

func add_main_menu(parent = null) -> void:
	var menu: PopupMenu
	if not parent:
		menu = $"%MenuButton".get_popup()
		menu.name = "main"
		menus["main"] = menu
		menu.allow_search = true
		menu.hide_on_checkable_item_selection = false
	else:
		menu = new_menu(parent, "Main")
	menu.add_item("Capture Mouse (Esc | U)")
	menu.add_item("Back to Start (Home)")
	add_saves_menu(menu)
	add_player_menu(menu)
	menu.add_separator()
	add_music_menu(menu)
	add_level_menu(menu)
	add_settings_menu(menu)
	add_debug_menu(menu)
	menu.add_separator()
	add_help_menu(menu)
	menu.add_item("Quit")

#	if not menu.is_connected("id_pressed", self, "_on_main_menu_pressed"):
	menu.connect("id_pressed", self, "_on_main_menu_pressed")

func _on_main_menu_pressed(item_id: int) -> void:
	var item_text: String = menus["main"].get_item_text(item_id)
	match item_text:
		"Capture Mouse (Esc | U)":
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			#Global.toggle_mouse_mode()
		"Back to Start (Home)":
			LevelManager.return_player_to_start()
		"Quit":
			get_tree().quit()
		_:
			Log.err("Unprocessed item in Main menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_level_menu(parent = null) -> void:
	if not parent:
		parent = $"%LevelButton".get_popup()
	var menu: PopupMenu = new_menu(parent, "Level")
	#for level_key in LevelManager.levels.keys():
	#	menu.add_item(LevelManager.levels[level_key]["name"])
	menu.add_item("Open Custom Level")
	menu.add_separator()
	add_environment_menu(menu)
	menu.add_separator()
	add_checkbox(menu, "Generator", generator and generator.is_processing())
	if level_has_generator():
		add_chunks_menu(menu)
		add_terrain_menu(menu)
		add_sea_menu(menu)
		add_trees_menu(menu)
		add_buildings_menu(menu)

func _on_level_menu_pressed(item_id: int) -> void:
	var item_text: String = menus["level"].get_item_text(item_id)
	match item_text:
		"Open Custom Level":
			UI.hud.file_dialog.popup()
		"Generator":
			#generator.set_process(!generator.is_processing())
			var menu = menus["level"]
			menu.set_item_checked(item_id, !menu.is_item_checked(item_id)) # toggle checkbox
			generator.set_process(menu.is_item_checked(item_id)) # set generator processing by checkbox
			#generator.noise.seed = randi()
		_:
			var level_found := false
			for level in LevelManager.levels.keys():
				if item_text == LevelManager.levels[level]["name"]:
					LevelManager.goto_level(level)
					level_found = true

			if not level_found:
				Log.err("Unprocessed item in Level menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_player_menu(parent = null) -> void:
	if not parent:
		parent = $"%PlayerButton".get_popup()
	var menu: PopupMenu = new_menu(parent, "Player")
	add_abilities_menu(menu)
	add_weapons_menu(menu)
	add_cam_menu(menu)
	add_speed_presets_menu(menu)
	add_player_size_menu(menu)
	menu.add_separator()
	add_checkbox(menu, "Fly (Tab)")
	add_checkbox(menu, "Flashlight (F)")

func _on_player_menu_pressed(item_id: int) -> void:
	var item_text: String = menus["player"].get_item_text(item_id)
	match item_text:
		"Fly (Tab)":
			if player.fly_mode == 1:
				#print("falling")
				player.fly_mode = 0
				player.gravity = -60
			# Switch to Keyboard Fly Mode
			else:
				#print("fly with keyboard")
				player.fly_mode = 1
				player.gravity = 0
				player.velocity.y = 0
		"Flashlight (F)":
			player.flashlight.visible = not player.flashlight.visible
		_:
			Log.err("Unprocessed item in Player menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_debug_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Debug")
	add_checkbox(menu, "Show (F12)", UI.hud.log_container.visible)
	add_checkbox(menu, "Console (Ctrl+`)", UI.console.visible)
	add_log_menu(menu)
	menu.add_separator()
	add_checkbox(menu, "Blood", UI.hud.blood.visible)
	add_checkbox(menu, "Dead", UI.hud.dead.visible)
	menu.add_item("Alert!")
	menu.add_item("Open Data Folder")
func _on_debug_menu_popup():
	UI.hud.set_event_text("Debug popup", 0.5)

func _on_debug_menu_pressed(item_id: int) -> void:
	var menu: PopupMenu = menus["debug"]
	var item_text: String = menu.get_item_text(item_id)
	match item_text:
		"Show (F12)":
			#toggle_checkable(menu, item_id)
			UI.hud.set_debug_visibility(not UI.hud.debug_label.visible)
		"Console (Ctrl+`)":
			UI.console.visible = not UI.console.visible
			menu.set_item_checked(item_id, UI.console.visible)
		"Blood":
			UI.hud.blood.visible = toggle_checkable(menu, item_id)
		"Dead":
			UI.hud.dead.visible = toggle_checkable(menu, item_id)
		"Alert!":
			OS.alert("Alert!")
		"Open Data Folder":
			OS.shell_open(ProjectSettings.globalize_path("user://"))
		_:
			Log.err("Unprocessed item in Debug menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

# Called by UI.hud.set_debug_visibility()
func update_debug_visibility() -> void:
	menus["debug"].set_item_checked(0, UI.hud.debug_label.visible)

# Called by UI.hud.set_log_visibility()
func update_log_visibility() -> void:
	menus["log"].set_item_checked(1, UI.hud.log_container.visible)

func add_log_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Log")
	add_checkbox(menu, "Enabled", Log.enabled)
	add_checkbox(menu, "Show", UI.hud.log_container.visible)
	menu.add_item("Clear")

func _on_log_menu_pressed(item_id: int) -> void:
	var menu: PopupMenu = menus["log"]
	var item_text: String = menu.get_item_text(item_id)
	match item_text:
		"Enabled":
			Log.enabled = not Log.enabled
			menu.set_item_checked(item_id, Log.enabled)
		"Show":
			UI.hud.set_log_visibility(not UI.hud.log_container.visible)
		"Clear":
			Log.clear_all()
			UI.hud.log_container.text = ""

func add_help_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Help")
	menu.add_item("Controls (F1)")
	menu.add_separator()
	menu.add_item("Web")
	menu.add_item("Discord")
	menu.add_item("Trello")
	menu.add_item("Git")
	menu.add_item("About Antray")

func _on_help_menu_pressed(item_id: int) -> void:
	var menu: PopupMenu = menus["help"]
	var item_text: String = menu.get_item_text(item_id)
	match item_text:
		"Controls (F1)":
			UI.hud.toggle_help()
		"Web":
			OS.shell_open("https://antray.duoverso.com")
		"Discord":
			OS.shell_open("https://discord.gg/aPFcgZsfxs")
		"Issues":
			OS.shell_open("https://gitlab.com/duoverso/antray/-/issues")
		"Trello":
			OS.shell_open("https://trello.com/b/AEBgillp/antray")
		"Git":
			OS.shell_open("https://gitlab.com/duoverso/antray")
		"About Antray":
			UI.hud.show_about()
		_:
			Log.err("Unprocessed item in Help menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_settings_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Settings")
	add_sound_volume_menu(menu)
	add_checkbox(menu, "Fullscreen (F11)", OS.window_fullscreen)
	add_onstart_menu(menu)
	add_theme_menu(menu)
	add_panels_menu(menu)
	menu.add_item("Window - Default")
	menu.add_item("Window - 720x720")
	menu.add_item("Window - 1280x1280")
	add_checkbox(menu, "Virtual Joysticks (J)", UI.hud.left_joystick.visible)

func _on_settings_menu_pressed(item_id: int) -> void:
	var menu: PopupMenu = menus["settings"]
	var item_text: String = menu.get_item_text(item_id)
	match item_text:
		"Fullscreen (F11)":
			OS.window_fullscreen = !OS.window_fullscreen
			menu.set_item_checked(item_id, OS.window_fullscreen)
		"Window - Default":
			print(get_viewport().size_override_stretch)
			get_viewport().set_size_override(false)
		"Window - 720x720":
			#get_viewport().set_size_override(true, Vector2(720, 720))
			OS.set_window_size(Vector2(720, 720))
		"Window - 1280x1280":
			#get_viewport().set_size_override(true, Vector2(1280, 1280))
			OS.set_window_size(Vector2(1280, 1280))
		"Virtual Joysticks (J)":
			#UI.hud.toggle_joysticks()
			if toggle_checkable(menu, item_id):
				UI.hud.show_joysticks()
			else:
				UI.hud.hide_joysticks()
		_:
			Log.err("Unprocessed item in Settings menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_onstart_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "On start", "onstart")
	add_checkbox(menu, "Resize window to 3/4", UI.onstart_resize_window)
	add_checkbox(menu, "Maximize window", UI.onstart_maximize_window)
	add_checkbox(menu, "Fullscreen", UI.onstart_fullscreen)

func _on_onstart_menu_pressed(item_id: int) -> void:
	var menu: PopupMenu = menus["onstart"]
	var item_text: String = menu.get_item_text(item_id)
	match item_text:
		"Resize window to 3/4":
			UI.onstart_resize_window = toggle_checkable(menu, item_id)
		"Maximize window":
			UI.onstart_maximize_window = toggle_checkable(menu, item_id)
		"Fullscreen":
			UI.onstart_fullscreen = toggle_checkable(menu, item_id)
		_:
			Log.err("Unprocessed item in Saves menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_theme_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Theme")
	add_radio(menu, "Black on White", theme and theme.resource_path == "res://assets/ui/theme/black-on-white/antray.theme")
	add_radio(menu, "White on Black", theme and theme.resource_path == "res://assets/ui/theme/white-on-black/antray.theme")

func _on_theme_menu_pressed(item_id: int) -> void:
	var menu: PopupMenu = menus["theme"]
	var item_text: String = menu.get_item_text(item_id)
	match item_text:
		"Black on White":
			theme = load("res://assets/ui/theme/black-on-white/antray.theme")
		"White on Black":
			theme = load("res://assets/ui/theme/white-on-black/antray.theme")
		_:
			Log.err("Unprocessed item in Themes menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_panels_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Panels")
	add_checkbox(menu, "Health", UI.hud.healthbar.visible)
	add_checkbox(menu, "Weapon", UI.hud.weaponbar.visible)
	add_checkbox(menu, "Money", UI.hud.moneybar.visible)

func _on_panels_menu_pressed(item_id: int) -> void:
	var menu: PopupMenu = menus["panels"]
	var item_text: String = menu.get_item_text(item_id)
	match item_text:
		"Health":
			UI.hud.set_healthbar_visibility(not UI.hud.healthbar.visible)
		"Weapon":
			UI.hud.set_weaponbar_visibility(not UI.hud.weaponbar.visible)
		"Money":
			UI.hud.set_moneybar_visibility(not UI.hud.moneybar.visible)

# Called by UI.hud.set_healthbar_visibility()
func update_healthbar_visibility() -> void:
	menus["panels"].set_item_checked(0, UI.hud.healthbar.visible)

# Called by UI.hud.set_weaponbar_visibility()
func update_weaponbar_visibility() -> void:
	menus["panels"].set_item_checked(1, UI.hud.weaponbar.visible)

# Called by UI.hud.set_moneybar_visibility()
func update_moneybar_visibility() -> void:
	menus["panels"].set_item_checked(2, UI.hud.moneybar.visible)

func add_saves_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Saves")
	menu.add_item("Save (F2)")
	menu.add_item("Load (F3)")
	menu.add_item("Reset")

func _on_saves_menu_pressed(item_id: int) -> void:
	var item_text: String = menus["saves"].get_item_text(item_id)
	match item_text:
		"Save (F2)":
			Settings.save_user_config()
		"Load (F3)":
			Settings.load_user_config()
		"Reset":
			Settings.reset()
		_:
			Log.err("Unprocessed item in Saves menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_music_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Music")
	add_music_volume_menu(menu)
	menu.add_separator()
	menu.add_item("Play/Stop (.)")
	menu.add_item("Next Track (,)")
	add_checkbox(menu, "Shuffle", Music.has_shuffle)
	menu.add_separator()
	add_music_tracks_menu(menu)

func _on_music_menu_pressed(item_id: int) -> void:
	var menu: PopupMenu = menus["music"]
	var item_text: String = menu.get_item_text(item_id)
	match item_text:
		"Play/Stop (.)":
			Music.toggle_playing()
		"Next Track (,)":
			Music.play_next()
		"Shuffle":
			var shuffle: bool = Music.toggle_shuffle()
			menu.set_item_checked(item_id, shuffle)
			UI.hud.set_event_text("Shuffle Music " + str("ON" if shuffle else "OFF"), 1.0)
		_:
			Log.err("Unprocessed item in Music menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_music_volume_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Volume", "music_volume")
	var bus_volume := int(Music.bus_volume * 100)
	for volume in range(0, 110, 10):
		add_radio(menu, str(volume) + "%", volume == bus_volume)

func _on_music_volume_menu_pressed(item_id: int) -> void:
	var menu: PopupMenu = menus["music_volume"]
	#var item_text: String = menu.get_item_text(item_id)
	for i in range(0, 11):
		menu.set_item_checked(i, false)
	menu.set_item_checked(item_id, true)
	Music.bus_volume = item_id * 0.1

func add_sound_volume_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Sound Volume", "sound_volume")
	var bus_volume := int(Sound.bus_volume * 100)
	for volume in range(0, 110, 10):
		add_radio(menu, str(volume) + "%", volume == bus_volume)

func _on_sound_volume_menu_pressed(item_id: int) -> void:
	var menu: PopupMenu = menus["sound_volume"]
	#var item_text: String = menu.get_item_text(item_id)
	for i in range(0, 11):
		menu.set_item_checked(i, false)
	menu.set_item_checked(item_id, true)
	Sound.bus_volume = item_id * 0.1

func add_music_tracks_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Tracks", "music_tracks")
	for track in Music.tracks:
		add_radio(menu, track)

func _on_music_tracks_menu_pressed(item_id: int) -> void:
	var menu: PopupMenu = menus["music_tracks"]
	#var item_text: String = menu.get_item_text(item_id)
	for i in range(menu.get_item_count()):
		menu.set_item_checked(i, false)
	menu.set_item_checked(item_id, true)
	Music.play_track(item_id)

func add_environment_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Environment")
	add_world_environment_menu(menu)
	add_light_menu(menu)

func add_world_environment_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "World", "world_environment")
	menu.add_item("Random (F7)")
	menu.add_separator()
	for env in LevelEnvironment.environments:
		add_radio(menu, env, env == LevelEnvironment.current)

func _on_world_environment_menu_pressed(item_id: int) -> void:
	var menu = menus["world_environment"]
	var item_text: String = menu.get_item_text(item_id)
	match item_text:
		"Random (F7)":
			LevelEnvironment.rand_environment()
		_:
			uncheck_all(menu)
			var env_found = LevelEnvironment.set_environment(item_text)
			if env_found is bool and env_found == false:
				UI.hud.set_event_text("Environment can't be changed to " + item_text + " now", 1.0)
			else:
				#UI.hud.set_event_text("Environment changed to " + item_text, 1.0)
				menu.set_item_checked(item_id, true)

func add_light_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Light")
	add_light_rotation_x_menu(menu)
	add_light_rotation_y_menu(menu)
	add_light_color_menu(menu)
	add_light_bake_mode_menu(menu)
	add_light_energy_menu(menu)
	add_light_indirect_energy_menu(menu)
	add_light_size_menu(menu) # for Light.BAKE_ALL only
	add_checkbox(menu, "Negative", LevelEnvironment.light.light_negative)
	add_light_specular_menu(menu)
	menu.add_separator()
	add_checkbox(menu, "Shadow", LevelEnvironment.light.shadow_enabled)
	add_shadow_mode_menu(menu)
	add_shadow_color_menu(menu)
	add_shadow_bias_menu(menu)
	add_checkbox(menu, "Shadow Reverse", LevelEnvironment.light.shadow_reverse_cull_face)

func add_light_rotation_x_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Rotation X", "light_rotation_x")
	for deg in range(0, 361, 30):
		add_radio(menu, str(deg), deg == LevelEnvironment.light.rotation_degrees.x)

func _on_light_rotation_x_menu_pressed(item_id: int) -> void:
	var menu = menus["light_rotation_x"]
	var item_text: String = menu.get_item_text(item_id)
	uncheck_all(menu)
	menu.set_item_checked(item_id, true)
	LevelEnvironment.light.rotation_degrees.x = float(item_text)

func add_light_rotation_y_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Rotation Y", "light_rotation_y")
	for deg in range(0, 361, 30):
		add_radio(menu, str(deg), deg == LevelEnvironment.light.rotation_degrees.y)

func _on_light_rotation_y_menu_pressed(item_id: int) -> void:
	var menu = menus["light_rotation_y"]
	var item_text: String = menu.get_item_text(item_id)
	uncheck_all(menu)
	menu.set_item_checked(item_id, true)
	LevelEnvironment.light.rotation_degrees.y = float(item_text)

func _on_light_menu_pressed(item_id: int) -> void:
	var menu = menus["light"]
	var item_text: String = menu.get_item_text(item_id)
	match item_text:
		"Negative":
			LevelEnvironment.light.light_negative = toggle_checkable(menu, item_id)
		"Shadow":
			LevelEnvironment.light.shadow_enabled = toggle_checkable(menu, item_id)
		"Shadow Reverse":
			LevelEnvironment.light.shadow_reverse_cull_face = toggle_checkable(menu, item_id)
		_:
			Log.err("Unprocessed item in Light menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_light_color_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Color", "light_color")
	add_radio(menu, "White", LevelEnvironment.light.light_color == Color.white)
	add_radio(menu, "Black")
	add_radio(menu, "Red")
	add_radio(menu, "Yellow")
	add_radio(menu, "Green")
	add_radio(menu, "Cyan")
	add_radio(menu, "Blue")
	add_radio(menu, "Magenta")

func _on_light_color_menu_pressed(item_id: int) -> void:
	var menu = menus["light_color"]
	var item_text: String = menu.get_item_text(item_id)
	uncheck_all(menu)
	menu.set_item_checked(item_id, true)
	match item_text:
		"White":
			LevelEnvironment.light.light_color = Color.white
		"Black":
			LevelEnvironment.light.light_color = Color.black
		"Red":
			LevelEnvironment.light.light_color = Color.red
		"Yellow":
			LevelEnvironment.light.light_color = Color.yellow
		"Green":
			LevelEnvironment.light.light_color = Color.green
		"Cyan":
			LevelEnvironment.light.light_color = Color.cyan
		"Blue":
			LevelEnvironment.light.light_color = Color.blue
		"Magenta":
			LevelEnvironment.light.light_color = Color.magenta
		_:
			Log.err("Unprocessed item in Light Color menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_light_bake_mode_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Bake Mode", "light_bake_mode")
	add_radio(menu, "Disabled", LevelEnvironment.light.light_bake_mode == Light.BAKE_DISABLED)
	add_radio(menu, "Indirect Only", LevelEnvironment.light.light_bake_mode == Light.BAKE_INDIRECT)
	add_radio(menu, "All (Direct + Indirect)", LevelEnvironment.light.light_bake_mode == Light.BAKE_ALL)

func _on_light_bake_mode_menu_pressed(item_id: int) -> void:
	var menu = menus["light_bake_mode"]
	var item_text: String = menu.get_item_text(item_id)
	uncheck_all(menu)
	menu.set_item_checked(item_id, true)
	match item_text:
		"Disabled":
			LevelEnvironment.light.light_bake_mode = Light.BAKE_DISABLED
		"Indirect Only":
			LevelEnvironment.light.light_bake_mode = Light.BAKE_INDIRECT
		"All (Direct + Indirect)":
			LevelEnvironment.light.light_bake_mode = Light.BAKE_ALL
		_:
			Log.err("Unprocessed item in Light Bake Mode menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_light_energy_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Energy", "light_energy")
	for i in range(0, 10, 1) + range(10, 20, 2) + range(20, 50, 10):
		var energy: float = i / 10.0
		add_radio(menu, str(energy), energy == LevelEnvironment.light.light_energy)

func _on_light_energy_menu_pressed(item_id: int) -> void:
	var menu = menus["light_energy"]
	var item_text: String = menu.get_item_text(item_id)
	uncheck_all(menu)
	menu.set_item_checked(item_id, true)
	LevelEnvironment.light.light_energy = float(item_text)

func add_light_indirect_energy_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Indirect Energy", "light_indirect_energy")
	for i in range(0, 10, 1) + range(10, 20, 2) + range(20, 50, 10):
		var energy: float = i / 10.0
		add_radio(menu, str(energy), energy == LevelEnvironment.light.light_indirect_energy)

func _on_light_indirect_energy_menu_pressed(item_id: int) -> void:
	var menu = menus["light_indirect_energy"]
	var item_text: String = menu.get_item_text(item_id)
	uncheck_all(menu)
	menu.set_item_checked(item_id, true)
	LevelEnvironment.light.light_indirect_energy = float(item_text)

# @TODO: For Bake Mode == BAKE_ALL only
func add_light_size_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Size", "light_size")
	for i in range(0, 11, 1):
		var size := i / 10.0
		add_radio(menu, str(size), size == LevelEnvironment.light.light_size)

func _on_light_size_menu_pressed(item_id: int) -> void:
	var menu = menus["light_size"]
	var item_text: String = menu.get_item_text(item_id)
	uncheck_all(menu)
	menu.set_item_checked(item_id, true)
	LevelEnvironment.light.light_size = float(item_text)

func add_light_specular_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Specular", "light_specular")
	for i in range(0, 10, 1) + range(10, 20, 2) + range(20, 160, 10):
		var specular: float = i / 10.0
		add_radio(menu, str(specular), specular == LevelEnvironment.light.light_specular)

func _on_light_specular_menu_pressed(item_id: int) -> void:
	var menu = menus["light_specular"]
	var item_text: String = menu.get_item_text(item_id)
	uncheck_all(menu)
	menu.set_item_checked(item_id, true)
	LevelEnvironment.light.light_specular = float(item_text)

func add_shadow_mode_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Shadow Mode", "shadow_mode")
	add_radio(menu, "Orthogonal (Fast)", LevelEnvironment.light.directional_shadow_mode == DirectionalLight.SHADOW_ORTHOGONAL)
	add_radio(menu, "PSSM 2 Splits (Average)", LevelEnvironment.light.directional_shadow_mode == DirectionalLight.SHADOW_PARALLEL_2_SPLITS)
	add_radio(menu, "PSSM 4 Splits (Slow)", LevelEnvironment.light.directional_shadow_mode == DirectionalLight.SHADOW_PARALLEL_4_SPLITS)

func _on_shadow_mode_menu_pressed(item_id: int) -> void:
	var menu = menus["shadow_mode"]
	var item_text: String = menu.get_item_text(item_id)
	uncheck_all(menu)
	menu.set_item_checked(item_id, true)
	match item_text:
		"Orthogonal (Fast)":
			LevelEnvironment.light.directional_shadow_mode = DirectionalLight.SHADOW_ORTHOGONAL
		"PSSM 2 Splits (Average)":
			LevelEnvironment.light.directional_shadow_mode = DirectionalLight.SHADOW_PARALLEL_2_SPLITS
		"PSSM 4 Splits (Slow)":
			LevelEnvironment.light.directional_shadow_mode = DirectionalLight.SHADOW_PARALLEL_4_SPLITS
		_:
			Log.err("Unprocessed item in Shadow Mode menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_shadow_color_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Shadow Color", "shadow_color")
	add_radio(menu, "White", LevelEnvironment.light.shadow_color == Color.white)
	add_radio(menu, "Black", LevelEnvironment.light.shadow_color == Color.black)
	add_radio(menu, "Red")
	add_radio(menu, "Yellow")
	add_radio(menu, "Green")
	add_radio(menu, "Cyan")
	add_radio(menu, "Blue")
	add_radio(menu, "Magenta")

func _on_shadow_color_menu_pressed(item_id: int) -> void:
	var menu = menus["shadow_color"]
	var item_text: String = menu.get_item_text(item_id)
	uncheck_all(menu)
	menu.set_item_checked(item_id, true)
	match item_text:
		"White":
			LevelEnvironment.light.shadow_color = Color.white
		"Black":
			LevelEnvironment.light.shadow_color = Color.black
		"Red":
			LevelEnvironment.light.shadow_color = Color.red
		"Yellow":
			LevelEnvironment.light.shadow_color = Color.yellow
		"Green":
			LevelEnvironment.light.shadow_color = Color.green
		"Cyan":
			LevelEnvironment.light.shadow_color = Color.cyan
		"Blue":
			LevelEnvironment.light.shadow_color = Color.blue
		"Magenta":
			LevelEnvironment.light.shadow_color = Color.magenta
		_:
			Log.err("Unprocessed item in Shadow Color menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_shadow_bias_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Shadow Bias", "shadow_bias")
	for bias in [-10, -5, -2, -1, -0.8, -0.6, -0.4, -0.3, -0.2, -0.15, -0.1, -0.05, 0, 0.05, 0.1, 0.15, 0.2, 0.3, 0.4, 0.6, 0.8, 1, 2 -5, 10]:
		add_radio(menu, str(bias), bias == LevelEnvironment.light.shadow_bias)

func _on_shadow_bias_menu_pressed(item_id: int) -> void:
	var menu = menus["shadow_bias"]
	var item_text: String = menu.get_item_text(item_id)
	uncheck_all(menu)
	menu.set_item_checked(item_id, true)
	LevelEnvironment.light.shadow_bias = float(item_text)

func add_chunks_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Chunks")
	add_grid_menu(menu)
	add_chunk_size_menu(menu)
	add_chunk_subdivide_menu(menu)

func add_grid_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Grid")
	add_checkbox(menu, "From Center", generator.from_center)
	add_checkbox(menu, "Circular", generator.circular)
	menu.add_separator()
	for item in grid_options:
		add_radio(menu, item.values()[0], generator.chunk_dist == item.keys()[0])

func _on_grid_menu_pressed(item_id: int) -> void:
	if not level_has_generator():
		return

	var menu: PopupMenu = menus["grid"]
	if item_id == 0: # From Center
		generator.from_center = toggle_checkable(menu, item_id)
	elif item_id == 1: # Circular
		generator.circular = toggle_checkable(menu, item_id)
	elif menu.is_item_checkable(item_id):
		var first := 3
		uncheck_all(menu, first)
		# Check new
		menu.set_item_checked(item_id, true) # !menu.is_item_checked(item_id))
		# Set Grid of Chunks
		generator.chunk_dist = grid_options[item_id - first].keys()[0]

	#var item_text: String = menu.get_item_text(item_id)
#	match item_text:
#		"Grid 1×1":
#			generator.chunk_dist = 0
#		_:
#			Log.err("Unprocessed item in Grid menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)
#			return

	generator.update_chunks()

func add_chunk_size_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Size", "chunk_size")
	for item in chunk_size_options:
		add_radio(menu, item.values()[0], generator.chunk_size == item.keys()[0])

func _on_chunk_size_menu_pressed(item_id: int) -> void:
	if not level_has_generator():
		return

	var menu: PopupMenu = menus["chunk_size"]
	uncheck_all(menu)
	# Check new
	menu.set_item_checked(item_id, true)

	# Set Chunk Size
	generator.chunk_size = chunk_size_options[item_id].keys()[0]
	#var item_text: String = menus["chunk_size"].get_item_text(item_id)
#	match item_text:
#		"25×25":
#			generator.chunk_size = 25
#		_:
#			Log.err("Unprocessed item in Chunk Size menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)
#			return

	generator.force_update_chunks()

func add_chunk_subdivide_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Subdivide", "chunk_subdivide")
	for item in chunk_subdivide_options:
		add_radio(menu, item.values()[0], generator.chunk_subdivide_per_size == item.keys()[0])

func _on_chunk_subdivide_menu_pressed(item_id: int) -> void:
	if not level_has_generator():
		return

	var menu: PopupMenu = menus["chunk_subdivide"]

	if menu.is_item_checkable(item_id):
		uncheck_all(menu)
		# Check new
		menu.set_item_checked(item_id, true) # !menu.is_item_checked(item_id))

	generator.chunk_subdivide_per_size = chunk_subdivide_options[item_id].keys()[0]
	#var item_text = menus["chunk_subdivide"].get_item_text(item_id)
#	match item_text:
#		"Subdivide 2.0":
#			generator.chunk_subdivide = generator.chunk_size * 2.0
#		_:
#			Log.err("Unprocessed item in Terrain Subdivide menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)
#			return

	generator.force_update_chunks()

func add_terrain_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Terrain")
	add_checkbox(menu, "Enabled", generator.options.has(generator.GENERATE_TERRAIN))
	menu.add_separator()
	add_terrain_materials_menu(menu)
	add_terrain_noise_menus(menu)

func _on_terrain_menu_pressed(item_id: int) -> void:
	if not level_has_generator():
		return

	var menu: PopupMenu = menus["terrain"]

	if item_id == 0:
		menu.set_item_checked(item_id, !menu.is_item_checked(item_id)) # Toggle check
		# Toggle option in generator
		if menu.is_item_checked(item_id):
			generator.enable_terrain()
		else:
			generator.disable_terrain()
	else:
		var item_text: String = menus["terrain"].get_item_text(item_id)
		Log.err("Unprocessed item in Terrain menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_terrain_materials_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Material", "terrain_materials")
	add_checkbox(menu, "Reload on change", generator.options.has(generator.RELOAD_TERRAIN_MATERIAL_ON_CHANGE))
	menu.add_separator()
	for item in generator.available_terrain_materials:
		add_radio(menu, item, item == generator.terrain_material_name)

func _on_terrain_materials_menu_pressed(item_id: int) -> void:
	if not level_has_generator():
		return

	var menu: PopupMenu = menus["terrain_materials"]

	if item_id == 0:
		menu.set_item_checked(item_id, !menu.is_item_checked(item_id)) # Toggle check
		# Toggle option in generator
		generator.set_reload_terrain_material_on_change(!generator.options.has(generator.RELOAD_TERRAIN_MATERIAL_ON_CHANGE))
	else:
		uncheck_all(menu, 2)
		# Check new
		menu.set_item_checked(item_id, true)

		var item_text: String = menus["terrain_materials"].get_item_text(item_id)
		generator.terrain_material_name = item_text

func add_terrain_noise_menus(parent: PopupMenu) -> void:
	menus["terrain_noises"] = []
	menus["terrain_noises_octaves"] = []
	menus["terrain_noises_periods"] = []
	menus["terrain_noises_heights"] = []
	for noise_id in range(generator.noises.size()):
		var noise = generator.noises[noise_id]
		var menu := PopupMenu.new()
		menus["terrain_noises"].append(menu)
		menu.set_name("terrain_noises_menu" + str(noise_id))
		menu.allow_search = true
		add_checkbox(menu, "Enabled", true)
		parent.add_child(menu)
		parent.add_submenu_item("Noise " + str(noise_id), "terrain_noises_menu" + str(noise_id))

		var menu_octaves := PopupMenu.new()
		menus["terrain_noises_octaves"].append(menu_octaves)
		menu_octaves.set_name("terrain_noises_octaves_menu" + str(noise_id))
		menu_octaves.allow_search = true
		for octaves in range(1, 10):
			add_radio(menu_octaves, "Octaves " + str(octaves))
		menu_octaves.set_item_checked(noise.octaves-1, true) # Check default
		menu_octaves.connect("id_pressed", self, "_on_terrain_noises_octaves_menu_pressed", [noise_id])
		menu.add_child(menu_octaves)
		menu.add_submenu_item("Octaves", "terrain_noises_octaves_menu" + str(noise_id))

		var menu_periods := PopupMenu.new()
		menus["terrain_noises_periods"].append(menu_periods)
		menu_periods.set_name("terrain_noises_periods_menu" + str(noise_id))
		menu_periods.allow_search = true
		for period in terrain_periods_options:
			add_radio(menu_periods, period.values()[0], noise.period == period.keys()[0])
		menu_periods.connect("id_pressed", self, "_on_terrain_noises_periods_menu_pressed", [noise_id])
		menu.add_child(menu_periods)
		menu.add_submenu_item("Period", "terrain_noises_periods_menu" + str(noise_id))

		var menu_heights := PopupMenu.new()
		menus["terrain_noises_heights"].append(menu_heights)
		menu_heights.set_name("terrain_noises_heights_menu" + str(noise_id))
		menu_heights.allow_search = true
		for height in terrain_heights_options:
			add_radio(menu_heights, height.values()[0], generator.noises_heights[noise_id] == height.keys()[0])
		menu_heights.connect("id_pressed", self, "_on_terrain_noises_heights_menu_pressed", [noise_id])
		menu.add_child(menu_heights)
		menu.add_submenu_item("Height", "terrain_noises_heights_menu" + str(noise_id))

func _on_terrain_noises_octaves_menu_pressed(item_id: int, noise_id: int) -> void:
	if not level_has_generator():
		return

	var menu: PopupMenu = menus["terrain_noises_octaves"][noise_id]
	var item_text: String = menu.get_item_text(item_id)
	var octaves = item_text.replace("Octaves ", "")
	if (octaves.is_valid_integer()):
		octaves = int(octaves)
		menu.set_item_checked(generator.noises[noise_id].octaves-1, false) # uncheck current
		menu.set_item_checked(item_id, true) # check new
		generator.noises[noise_id].octaves = octaves
		generator.force_update_chunks()
#	else:
#		Log.err("Unprocessed item in Terrain Octaves menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)
#		return

func _on_terrain_noises_periods_menu_pressed(item_id: int, noise_id: int) -> void:
	if not level_has_generator():
		return

	var menu: PopupMenu = menus["terrain_noises_periods"][noise_id]

	if menu.is_item_checkable(item_id):
		uncheck_all(menu)
		# Check new
		menu.set_item_checked(item_id, true)

	# Set Noise Period
	generator.noises[noise_id].period = terrain_periods_options[item_id].keys()[0]
	generator.force_update_chunks()

func _on_terrain_noises_heights_menu_pressed(item_id: int, noise_id: int) -> void:
	if not level_has_generator():
		return

	var menu: PopupMenu = menus["terrain_noises_heights"][noise_id]

	if menu.is_item_checkable(item_id):
		uncheck_all(menu)
		# Check new
		menu.set_item_checked(item_id, true)

	# Set Terrain Height
	generator.noises_heights[noise_id] = terrain_heights_options[item_id].keys()[0]
	generator.force_update_chunks()

func add_sea_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Sea")
	add_checkbox(menu, "Enabled", generator.options.has(generator.GENERATE_SEA))
	menu.add_separator()
	add_sea_materials_menu(menu)
	add_sea_levels_menu(menu)
	add_sea_level_speeds_menu(menu)

func _on_sea_menu_pressed(item_id: int) -> void:
	if not level_has_generator():
		return

	var menu: PopupMenu = menus["sea"]

	if item_id == 0:
		menu.set_item_checked(item_id, !menu.is_item_checked(item_id)) # Toggle check
		# Toggle option in generator
		if menu.is_item_checked(item_id):
			generator.enable_sea()
		else:
			generator.disable_sea()
	else:
		var item_text: String = menus["sea"].get_item_text(item_id)
		Log.err("Unprocessed item in Sea menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_sea_materials_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Material", "sea_materials")
	add_checkbox(menu, "Reload on change", generator.options.has(generator.RELOAD_SEA_MATERIAL_ON_CHANGE))
	menu.add_separator()
	for item in generator.available_sea_materials:
		add_radio(menu, item, item == generator.sea_material_name)

func _on_sea_materials_menu_pressed(item_id: int) -> void:
	if not level_has_generator():
		return

	var menu: PopupMenu = menus["sea_materials"]

	if item_id == 0:
		menu.set_item_checked(item_id, !menu.is_item_checked(item_id)) # Toggle check
		# Toggle option in generator
		generator.set_reload_sea_material_on_change(!generator.options.has(generator.RELOAD_SEA_MATERIAL_ON_CHANGE))
	else:
		uncheck_all(menu, 2)
		# Check new
		menu.set_item_checked(item_id, true)

		var item_text: String = menus["sea_materials"].get_item_text(item_id)
		generator.sea_material_name = item_text

func add_sea_levels_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Level", "sea_levels")
	menu.add_item("Reset to 0")
	menu.add_item("At the Player's position")
	menu.add_separator()
	menu.add_item("Increase for 10.000")
	menu.add_item("Increase for 1.000")
	menu.add_item("Increase for 100")
	menu.add_item("Increase for 10")
	menu.add_item("Increase for 1")
	menu.add_item("Increase for 0.1")
	menu.add_item("Increase for 0.01")
	menu.add_separator()
	menu.add_item("Decrease for 0.01")
	menu.add_item("Decrease for 0.1")
	menu.add_item("Decrease for 1")
	menu.add_item("Decrease for 10")
	menu.add_item("Decrease for 100")
	menu.add_item("Decrease for 1.000")
	menu.add_item("Decrease for 10.000")

func _on_sea_levels_menu_pressed(item_id: int) -> void:
	var item_text: String = menus["sea_levels"].get_item_text(item_id)
	match item_text:
		"Reset to 0":
			generator.sea_level_to = 0.0
		"At the Player's position":
			generator.sea_level_to = player.global_translation.y
		"Increase for 10.000":
			generator.sea_level_to = generator.sea_level + 10000
		"Increase for 1.000":
			generator.sea_level_to = generator.sea_level + 1000
		"Increase for 100":
			generator.sea_level_to = generator.sea_level + 100
		"Increase for 10":
			generator.sea_level_to = generator.sea_level + 10
		"Increase for 1":
			generator.sea_level_to = generator.sea_level + 1
		"Increase for 0.1":
			generator.sea_level_to = generator.sea_level + 0.1
		"Increase for 0.01":
			generator.sea_level_to = generator.sea_level + 0.01
		"Decrease for 0.01":
			generator.sea_level_to = generator.sea_level - 0.01
		"Decrease for 0.1":
			generator.sea_level_to = generator.sea_level - 0.1
		"Decrease for 1":
			generator.sea_level_to = generator.sea_level - 1
		"Decrease for 10":
			generator.sea_level_to = generator.sea_level - 10
		"Decrease for 100":
			generator.sea_level_to = generator.sea_level - 100
		"Decrease for 1.000":
			generator.sea_level_to = generator.sea_level - 1000
		"Decrease for 10.000":
			generator.sea_level_to = generator.sea_level - 10000
		_:
			Log.err("Unprocessed item in Sea Levels menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_sea_level_speeds_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Level Speed", "sea_level_speeds")
	menu.add_item("Immediate (∞ / s)")
	menu.add_separator()
	menu.add_item("Increase for 10 / s")
	menu.add_item("Increase for 1 / s")
	menu.add_item("Increase for 0.1 / s")
	menu.add_item("Increase for 0.01 / s")
	menu.add_item("Increase for 0.001 / s")
	menu.add_separator()
	menu.add_item("Decrease for 0.001 / s")
	menu.add_item("Decrease for 0.01 / s")
	menu.add_item("Decrease for 0.1 / s")
	menu.add_item("Decrease for 1 / s")
	menu.add_item("Decrease for 10 / s")

func _on_sea_level_speeds_menu_pressed(item_id: int) -> void:
	var item_text: String = menus["sea_level_speeds"].get_item_text(item_id)
	match item_text:
		"Immediate (∞ / s)":
			generator.set_sea_level_speed(0)
		"Increase for 10 / s":
			generator.set_sea_level_speed(generator.sea_level_speed + 10)
		"Increase for 1 / s":
			generator.set_sea_level_speed(generator.sea_level_speed + 1)
		"Increase for 0.1 / s":
			generator.set_sea_level_speed(generator.sea_level_speed + 0.1)
		"Increase for 0.01 / s":
			generator.set_sea_level_speed(generator.sea_level_speed + 0.01)
		"Increase for 0.001 / s":
			generator.set_sea_level_speed(generator.sea_level_speed + 0.001)
		"Decrease for 0.001 / s":
			generator.set_sea_level_speed(generator.sea_level_speed - 0.001)
		"Decrease for 0.01 / s":
			generator.set_sea_level_speed(generator.sea_level_speed - 0.01)
		"Decrease for 0.1 / s":
			generator.set_sea_level_speed(generator.sea_level_speed - 0.1)
		"Decrease for 1 / s":
			generator.set_sea_level_speed(generator.sea_level_speed - 1)
		"Decrease for 10 / s":
			generator.set_sea_level_speed(generator.sea_level_speed - 10)
		_:
			Log.err("Unprocessed item in Sea Level Speeds menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_trees_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Trees")
	add_checkbox(menu, "Reload on change", generator.options.has(generator.RELOAD_TREES_ON_CHANGE))
	menu.add_separator()
	for item in generator.available_tree_scenes:
		add_radio(menu, item, item == generator.tree_scene_name)

func _on_trees_menu_pressed(item_id: int) -> void:
	if not level_has_generator():
		return

	var menu: PopupMenu = menus["trees"]

	if item_id == 0:
		menu.set_item_checked(item_id, !menu.is_item_checked(item_id)) # Toggle check
		# Toggle option in generator
		if generator.options.has(generator.RELOAD_TREES_ON_CHANGE):
			generator.disable_reload_trees_on_change()
		else:
			generator.enable_reload_trees_on_change()
	else:
		uncheck_all(menu, 2)
		# Check new
		menu.set_item_checked(item_id, true)

		var item_text: String = menus["trees"].get_item_text(item_id)
		generator.tree_scene_name = item_text

func add_buildings_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Buildings")
	add_checkbox(menu, "Houses", generator.options.has(generator.GENERATE_BUILDINGS))
	add_checkbox(menu, "Pyramids", generator.options.has(generator.GENERATE_TOWERS))
	#menu.add_separator()
	#add_checkbox(menu, "Towers", generator.options.has(generator.GENERATE_TOWERS))

func _on_buildings_menu_pressed(item_id: int) -> void:
	if not level_has_generator():
		return

	var menu: PopupMenu = menus["buildings"]
	var item_text: String = menu.get_item_text(item_id)
	match item_text:
		"Houses":
			#if generator.options.has(generator.GENERATE_BUILDINGS):
			if menu.is_item_checked(item_id):
				generator.disable_buildings()
			else:
				generator.enable_buildings()
			menu.set_item_checked(item_id, !menu.is_item_checked(item_id)) # Toggle check
		"Pyramids":
			if menu.is_item_checked(item_id):
				generator.disable_towers()
			else:
				generator.enable_towers()
			menu.set_item_checked(item_id, !menu.is_item_checked(item_id)) # Toggle check
		_:
			Log.err("Unprocessed item in Buildings menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)
			return
	#generator.force_update_chunks()

func add_player_size_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Size", "player_size")
	menu.add_item("Reset (Ctrl+0)")
	menu.add_item("Enlarge (Ctrl+plus)")
	menu.add_item("Shrink (Ctrl+minus)")
	menu.add_separator()

func _on_player_size_menu_pressed(item_id: int) -> void:
	var item_text: String = menus["player_size"].get_item_text(item_id)
	match item_text:
		"Reset (Ctrl+0)":
			player.size = 1.0
		"Enlarge (Ctrl+plus)":
			player.size *= 1.3333
		"Shrink (Ctrl+minus)":
			player.size *= 0.75
		_:
			Log.err("Unprocessed item in Player Size menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_speed_presets_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Speed Presets")
	menu.add_item("Speed up (Num+)")
	menu.add_item("Slow down (Num-)")
	menu.add_separator()
	#menu.add_item("100 (1)")
	for num in range(speed_presets.size()):
		var item_idx := menu.get_item_count() # speed_options_first_idx + num
		speed_presets[num]["idx"] = item_idx
		var item = speed_presets[num]
		add_radio(menu, item["text"], item["value"] == player.speed)

func _on_speed_presets_changed(speed: float) -> void:
	var menu: PopupMenu = menus["speed_presets"]
	if speed_presets_checked_idx:
		menu.set_item_checked(speed_presets_checked_idx, false) # Uncheck current
	var speed_floor = int(speed)
	speed_floor -= speed_floor % 10
	for item in speed_presets:
		if item["value"] == speed_floor:
			speed_presets_checked_idx = item["idx"]
			menu.set_item_checked(speed_presets_checked_idx, true) # Check new
			break

func _on_speed_presets_menu_pressed(item_id: int) -> void:
	# Any speed_presets item pressed
	if item_id >= speed_presets_first_idx and item_id < speed_presets_first_idx + speed_presets.size():
		player.speed = speed_presets[item_id - speed_presets_first_idx]["value"]
		_on_speed_presets_changed(player.speed)
		return

	var item_text: String = menus["speed"].get_item_text(item_id)
	match item_text:
		"Speed up (Num+)":
			# Multiplier should be based on interaction in UI vs. keyboard
			player.speed *= 2.0
		"Slow down (Num-)":
			player.speed = max(1, player.speed/2.0)
#		"100 (1)":
#			player.speed = 100
		_:
			Log.err("Unprocessed item in Speed menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_abilities_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Abilities", "abilities")
	#var menu := new_menu(parent, "Abilities " + str(randi()), "abilities")
	for ability in abilities_options:
		add_checkbox(menu, ability, player.can(abilities_options[ability]))

func _on_abilities_menu_pressed(item_id: int) -> void:
	var menu: PopupMenu = menus["abilities"]
	var new_state: bool = toggle_checkable(menu, item_id)
	var item_text: String = menu.get_item_text(item_id)
	player.set_ability(abilities_options[item_text], new_state)

	# @TODO Will denying ability stops it immediately when already started?
	if item_text == "Can Fly" and not new_state:
		player.fly_mode = 0
		player.gravity = -60

func add_weapons_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Weapons")
	for num in range(player.weapons.size()):
		var weapon = player.weapons[num]
		add_radio(menu, weapon["name"], num == player.weapon)

func _on_weapons_menu_pressed(item_id: int) -> void:
	#update_weapon(player.weapon, item_id)
	player.set_weapon(item_id, true, true)

# @TODO: Move to UI?
func update_weapon(old_weapon: int, new_weapon: int) -> void:
	if not "weapons" in menus:
		return
	var menu: PopupMenu = menus["weapons"]
	if old_weapon >= 0:
		menu.set_item_checked(old_weapon, false) # uncheck current
	menu.set_item_checked(new_weapon, true) # check new

func add_cam_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Camera", "cam")
	add_checkbox(menu, "AR/VR (V)")
	menu.add_separator()
	add_radio(menu, "First Person View (Ctrl+Num5)", player.cam == player.cam_1st)
	add_radio(menu, "Third Person View (Ctrl+Num5)", player.cam == player.cam_3rd)
	menu.add_separator()
	add_cam_view_menu(menu)
	add_cam_fov_menu(menu)
	add_cam_far_menu(menu)
	add_checkbox(menu, "Toggle Fog (F10)")

func _on_cam_menu_pressed(item_id: int) -> void:
	var menu: PopupMenu = menus["cam"]
	var item_text: String = menu.get_item_text(item_id)
	match item_text:
		"AR/VR (V)":
			Global.toggle_arvr()
		"First Person View (Ctrl+Num5)":
			player.cam_1st()
			player.cam_perspective()
			update_cam()
		"Third Person View (Ctrl+Num5)":
			player.cam_3rd()
			player.cam_perspective()
			update_cam()
		"Toggle Fog (F10)":
			LevelEnvironment.env.environment.fog_enabled = !LevelEnvironment.env.environment.fog_enabled
			menu.set_item_checked(item_id, LevelEnvironment.env.environment.fog_enabled)
		_:
			Log.err("Unprocessed item in Camera menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func update_cam() -> void:
	var menu: PopupMenu = menus["cam"]
	menu.set_item_checked(2, player.cam == player.cam_1st)
	menu.set_item_checked(3, player.cam == player.cam_3rd)

func add_cam_view_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "View", "cam_view")
	add_radio(menu, "Perspective Projection (Num5)", player.cam.projection == Camera.PROJECTION_PERSPECTIVE)
	add_radio(menu, "Orthogonal Projection (Num5)", player.cam.projection == Camera.PROJECTION_ORTHOGONAL)
	menu.add_separator()
	menu.add_item("Top View (Num7)")
	menu.add_item("Bottom View (Ctrl+Num7)")
	menu.add_item("Front View (Num1)")
	menu.add_item("Back View (Ctrl+Num1)")
	menu.add_item("Right View (Num3)")
	menu.add_item("Left View (Ctrl+Num3)")

func _on_cam_view_menu_pressed(item_id: int) -> void:
	var menu: PopupMenu = menus["cam_view"]
	var item_text: String = menu.get_item_text(item_id)
	match item_text:
		"Perspective Projection (Num5)":
			player.cam_perspective()
			menu.set_item_checked(item_id, true)
			menu.set_item_checked(item_id+1, false)
		"Orthogonal Projection (Num5)":
			player.cam_orthogonal()
			menu.set_item_checked(item_id, true)
			menu.set_item_checked(item_id-1, false)
		"Top View (Num7)":
			player.cam_top_view()
		"Bottom View (Ctrl+Num7)":
			player.cam_bottom_view()
		"Front View (Num1)":
			player.cam_front_view()
		"Back View (Ctrl+Num1)":
			player.cam_back_view()
		"Right View (Num3)":
			player.cam_right_view()
		"Left View (Ctrl+Num3)":
			player.cam_left_view()
		_:
			Log.err("Unprocessed item in Camera View menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_cam_fov_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "FOV / Zoom", "cam_fov")
	menu.add_item("-FOV / Zoom In (Ctrl+ScrollUp)")
	menu.add_item("+FOV / Zoom Out (Ctrl+ScrollDown)")
	menu.add_item("Reset to 90 deg (/)")
	menu.add_separator()
	for fov in [150, 120, 100, 80, 70, 60, 45, 30, 20, 15, 10, 5, 2, 1]:
		menu.add_item(str(fov) + " deg")

func _on_cam_fov_menu_pressed(item_id: int) -> void:
	var item_text: String = menus["cam_fov"].get_item_text(item_id)
	match item_text:
		"-FOV / Zoom In (Ctrl+ScrollUp)":
			player.cam.fov /= 1.5
		"+FOV / Zoom Out (Ctrl+ScrollDown)":
			player.cam.fov *= 1.5
		"Reset to 90 deg (/)":
			player.fov_reset()
		"150 deg":
			player.cam.fov = 150
		"120 deg":
			player.cam.fov = 120
		"100 deg":
			player.cam.fov = 100
		"80 deg":
			player.cam.fov = 80
		"70 deg":
			player.cam.fov = 70
		"60 deg":
			player.cam.fov = 60
		"45 deg":
			player.cam.fov = 45
		"30 deg":
			player.cam.fov = 30
		"20 deg":
			player.cam.fov = 20
		"15 deg":
			player.cam.fov = 15
		"10 deg":
			player.cam.fov = 10
		"5 deg":
			player.cam.fov = 5
		"2 deg":
			player.cam.fov = 2
		"1 deg":
			player.cam.fov = 1
		_:
			Log.err("Unprocessed item in FOV menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func add_cam_far_menu(parent: PopupMenu) -> void:
	var menu := new_menu(parent, "Far", "cam_far")
	menu.add_item("-Far ([)")
	menu.add_item("+Far (])")
	menu.add_separator()
	menu.add_item("1 m")
	menu.add_item("3 m")
	menu.add_item("10 m")
	menu.add_item("33 m")
	menu.add_item("100 m")
	menu.add_item("333 m")
	menu.add_item("1 km")
	menu.add_item("3 km")
	menu.add_item("10 km")
	menu.add_item("33 km")
	menu.add_item("100 km")
	menu.add_item("333 km")
	menu.add_item("1000 km")

func _on_cam_far_menu_pressed(item_id: int) -> void:
	var item_text: String = menus["cam_far"].get_item_text(item_id)
	match item_text:
		"-Far ([)":
			player.cam_far = max(10.0, player.cam_far / 1.5)
		"+Far (])":
			player.cam_far = min(2000000, player.cam_far * 1.5)
		"1 m":
			player.cam_far = 1
		"3 m":
			player.cam_far = 3
		"10 m":
			player.cam_far = 10
		"33 m":
			player.cam_far = 33
		"100 m":
			player.cam_far = 100
		"333 m":
			player.cam_far = 333
		"1 km":
			player.cam_far = 1e3
		"3 km":
			player.cam_far = 3e3
		"10 km":
			player.cam_far = 10e3
		"33 km":
			player.cam_far = 33e3
		"100 km":
			player.cam_far = 100e3
		"333 km":
			player.cam_far = 333e3
		"1000 km":
			player.cam_far = 1e6
		_:
			Log.err("Unprocessed item in Camera Far menu: " + str(item_id) + " (" + str(item_text) + ")", logchan)

func update_sizebar() -> void:
	var new_value = AntMath.log(player.scale.y, 10.0)
	Log.debug("update_sizebar() from player.scale.y: " + str(player.scale.y) + " to AntMath.log(scale, 10): " + str(new_value), logchan)
	sizebar.value = new_value

func _on_Sizebar_value_changed(value) -> void:
	#player.scale_object_local(Vector3.ONE * (value / 10))
	Log.debug("Sizebar changed: " + str(value), logchan)
	player.size = pow(10, value)

func update_zoombar() -> void:
	Log.debug("update_zoombar() from player.cam.fov: " + str(player.cam.fov), logchan)
	zoombar.value = 100 - (player.cam.fov / 1.2)

func _on_Zoombar_value_changed(value) -> void:
	Log.debug("Zoombar changed: " + str(value), logchan)
	player.cam.fov = (100 - value) * 1.2

func update_speedbar() -> void:
	var new_speed = sqrt(player.speed) # sqrt(player.speed / 10)
	Log.debug("update_speedbar() from player.speed: " + str(player.speed) + " to: " + str(new_speed), logchan)
	speedbar.value = new_speed

func _on_Speedbar_value_changed(value) -> void:
	Log.debug("Speedbar changed: " + str(value), logchan)
	# Changes speed by setter but doesn't propagate changes into itself
	player.set_speed(value*value, false) # 10*value*value

func update_bars() -> void:
	update_sizebar()
	update_zoombar()
	update_speedbar()

func _on_SizeButton_pressed() -> void:
	var tween := create_tween().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
	tween.tween_property(Global.player, "size", 1.0, 0.5)

func _on_ZoomButton_pressed() -> void:
	var tween := create_tween().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
	tween.tween_property(Global.player.cam, "fov", Global.player.cam_fov_default, 0.5)

func _on_SpeedButton_pressed() -> void:
	var tween := create_tween().set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
	tween.tween_property(Global.player, "speed", 100.0, 0.5)
