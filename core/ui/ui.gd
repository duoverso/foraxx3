extends Node

var logchan := "ui"

var settings_section := "ui"
var settings_properties := [
	"onstart_resize_window", "onstart_resize_window_x", "onstart_resize_window_y",
	"onstart_resize_window_rel", "onstart_resize_window_x_rel", "onstart_resize_window_y_rel",
	"onstart_position_window", "onstart_position_window_x", "onstart_position_window_y",
	"onstart_position_window_rel", "onstart_position_window_x_rel", "onstart_position_window_y_rel",
	"onstart_maximize_window", "onstart_fullscreen",
]

# Absolute window size (before maximization to be unmaximized to this size)
onready var onstart_resize_window := false
onready var onstart_resize_window_x := 600
onready var onstart_resize_window_y := 600
# Relative window size to screen size (before maximization to be unmaximized to this size)
onready var onstart_resize_window_rel := true
onready var onstart_resize_window_x_rel := 0.75
onready var onstart_resize_window_y_rel := 0.75
# Absolute window position
onready var onstart_position_window := false
onready var onstart_position_window_x := 0
onready var onstart_position_window_y := 0
# Relative window position to screen size
onready var onstart_position_window_rel := true
onready var onstart_position_window_x_rel := 0.125
onready var onstart_position_window_y_rel := 0.125
onready var onstart_maximize_window := true
onready var onstart_fullscreen := false

onready var pause_screen := $"%Pause"

onready var hud := $"%HUD"

onready var console := $"%Console"

func _enter_tree():
	Log.verbose("_enter_tree() at: " + get_path(), logchan)
	Settings.connect("settings_loaded", self, "load_settings")
	Log.verbose("Settings.connect(settings_loaded) to load_settings()", logchan)
	Settings.connect("save_settings", self, "save_settings")
	Log.verbose("Settings.connect(save_settings) to save_settings()", logchan)

func load_settings() -> void:
	Settings.load_to(self, settings_section, settings_properties)

func save_settings() -> void:
	Settings.save_from(self, settings_section, settings_properties)

func _ready() -> void:
	Log.verbose("_ready()", logchan)
	load_settings()
	#set_pause_mode(Node.PAUSE_MODE_PROCESS) # already set in .tscn
	pause_screen.visible = false
	apply_window_settings()

func apply_window_settings() -> void:
	Log.verbose("apply_window_settings()", logchan)
	#OS.set_window_per_pixel_transparency_enabled(true)
	OS.set_borderless_window(false)
	# Set window size relative to screen size before maximization
	#OS.set_window_size(Vector2(1024, 578))
	var screen = OS.get_screen_size()
	Log.info("Screen size: " + str(screen), logchan)
	if onstart_resize_window:
		Log.info("onstart_resize_window (from " + str(OS.window_size, "): ", onstart_resize_window_x, "x", onstart_resize_window_y), logchan)
		OS.window_size = Vector2(onstart_resize_window_x, onstart_resize_window_y)
	if onstart_resize_window_rel:
		Log.info("onstart_resize_window_rel (from " + str(OS.window_size, "): ", onstart_resize_window_x_rel, "x", onstart_resize_window_y_rel), logchan)
		OS.window_size = Vector2(screen.x * onstart_resize_window_x_rel, screen.y * onstart_resize_window_y_rel)
	if onstart_position_window:
		Log.info("onstart_position_window (from " + str(OS.window_position, "): ", onstart_position_window_x, "x", onstart_position_window_y), logchan)
		OS.window_position = Vector2(onstart_position_window_x, onstart_position_window_y)
	if onstart_position_window_rel:
		Log.info("onstart_position_window_rel (from " + str(OS.window_position, "): ", onstart_position_window_x_rel, "x", onstart_position_window_y_rel), logchan)
		OS.window_position = Vector2(floor(screen.x * onstart_position_window_x_rel), floor(screen.y * onstart_position_window_y_rel))
	if onstart_maximize_window:
		OS.window_maximized = true
		Log.info("Window size (after maximization): " + str(OS.window_size), logchan)
	if onstart_fullscreen:
		OS.window_fullscreen = true

func _unhandled_input(event: InputEvent) -> void:
	#Log.verbose("_unhandled_input( " + str(event) + " )", logchan)

	if event is InputEventKey and event.is_pressed():

		if event.is_action("quit"):
			#Settings.save()
			get_tree().quit()

		#elif Input.is_key_pressed(KEY_ESCAPE):
		elif event.is_action("ui_cancel"):
			Global.player.mouse_captured = toggle_mouse_mode()

		# Thanks to GDQuest's How to Code a Pause in Godot (tutorial) (https://www.youtube.com/watch?v=Jf7F3JhY9Fg)
		elif event.is_action("pause"):
			var new_pause_state = not get_tree().paused
			get_tree().paused = new_pause_state
			pause_screen.visible = new_pause_state
			Log.info("Game is paused" if get_tree().paused else "Game is running", logchan)

		elif event.is_action("mouse_mode"):
			Global.player.mouse_captured = toggle_mouse_mode()

		elif event.is_action("load_game"):
			Settings.load_user_config()

		elif event.is_action("save_game"):
			Settings.save_user_config()

		elif event.is_action("go_to_lobby"):
			#print("go to lobby\n")
			#get_tree().change_scene("res://scenes/Level-0.tscn")
			LevelManager.goto_level("constructor")

		elif event.is_action("return"):
			#Global.player.translation = Vector3(0, 100, 0)
			LevelManager.return_player_to_start()

		elif event.is_action("next_level"):
			LevelManager.next_level()

		elif event.is_action("previous_level"):
			LevelManager.previous_level()

		elif event.is_action("random_environment"):
			LevelEnvironment.rand_environment()
		else:
			return # unhandled

		Log.debug("handled " + str(event), logchan)
		get_tree().set_input_as_handled()

func toggle_mouse_mode() -> bool:
	if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		hud.crosshair.hide()
		hud.show_top_menu()
		return false
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		hud.crosshair.show()
		hud.hide_top_menu()
		return true
