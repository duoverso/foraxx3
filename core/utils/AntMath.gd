extends Object
class_name AntMath

# Logarithm with any base
static func log(value: float, base: float) -> float:
	return log(value) / log(base)
