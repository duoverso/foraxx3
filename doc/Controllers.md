# Simplest
```
- Player: KinematicBody
	- Camera1st
	- Camera3rd
	- ARVRCamera
```

# More controllers
- more controller types, controllers, models...
- camera independent to controller
- realtime strategy!

```
- Player: Spatial
	- FPSPlayer: KinematicBody
		- CameraBase: Spatial
		- Flashlight: SpotLight
		- Model: MeshInstance
	- RPGPlayer: RigidBody
		- CameraBase: Spatial
		- OmniLight
		- Model: MeshInstance
	- Vehicle: VehicleBody
		- CameraBase: Spatial
		- Lights: Array of SpotLight
		- Model: MeshInstance
	- Camera
		- CameraFlashlight: SpotLight
	- ARVRCamera
```