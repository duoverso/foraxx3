Exports
=======

In **Project Settings**

Input Devices > Pointing:
    Emulate Touch From Mouse: Off (for touch screens, On to emulate controls like virtual joysticks)

# Linux

# Windows Desktop

# UWP (Universal Windows Platform) - Xbox One

# Mac OSX

# iOS

# Android

```
# Free previous offline devices
adb kill-server

# Connect USB cable
adb tcpip 5555

# Disconnect USB cable & connect
adb connect 192.168.1.123:5555
```

## Permissions
### DownloadManager
- Internet (`OS.request_permissions()` isn't needed)

### Android Scope Storage
> to read/write outside `user://` (`OS.request_permissions()` IS NEEDED)
- Manage External Storage
- Read External Storage
- Write External Storage

# HTML5 (https://antray.duoverso.com)
- [Exporting for the Web](https://docs.godotengine.org/en/stable/tutorials/export/exporting_for_web.html)
- [Custom HTML page for Web export](https://docs.godotengine.org/en/stable/tutorials/platform/customizing_html5_shell.html)

## cross-origin isolation headers
> See https://docs.godotengine.org/en/latest/tutorials/export/exporting_for_web.html#threads and https://web.dev/cross-origin-isolation-guide/

in NGINX use [`add_header`](https://nginx.org/en/docs/http/ngx_http_headers_module.html#add_header)
```
add_header Cross-Origin-Embedder-Policy require-corp;
add_header Cross-Origin-Opener-Policy same-origin;
```

in PHP
```
<?php
header('Cross-Origin-Embedder-Policy: require-corp');
header('Cross-Origin-Opener-Policy: same-origin');
?>
```