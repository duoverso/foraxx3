# Filesystem structure

* [`addons/`](../addons/)
* [`assets/`](../assets/)
	* [`audio/`](../assets/audio/)
		* [`music/`](../assets/audio/music/) - Music (OGG, MP3...) are loaded from here & `user://repos/<repo>/music`
		* [`sound/`](../assets/audio/sound/) - Sounds (WAV...) are loaded from here & `user://repos/<repo>/sound`
	* [`environemnt/`](../assets/environment/) - Environments (HDR images...) are loaded from here & `user://repos/<repo>/environment`
	* [`levels/`](../assets/levels/) - Levels (Godot's .tscn, .scn...) are loaded from here & `user://repos/<repo>/levels`
	* [`materials/`](../assets/materials/) - Materials (colors, images, shaders...) are loaded from here & `user://repos/<repo>/materials`
	* [`models/`](../assets/models/) - Models are loaded from here & `user://repos/<repo>/models`
		* [`buildings/`](../assets/models/buildings/)
		* [`geometries/`](../assets/models/geometries/)
		* [`nature/`](../assets/models/nature/)
	* [`terrain/`](../assets/terrain/)
		* [`generator/`](../assets/terrain/generator/)
			* [`chunk.gd`](../assets/terrain/generator/chunk.gd) - Creates each tile of grid
			* [`generator.gd`](../assets/terrain/generator.gd) - Terrain generator
	* [`ui/`](../assets/ui/)
		* [`cursors/`](../assets/ui/cursors/) - Images (PNG, SVG...)
		* [`fonts/`](../assets/ui/fonts/)
		* [`joysticks/`](../assets/ui/joysticks/)
		* [`theme/`](../assets/ui/theme/) - Godot Themes
* [`core/`](../core/)
	* [`apps/`](../core/apps/) - 3D/XR apps
	* [`audio/`](../core/audio/)
		* [`music.gd`](../core/audio/music.gd) - Autoloaded `Music`
		* [`sound.gd`](../core/audio/sound.gd) - Autoloaded `Sound`
	* [`bbcode/`](../core/bbcode/) - Custom `RichTextEffect`s
	* [`input/`](../core/input/) - Input Controllers
		* [`camera.gd`](../core/input/camera.gd) - `PlayerCamera`
		* [`player.gd`](../core/input/player.gd) - Base `Player` class inherited by all Player Controllers
	* [`net/`](../core/net/)
		* [`download_manager.gd`](../core/net/download_manager.gd) - Autoloaded `DownloadManager`
		* [`repo_manager.gd`](../core/net/repo_manager.gd) - Autoloaded `RepoManager`
	* [`osm/`](../core/osm/) - OpenStreetMap
	* [`quest/`](../core/quest/) - Quest System
	* [`server/`](../core/server/) - Multiplayer support
	* [`ui/`](../core/ui/)
		* [`hud.gd`](../core/ui/hud.gd) - Handles HUD
		* [`hud.tscn`](../core/ui/hud.tscn) - Autoloaded `HUD`
		* [`top_menu.gd`](../core/ui/top_menu.gd) - Generates and handles Top Menu
		* [`top_menu.tscn`](../core/ui/top_menu.tscn) - Top Menu
	* [`camera_follow.gd`](../core/camera_follow.gd) - GDScript for Camera to follow path
	* [`inventory.gd`](../core/inventory.gd) - Inventory features
	* [`joystick.gd`](../core/joystick.gd)
	* [`level.gd`](../core/level.gd) - Base `Level` class inherited by all Levels
	* [`level_environment.gd`](../core/level_environment.gd) - Autoloaded `LevelEnvironment`
	* [`level_manager.gd`](../core/level_manager.gd) - Autoloaded `LevelManager`
	* [`log.gd`](../core/log.gd) - Autoloaded `Log`
	* [`start_point.gd`](../core/start_point.gd)
	* [`player_start.tscn`](../core/player_start.tscn)
	* [`resource_queue.tscn`](../core/resource_queue.tscn)
	* [`sensors.gd`](../core/sensors.gd) - Autoloaded `Sensors`
	* [`settings.gd`](../core/settings.gd) - Autoloaded `Settings`
* [`doc/`](../doc/)
* [`global.tscn`](../global.tscn) - Autoloaded `Global`
* [`global.gd`](../global.gd)