extends Node

var logchan := "global"

var _loaded := false

#export(int, "Visible", "Hidden", "Captured", "Confined") var mouse_mode = Input.MOUSE_MODE_CAPTURED

# Possible values: "Android", "iOS", "HTML5", "OSX", "Server", "Windows", "UWP", "X11"
onready var os_name := OS.get_name()
var is_mobile: bool
var is_web: bool
var is_desktop: bool

onready var has_touch := OS.has_touchscreen_ui_hint()

export var start_in_arvr := false
onready var arvr_interface := ARVRServer.find_interface("Native mobile")

enum PlayerControllers {DUMMY, GONKEE, GARBAJ_IMPROVED, CAR, SPACE_SHIP}
export(PlayerControllers) var player_controller
onready var player: Player # KinematicBody # = $Player

onready var enemy_names := ClassDB.get_class_list()

func _init():
	Log.verbose("_init()", logchan)

	if os_name in ["Android", "BlackBerry 10", "iOS"]:
		is_mobile = true
		is_web = false
		is_desktop = false
	elif os_name in ["Flash", "HTML5"]:
		is_web = true
		is_mobile = false
		is_desktop = false
	else:
		is_desktop = true
		is_web = false
		is_mobile = false

func _enter_tree():
	Log.verbose("_enter_tree() at: " + get_path(), logchan)
	if get_path() == "/root/Global":
		_loaded = true
	else:
		Log.err("!!! global.gd _enter_tree() at path: " + get_path() + " ... queue_free()", logchan)
		queue_free()

func _ready():
	if not _loaded:
		return

	Log.info("OS.get_processor_count(): " + str(OS.get_processor_count()), logchan)

	if start_in_arvr:
		start_arvr()

	if is_mobile:
		mobile_ready_start()
	elif is_web:
		web_ready_start()
	else:
		desktop_ready_start()

	#var Downloader = preload("res://core/net/downloader.gd")
	Downloader.download_user_files()
	Repo.load_all()
	load_player()


	if is_mobile:
		mobile_ready_end()
	elif is_web:
		web_ready_end()
	else:
		desktop_ready_end()


	call_deferred("when_ready")

func when_ready():
	if has_touch:
		UI.hud.enable_touch_control()
	else:
		UI.hud.disable_touch_control()

	if is_mobile:
		mobile_ready_deferred()
	elif is_web:
		web_ready_deferred()
	else:
		desktop_ready_deferred()

func load_player():
	Log.verbose("load_player()", logchan)
	match player_controller:
		PlayerControllers.DUMMY:
			Log.verbose("player_controller: DUMMY", logchan)
			player = load("res://core/input/dummy.tscn").instance()
		PlayerControllers.GONKEE:
			Log.verbose("player_controller: GONKEE", logchan)
			player = load("res://core/input/gonkee.tscn").instance()
		PlayerControllers.GARBAJ_IMPROVED:
			Log.verbose("player_controller: GARBAJ_IMPROVED", logchan)
			player = load("res://core/input/garbaj.tscn").instance()
		PlayerControllers.CAR:
			Log.verbose("player_controller: CAR ... not implemented", logchan)
		PlayerControllers.SPACE_SHIP:
			Log.verbose("player_controller: SPACE_SHIP ... not implemented", logchan)
		_:
			Log.err("Unknown player_controller", logchan)
	add_child(player)

func start_arvr():
	Log.info("start_arvr(); interface: " + str(arvr_interface) + "; initialized: " + str(arvr_interface.initialize()), logchan)
	#UI.hud.debug_container.get_node("Event").text = str(ARVRServer.get_interfaces())
	if arvr_interface and arvr_interface.initialize():
		if OS.has_touchscreen_ui_hint():
			has_touch = false
			UI.hud.disable_touch_control()
		UI.hud.crosshair.hide()
		UI.hud.debug_container.hide()
		#UI.hud.get_node("MarginContainer").hide()
		#player.rotation = Vector3(0, 0, 0)

		get_viewport().arvr = true
		get_viewport().hdr = false
		# keep linear color space, not needed with the GLES2 renderer
		get_viewport().keep_3d_linear = true
		# make sure vsync is disabled or we'll be limited to 60fps
		OS.vsync_enabled = false
		# up our physics to 90fps to get in sync with our rendering
		Engine.iterations_per_second = 90

		player.hide_model()
		player.y_rotation_speed = 0.005

func stop_arvr():
	if arvr_interface and arvr_interface.interface_is_initialized:
		if OS.has_touchscreen_ui_hint():
			has_touch = true
			UI.hud.enable_touch_control()
		UI.hud.crosshair.show()
		UI.hud.debug_container.show()
		#UI.hud.get_node("MarginContainer").show()

		get_viewport().arvr = false
		get_viewport().hdr = true
		arvr_interface.uninitialize()
		Engine.iterations_per_second = 60

		player.show_model()
		player.y_rotation_speed = 0.01

func toggle_arvr() -> bool:
	if get_viewport().arvr:
		stop_arvr()
		return false
	else:
		start_arvr()
		return true

func mobile_ready_start():
	#OS.show_virtual_keyboard()

	#OS.request_permissions()

	if OS.get_name() == "Android":
		$DirectionalLight.light_energy = 0

func mobile_ready_end():
	pass

func mobile_ready_deferred():
	pass

func web_ready_start():
	pass

func web_ready_end():
	player.hide_model()

func web_ready_deferred():
	pass

func desktop_ready_start():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	UI.hud.hide_top_menu()

func desktop_ready_end():
	pass

func desktop_ready_deferred():
	pass

#func mouse_captured():
#	mouse_mode = Input.MOUSE_MODE_CAPTURED
#	Input.set_mouse_mode(mouse_mode)
#
#func mouse_visible():
#	mouse_mode = Input.MOUSE_MODE_VISIBLE
#	Input.set_mouse_mode(mouse_mode)

#func toggle_mouse_mode() -> bool:
#	if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
#		mouse_visible()
#		UI.hud.crosshair.hide()
#		UI.hud.show_top_menu()
#		return false
#	else:
#		mouse_captured()
#		UI.hud.crosshair.show()
#		UI.hud.hide_top_menu()
#		return true

func _exit_tree() -> void:
	Log.verbose("_exit_tree()", logchan)
#	#player.queue_free()
#	queue_free()
